// Based on database constraints, these queries should not return any items:

//ROOMS VALIDATION
//2. If item is in user inventory, it can be picked up
//TODO

//3. If Item is in roomItemList, it does exist
//TODO

//4. If Item is in userInventory, it does exist
//TODO

//5. The order of the array for  roomImageList is [roomImage, leftDoorImage, rightDoorImage, item, item, item]
//TODO

//6. At start, the door images in roomImageList should be blank 
//TODO

//7. At start, the items in the roomImageList should exist

//8. At start, the item images in roomImageList should match the images ineach item's changeImagePath
//TODO

//9. At start, all imageStates should be 0

//10. At start, all linkedRooms should be length 1, and should contain the room's id


//ITEMS VALIDATION
//10. Confirm that roomImagePath, changeImagePath, and changeImageCmd are added to all items

//11. Confirm that either all of the above three are blank, or that none are blank

//12. confirm that changeImageCmd equals stageKeyType

//13. If an item does not have stages, it should be unactivated
//TODO

//14.  MAYBE give all items stages?

//15. If Image canBeHeld==true, then imagePath!=""

//16. If Item has a pushNumber/pullNumber, should also have stage with keyType push/pull


//DOORS VALIDATION
//1. Every door starts out as locked

//2. Every door has stages

//3. Every stage has all required aspects: keytype, password, onStageCompleteMessage, onStageCompleteDesc

//ACTION TESTS:
//1. Confirm that an object, once picked up, wont have its roomListImageState changed again

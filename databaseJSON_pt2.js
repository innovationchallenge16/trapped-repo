//Reset database
use trapped_db;

db.items.drop();
db.userrooms.drop();
db.doors.drop();  
db.testlogs.drop();
db.createCollection("items");
db.createCollection("userrooms");
db.createCollection("doors");
db.createCollection("testlogs");

//This is a continuation of the previous page, for reading purposes specifically

//UserRooms - 8
//TODO ADD All roomSafes
db.userrooms.insert(
 [{
  roomId: "I",
  roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. Looking around you find a lockbox i. The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorIJ", 
  roomRightDoor: "doorIH",
  roomImagePath: "images/newRooms/masterBathroom/MasterBathroom.png",
  roomImageList:   [{ imageId: "I",
                        imageState: 0 },
                      { imageId: "doorIJ", 
                        imageState: 0  },
                      { imageId: "doorIH",
                        imageState: 0 },
                      { imageId: "lockbox i",
                        imageState: 0 }],
  itemsInRoom: ["lockbox i"],
  linkedRooms: ["I"],
  userName: "Ian",
  userInventory: ["note 1i", "note 2i-b"]
 },
 {
  roomId: "J",
  roomDescription: "You find yourself in a modern looking living room. There is also a lockbox j that looks interesting, and a top hat. The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorJK", 
  roomRightDoor: "doorJI",
  roomImagePath: "images/newRooms/livingRoom/livingRoom.png",
  roomImageList:   [{ imageId: "J",
                        imageState: 0 },
                      { imageId: "doorJK", 
                        imageState: 0  },
                      { imageId: "doorJI",
                        imageState: 0 },
                      { imageId: "lockbox j",
                        imageState: 0 }],
  itemsInRoom: ["lockbox j", "top hat"],
  linkedRooms: ["J"],
  userName: "Jesus",
  userInventory: ["note 1j"]
 },
 {
  roomId: "K",
  roomDescription: "You find yourself in an overwhelmingly pink bedroom. As you squint your eyes against the offensive color scheme you see a cabinet and a keypad. Every now and then you can hear a faint sound. Is that scratching? The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorKL", 
  roomRightDoor: "doorKJ",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoom.png",
  roomImageList:   [{ imageId: "K",
                        imageState: 0 },
                      { imageId: "doorKL", 
                        imageState: 0  },
                      { imageId: "doorKJ",
                        imageState: 0 },
                      { imageId: "cabinet",
                        imageState: 0 },
                      { imageId: "parrot",
                        imageState: 0 }],
  itemsInRoom: ["cabinet", "parrot", "keypad"],
  linkedRooms: ["K"],
  userName: "Karl",
  userInventory: ["note 1k"]
 },
 {
  roomId: "L",
  roomDescription: "You find yourself in a cute nursery. As you look around you notice crackers on the floor and some money on the ceiling. The money looks like it is just out of reach for you. If only you had something to help you reach it. The clock on the wall does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorLM", 
  roomRightDoor: "doorLK",
  roomImagePath: "images/newRooms/nursery/nursery.png",
  roomImageList:   [{ imageId: "L",
                        imageState: 0 },
                      { imageId: "doorLM", 
                        imageState: 0  },
                      { imageId: "doorLK",
                        imageState: 0 },
                      { imageId: "crackers",
                        imageState: 0 },
                      { imageId: "money",
                        imageState: 0 }],
  itemsInRoom: ["crackers","money"],
  linkedRooms: ["L"],
  userName: "Luke",
  userInventory: ["note 1l"]
 },
 {
  roomId: "M",
  roomDescription: "You find yourself in trapped in a gym. Looking around the only thing of interest that you see is a combo lockbox. The only way out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorMN", 
  roomRightDoor: "doorML",
  roomImagePath: "images/newRooms/gym/gym.png",
  roomImageList:   [{ imageId: "M",
                        imageState: 0 },
                      { imageId: "doorMN", 
                        imageState: 0  },
                      { imageId: "doorML",
                        imageState: 0 },
                      { imageId: "combo lockbox",
                        imageState: 0 }],
  itemsInRoom: ["combo lockbox"],
  linkedRooms: ["M"],
  userName: "Michael",
  userInventory: ["note 1m", "note 2m"]
 },
 {
  roomId: "N",
  roomDescription: "You find yourself trapped in a theater room of some sort. Looking around you see a combination lock, and a black hat. Wonder what it holds? Hmm It is not so bad in here. Maybe if you could put on a movie it would not be horrible to stay here for a bit. The only way out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorNO", 
  roomRightDoor: "doorNM",
  roomImagePath: "images/newRooms/theater/theater.png",
  roomImageList:   [{ imageId: "N",
                        imageState: 0 },
                      { imageId: "doorNO", 
                        imageState: 0  },
                      { imageId: "doorNM",
                        imageState: 0 },
                      { imageId: "combination lock",
                        imageState: 0 }],
  itemsInRoom: ["combination lock","black hat"],
  linkedRooms: ["N"],
  userName: "Nicholas",
  userInventory: ["note 1n"]
 },
 {
  roomId: "O",
  roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. As you hold your nose from the smell you see a locked box and some yellow cheese. Nasty! Who keeps cheese in the bathroom? The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorOP", 
  roomRightDoor: "doorON",
  roomImagePath: "images/newRooms/bathroom/bathroom.png",
  roomImageList:   [{ imageId: "O",
                        imageState: 0 },
                      { imageId: "doorOP", 
                        imageState: 0  },
                      { imageId: "doorON",
                        imageState: 0 },
                      { imageId: "yellow cheese",
                        imageState: 0 },
                      { imageId: "locked box",
                        imageState: 0 }],
  itemsInRoom: ["yellow cheese", "locked box"],
  linkedRooms: ["O"],
  userName: "Omar",
  userInventory: ["note 1o", "note 2o"]
 },
 {
  //TODO CHECK THIS ROOM after Raegan checks in new room image
  roomId: "P",
  roomDescription: "You look around the simple bedroom and don't find much of interest besides a standard desk, and the software red hat. As you look at the figure in the painting on the wall you think that it looks familiar. The only ways out that you can see are two doors on either side of the room. ",
  roomLeftDoor: "doorPA", 
  roomRightDoor: "doorPO",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroom.png",
  roomImageList:   [{ imageId: "P",
                        imageState: 0 },
                      { imageId: "doorPA", 
                        imageState: 0  },
                      { imageId: "doorPO",
                        imageState: 0 },
                      { imageId: "desk",
                        imageState: 0 },
                      { imageId: "gray mouse",
                        imageState: 0 }],
  itemsInRoom: ["desk", "gray mouse", "red hat"],
  linkedRooms: ["P"],
  userName: "Phillip",
  userInventory: ["note 1p"]
 }
 ]);


////ITEMS LIST//// - 40
db.items.insert(
 [
//ALL NOTES
{
  itemId: "note1I",
  itemName: "note 1i",
  description: "Day 35: Doris has given me the following clue: What oil sector is ConocoPhillips classified as that is also known as E&P?  How am I supposed to figure it out without google?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1I.png" 
 },
 {
  itemId: "note2I",
  itemName: "note 2i",
  description: "A S E H L",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2I.png" 
 },
 {
  itemId: "note2I-B",
  itemName: "note 2i-b",
  description: "Day 329: I don't undertand. Doris said that the combination was a birthday. If it was not mine, whose is it?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2IB.png" 
 },
 {
  //TODO CHANGEME
  itemId: "note1J",
  itemName: "note 1j",
  description: "Look under your chair",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1K.png" //nchanged because jk mix up
 },
 {
  itemId: "note2J",
  itemName: "note 2j",
  description: "New technologies have allowed us to more effectively get oil and gass out of what type of rock? Doris said the password is the answer to that question. What could it be? Im not a geologist...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2J.png" 
 },
  {
  itemId: "note1K",
  itemName: "note 1k",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors'" ,
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1J.png" //nchanged because jk mix up
 },
 {
  itemId: "note4k", //USER DOES NOT START HOLDING THIS CLUE
  itemName: "note4k",
  description: "Look in the Hat" ,
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note4K.png" 
 },
 {
  itemId: "note1L",
  itemName: "note 1l",
  description: "Day 52: Still can not figure out the password to the door on the right. The only thing Doris will say when I ask is Tick Toc...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1L.png" 
 },
  {
  itemId: "note1M",
  itemName: "note 1m",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1M.png" 
 },
 {
  itemId: "note 2M",
  itemName: "note 2m",
  description: "Day 329: I don't undertand. Doris said that the combination was a birthday. If it was not mine, whose is it?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2M.png" 
 },
 { //TODO Double check this pw on door
  itemId: "note 2MP",
  itemName: "note 2mpic",
  description: "Note with two Pictograms on it",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2MP.png" 
 },
 {
  itemId: "note1N",
  itemName: "note 1n",
  description: "Day 34: The only clue I can get from Doris is that the password to one of the doors is the name of a movie that has her computer brother, Joshua, in it and that he likes to play games? I wonder if her brother is as crazy as she is. If only I had access to google I could figure it out...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1N.png" 
 },
 {
  itemId: "note 2NP",
  itemName: "note 2npic",
  description: "<Has note about password being answer to the pictogram puzzles>",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2NP.png" 
 },
  {
  itemId: "note1O",
  itemName: "note 1o",
  description: "Day 101:  I have finally tricked Doris into giving me a clue to the password for one of the doors. Doris said something about inspecting the code comments? What does that even mean though?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1O.png" 
 },
 {
  itemId: "note2O",
  itemName: "note 2o",
  description: "Day 2: I have found a Bobby pin. Maybe I can use it to pick a lock. Like the one I saw in the room next door.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2O.png" 
 },
 {
  itemId: "note1P",
  itemName: "note 1p",
  description: "Day 97: After months of questioning I have made a break through. The password to one of the doors has something to do with Doris's Mother. She said that there is a picture of her mother in the room. How does a computer have a mother? If only I had access to google I might be able to figure it out...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1P.png" 
 },

//ROOM I ITEMS
{
  itemId: "lockbox i",
  itemName: "lockbox i",
  description: "What's inside?",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //password is userJ's birthday, derived from note
                    key: "092194", //FINDME Personalized!
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2i"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/masterBathroom/masterBathroomBlank.png",
  changeImagePath: "images/newRooms/masterBathroom/masterBathroomOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed  
 },
 //ROOM J ITEMS
 {
  itemId: "lockbox j",
  itemName: "lockbox j",
  description: "What's inside?",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //password is userI's birthday, drived from note
                    key: "112395", //FINDME Personalized!
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2j"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/livingRoom/livingRoomBlank.png",
  changeImagePath: "images/newRooms/livingRoom/livingRoomOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed 
 },
 {
  itemId: "hatJ",
  itemName: "top hat",
  description: "I don't see anything inside!",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "TEMP", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "I think the hat is actually empty this time.",
                    onStageCompleteMessage: "You found a fishing rod? How did that fit in a hat?!",
                    postStageCmd: "pick up rod"
                }],
  imagePath: "images/items/topHat.png" 
 },
 {
  itemId: "rodJ",
  itemName: "rod",
  description: "This looks like it's missing an important piece.",
    canBeHeld: false,
  unActivated: true, 
  imagePath: "images/items/rod.png" 
 },
{
  itemId: "fishingrodJ",
  itemName: "fishing rod",
  description: "What can you use this on? There's no water around...maybe it can reach something far away?",
    canBeHeld: true,
  unActivated: false, 
  imagePath: "images/items/fishingrod.png" 
 },
 //DOOR K ITEMS
  
 {
  itemId: "heavyItemK",
  itemName: "cabinet",
  description: "This is heavy, I wonder if there's anything behind it?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "L",
                    onStageCompleteDesc: "Well this is unusual...you found a parrot behind the cabinet!",
                    onStageCompleteMessage: "A wild parrot appeared! He is shouting 'POLLY WANTS A CRACKER', and it's very annoying. ",
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoomBlank.png",
  changeImagePath: "images/newRooms/kidsRoom/kidsRoomMoved.png", //Path for image once its been changed
  changeImageCmd: "push" // Command that signals item will need to be changed  
 },
 {
  itemId: "parrotK",
  itemName: "parrot",
  description: "The parrot has a key..but he doesn't look like he wants to give it up. ",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "crackers",
                    onStageCompleteDesc: "You aren't sure how, but the parrot disappeared when you stopped looking...how bizarre!",
                    onStageCompleteMessage: "The parrot gave you key kj",
                    postStageCmd: "pick up key kj"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoomBlank.png",
  changeImagePath: "images/newRooms/kidsRoom/kidsRoomTaken.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed 
 },
{ 
  itemId: "keypadK",
  itemName: "keypad",
  description: "It appears that you can type a password directly to Doris herself through this alphanumeric keypad...I wonder what it could be for? ",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password derived from 7 worded question
                    key: "4712563",
                    onStageCompleteDesc: "The keypad doesn't look very interesting now that you've already activated it.",
                    onStageCompleteMessage: "Doris is very impressed that you figured out her puzzle..she has given you a special clue to help you get out.",
                    postStageCmd: "pick up note4k"
                }],
  imagePath: "" 
 },
{
  itemId: "keyKJ",
  itemName: "key kj",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key.png" 
 },
//ROOM L ITEMS
 {
  itemId: "moneyL", //TODO Give Override
  itemName: "money",
  description: "That money looks pretty nifty...too bad you can't reach it, no matter how hard you reach.",
    canBeHeld: false, //manually set this when money can actually be reached
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "fishing rod",
                    onStageCompleteDesc: "Now you have money! ....in a house. Where you can't buy anything. How useful.",
                    onStageCompleteMessage: "You retrieved the money!",
                    postStageCmd: "pick up money"
                }],
  imagePath: "images/items/moneyL.png",
  roomImagePath: "images/newRooms/nursery/nurseryMoney.png",
  changeImagePath: "images/newRooms/nursery/nurseryBlank.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed  
 },
 {
  itemId: "crackersL",
  itemName: "crackers",
  description: "I probably wouldn't eat crackers that have been left out...but I bet someone else would.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/crackers2L.png",
  roomImagePath: "images/newRooms/nursery/nurseryCrackers.png",
  changeImagePath: "images/newRooms/nursery/nurseryBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" // Command that signals item will need to be changed 
 },
//ROOM M ITEMS
{
  itemId: "combinationlockM",
  itemName: "combo lockbox",
  description: "I wonder if there's anything useful in here...",//"contains note about password [2NP]; Opens with birthday of N",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password is userN's birthday, derived from note
                    key: "091196", //FINDME Personalized!
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2mpic"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/gym/gymBlank.png",
  changeImagePath: "images/newRooms/gym/gymOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed  
 },
 //ROOM N ITEMS
 {
  itemId: "combinationlockN",
  itemName: "combination lock",
  description: "I wonder if there's anything useful in here...",//"Contains note with Pictograms [2MP]; Opens with birthday of M",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password is userM's birthday, derived from note
                    key: "121394", //FINDME Personalized!
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2npic"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/theater/theaterBlank.png",
  changeImagePath: "images/newRooms/theater/theaterOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed 
 },
 {
  itemId: "hatN",
  itemName: "black hat",
  description: "There is a worn handle inside the hat...what a weird place to keep that",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "look", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "Nothing else inside this giant black hat.",
                    onStageCompleteMessage: "There is a worn handle inside the hat...what a weird place to keep that",
                    postStageCmd: "pick up handle" //FINDME change to pick up reel
                }],
  imagePath: "" 
 },
 {
  itemId: "handleN",
  itemName: "handle",
  description: "This handle is clearly old, but it looks like it connects to something.",
  canBeHeld: true,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key", //change to look when sevenworded question is answered
                    key: "rod",
                    onStageCompleteDesc: "This is part of the pole now",
                    onStageCompleteMessage: "The handle combined with the rod to create a pole",
                    postStageCmd: "pick up pole"
                }],
  imagePath: "images/items/handle.png" 
 },
 {
  itemId: "poleN",
  itemName: "pole",
  description: "This handle is clearly old, but it looks like it connects to something.",
  canBeHeld: true,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key", //change to look when sevenworded question is answered
                    key: "reel",
                    onStageCompleteDesc: "This is part of the fishing rod now",
                    onStageCompleteMessage: "The pole combined with the reel to create a working fishing rod! I wonder what you can use it to reach?",
                    postStageCmd: "pick up fishing rod"
                }],
  imagePath: "images/items/pole.png" 
 },
 //ROOM O ITEMS
 {
  itemId: "cheeseO",
  itemName: "yellow cheese",
  description: "yellow cheese",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/cheeseO.png",
  roomImagePath: "images/newRooms/bathroom/bathroomCheese.png",
  changeImagePath: "images/newRooms/bathroom/bathroomBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" // Command that signals item will need to be changed  
 },
 {
  itemId: "lockedboxO",
  itemName: "locked box",
  description: "I wonder how you'll open this?",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "bobby pin",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up dusty key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/bathroom/bathroomBlank.png",
  changeImagePath: "images/newRooms/bathroom/bathroomOpen.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed 
 },
 {
  itemId: "keyON",
  itemName: "dusty key",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key2.png" 
 },
 //ROOM P ITEMS
 {
  itemId: "heavyItemP",
  itemName: "desk",
  description: "This is heavy, I wonder if there's anything behind it?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "O",
                    onStageCompleteDesc: "There's a gray mouse staring at you with beady eyes, holding a bobby pin.",
                    onStageCompleteMessage: "You pushed the dresser, only to reveal a gray mouse, holding a bobby pin.",
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroomBlank.png",
  changeImagePath: "images/newRooms/adultBedroom/adultBedroomMoved.png", //Path for image once its been changed
  changeImageCmd: "push" // Command that signals item will need to be changed 
 },
 {
  itemId: "mouseP",
  itemName: "gray mouse",
  description: "This gray mouse seems be carrying a bobby pin",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "yellow cheese",
                    onStageCompleteDesc: "The mouse already disappeared...you can't find him anymore.",
                    onStageCompleteMessage: "The mouse dropped a bobby pin, then scattered away.",
                    postStageCmd: "pick up bobby pin"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroomBlank.png",
  changeImagePath: "images/newRooms/adultBedroom/adultBedroomMouseGone.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed  
 },
 {
  itemId: "bobbypinP",
  itemName: "bobby pin",
  description: "I bet this thing will be useful..",
    canBeHeld: true,//ue if change needed to 
  unActivated: false, //trobject, default false
  imagePath: "images/items/pinP.png" 
 },
 {
  itemId: "hatP",
  itemName: "red hat",
  description: "Something seems off about this, but you don't know what.",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "TEMP", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "The red hat looks normal now.",
                    onStageCompleteMessage: "You found a reel near the computer!",
                    postStageCmd: "pick up reel" //FINDME change to pick up reel
                }],
  imagePath: "" 
 },
 {
  itemId: "reelP",
  itemName: "reel",
  description: "something seems off about this, but you don't know what.",
    canBeHeld: false,
  unActivated: true,
  imagePath: "images/items/reel.png" 
 }
 ]);

//Doors - 29
db.doors.insert(
 [
 {
  doorId: "doorIJ",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //user gets this answer by reading their first note
    key: "upstream",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorJI",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password",//THIS CANNOT CHANGE, already on puzzle
    key: "tinyspider883", //usergets this from the back of their puzzle
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorJK",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //get this from a note
    key: "shale",
    onStageCompleteDesc: "The door is open"
  }]
 },{
  doorId: "doorKL",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //Received from Rock Paper Scissors
    key: "cubs789",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorLK",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received from clock hint
    key: "1:50",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorKJ",
  description: "This door has a hole for a key.",
  locked: true,
  doorStageList: 
  [{
    keyType: "key", //received from parrot
    key: "key kj",
    onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
  }]
 },
 {
  doorId: "doorLM",
  description: "You will definitely need some help for this - One of the past prisoners who has passed still roams the halls of Doris’s House. Rumor has it that they are actually a friendly ghost is just bored. Maybe if you play a game like 20 questions with them, they will help you get out.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //impress a guard 
    key: "owlet262",
    onStageCompleteDesc: "Although the Janitor normally would not help out prisoners for fear of getting Doris mad, it is said that they will do anything for certain candy. Maybe if you give them the correct candies they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //bribe a janitor
    key: "kisses",
    onStageCompleteDesc: "One of the previous prisoners still lingers around Doris’s House to this day. The previous prisoner has lost his mind from being trapped too long. Maybe if you help them remember who they are they will help you get out of this door.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //nickname of past prisoner  
    key: "terry dactul",
    onStageCompleteDesc: "One of the past prisoners who has passed still roams the halls of Doris’s House. Rumor has it that they are actually a friendly ghost is just bored. Maybe if you play a game like 20 questions with them, they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //talk to ghost
    key: "albert einstein",
    onStageCompleteDesc: "The door is FINALLY open"
  }]
 },
 {
  doorId: "doorMN",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received from rock paper scissors
    key: "calf131",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorNM",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //found in riddle in note
    key: "wargames",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorNO",
  description: "This door definitely takes a password. You should use the symbol '&' if you are going to list two things as the password.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //Found in pictograms in each room combolockbox
    key: "trail mix & side show", 
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorOP",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received in HTML
    key: "joeys101", //TODO MAKE SURE THIS MATCHES doorHG
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorPO",
  description: "This door has a space for a words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //from clue, picture in room
    key: "ada lovelace",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorON",
  description: "This door has a hole for a key.",
  locked: true,
  doorStageList: 
  [{
    keyType: "key", //received from lockbox that is opened by bobby pin
    key: "dusty key",
    onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
  }]
 },
 {
  doorId: "doorML",
  description: "You will definitely need some help for this - Although the Janitor normally would not help out prisoners for fear of getting Doris mad, it is said that they will do anything for certain candy. Maybe if you give them the correct candies they will help you get out.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //bribe a janitor
    key: "milky way",
    onStageCompleteDesc: "One of the previous prisoners still lingers around Doris’s House to this day. The previous prisoner has lost his mind from being trapped too long. Maybe if you help them remember who they are they will help you get out of this door.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //nickname of past prisoner  
    key: "chris p bacon",
    onStageCompleteDesc: "One of the past prisoners who has passed still roams the halls of Doris’s House. Rumor has it that they are actually a friendly ghost is just bored. Maybe if you play a game like 20 questions with them, they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //talk to ghost
    key: "floppy disk",
    onStageCompleteDesc: "Although the guard tries his best to keep order in Doris’s house, they have a tendency to let out celebrities that they “recognize”. Maybe if you sing them a song or act out a part of their favorite movie they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
  },
  {
    keyType: "password", //impress a guard 
    key: "piglet425",
    onStageCompleteDesc: "The door is FINALLY open"
  }]
 },
 {
  doorId: "doorPA",
  description: "This door doesn't even look like it is going to open any time soon...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //found in riddle in note
    key: "vending machine",
    onStageCompleteDesc: "You united the full team - now how to you get out? "
  }]
 },
 {
  doorId: "doorIH",
  description: "This door doesn't even look like it opens any more...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "RANDOM", //found in riddle in note
    key: "",
    onStageCompleteDesc: "user will never enter this door"
  }]
 }
 ]);


db.items.count();
db.userrooms.count();
db.doors.count();
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var path = require('path');
var Schema = mongoose.Schema;

/* Mongo Schemas and Collections */

//UserRoom
//potentially add ceilingDoor?
var userroomSchema = new Schema({ //combined to reduce calls to db
  roomId: String, //used to identify current user and room
  roomDescription: String, //returned on look room 
  roomLeftDoor: String, //left door for leaving the room
  roomRightDoor: String, //right door for leaving the room
  roomImagePath: String, //the image that this room is pointing to
  roomImageList: Array, // Array of images: 1. Safe closed, light on 2. Safe closed light off 3. Safe open light on 4. safe open light off
                        // if only one image, can be empty
                        // if multiple
  roomSafe: String, //identifies what item acts as the 'roomSafe'
  roomSafeOpen: Boolean, //if roomSafe is activated, then change this Variable for the room it is in 
  roomLightLetter: String,
  roomLightSwitch: Boolean,
  heavyItem: String,
  heavyItemMoved: Boolean,
  centerDoorGOpen: Boolean,
  itemsInRoom: Array, //list of items in the room so that user can't pick up item not in room
  linkedRooms: {type: Array, default: []}, //marks that the doors have come down between these rooms. 
  userName: String, //First name of user within this room
  userInventory: Array //inventory shown on front end for this room

});
var userrooms = mongoose.model('userrooms', userroomSchema, 'userrooms');
module.exports = userrooms;

//Door
var doorSchema = new Schema({
    doorId: String, //doorId : door<currRoomId>_<LinkedRoomId>
    doorStageList: [{ keyType: String, //if key, key item; if password, password
                      key: String, //identifies either password or key
                      onStageCompleteDesc: String,  //Description Changes to this after stage complete
                      onStageCompleteMessage: String, //Message sent to console when this stage is completed
                      postStageCmd: String }] , 
    currentStage: Number,//if undefined, assume   0 
    description: String, //returned on look <left|right> door; hint on what opens the door
    locked: Boolean, //determines if door has been opened
    onDoorUnlock: String
});
var doors = mongoose.model('doors', doorSchema, 'doors');
module.exports = doors;

//Item
var itemSchema = new Schema({
  itemId: String, //only used for admin to identify how items fit into puzzles
  itemName: String, //used by server to identify item and what user knows item as
  description: String, //returned when user looks at an item
  visible: Boolean, //determines if item currently hidden always TRUE
  canBeHeld: Boolean, // determines if a user can pick up an item 
  unActivated: Boolean, //allows one change to happen to item
  itemStageList: [{ keyType: String, //key, password OR look (eliminates activateCmd)
                    key: String, //if keyType=key, this is the key item. If keyType=password, this is the password
                    onStageCompleteDesc: String,
                    onStageCompleteMessage: String,
                    postStageCmd: String //command to execute if stage activated
                    }],
  currentStage: Number,
  imagePath: String, //image path for the front end to illustrate items here
  largeImagePath: String
});

var items = mongoose.model('items', itemSchema, 'items');
module.exports = items;

//TestLogs
// http://www.w3schools.com/js/js_dates.asp
// http://stackoverflow.com/questions/10520501/how-to-insert-a-doc-into-mongodb-using-mongoose-and-get-the-generated-id
var testLogSchema = new Schema({
    itemOrDoor: String, //item if itemLog, door if doorLog
    itemName: String,
    doorId: String,
    currentStage: Object,
    currentRoom: String,
    timeStamp: String
});

var testlogs = mongoose.model('testlogs', testLogSchema, 'testlogs');
module.exports = testlogs;


//Where to send commands
app.get('/', function(req, res){
//  res.sendFile(__dirname + '/studentIndex_socketIO.html');
  res.sendFile(__dirname + '/zstudentIndex2_withDB.html');
});

app.use(express.static(path.join(__dirname, 'public')));


/** Built in messages **/
var startMessage = "My name is Doris. I have trapped you and all the other visitors in your rooms. Please do not try to escape. Please do not type HELP for commands."
var helpMessage = "Get all the help you need. You will never get out!";
var backMessage = "Are you ready to try to work again finally?"
var defaultOpenedDoorMessage = "The door is open.";

var errorMessage = "I don't understand what you are trying to say.";
var cannotFindMessage = "I don't see that here";
var cannotFindItemMessage = "I don't see the thing you want to pick up here";
var cannotFindDoorMessage = "Which door are you talking about?";
var itemAlreadyHeldMessage = "You are already holding this item!";
var itemCannotBeHeldMessage = "You dummy - this item cannot be held.";
var unlockedDoorMessage = "You unlocked a door? How could you?!";
var doorAlreadyUnlocked = "The door is already unlocked.";
var failedUseMessage = "Nothing happened...";
var notHoldingMessage = "You cannot use an item that you aren't holding."
var roomsAreNotLinkedMessage = "You can't give items to someone if the door between you is locked.";
var userDoesNotExistMessage = "I don't know who you are talking about.";
var spiritList = '';
// var spiritHit

//Seven Woreded Question
//TODO ADD SEVEN WORDED QUESTION
//Who is the most awesome, smartest computer?
//If Doris


/** Server Actions  **/

//On Connection Actions
io.on('connection', function(socket){

  //Receive actions from the console input
  socket.on('action', function(input, roomId){
    
    //Callback functions
    var emitDorisText = function(response,roomId){
       dorisReaction = 'Doris';
       io.emit('doris', response, dorisReaction, roomId);
    };

    var emitRoomChange = function(imagePath,roomId){
      io.emit('room change', imagePath, roomId);
    };

    var emitInvChange = function (inventoryArray, input_roomId){
      var responseArr = [];
      emitInvHelper(responseArr, inventoryArray, input_roomId);  
    }

    var emitInvHelper = function(responseArr, inventoryArray, input_roomId){
      //this activates on conclusion
      if(responseArr.length==inventoryArray.length){
        io.emit('inventory change', responseArr, input_roomId);
      } else {
        //Add one item from array to response array
        var i = responseArr.length;
        items.find({itemName:inventoryArray[i]},function(error,items){
          var item = items[0];
          responseArr.push(item);
          emitInvHelper(responseArr,inventoryArray, input_roomId)
        });//end items query  
      }
    }

    var emitGivePopup = function(input_userName, input_itemName, targetRoomId){ 
      items.find({itemName:input_itemName},function(error,items){
        var item = items[0];
        io.emit('give message', input_userName, item, targetRoomId);
      });
    }

    //Send text to be handled by other functions
    input = input.toLowerCase();
    handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup);
  
  }); //end onAction
}); //end onConnection


//Function for handling a command
function handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup){
  var response = '';
  var dorisReaction = 'Doris';

  //Determine which action is being taken
  var verb = input.substring(0,input.indexOf(' '));
  if(verb==""){ //Check for one-word commands
    verb = input;
  }

  var response = '';
  var dorisReaction = 'Doris';
  switch(verb) {
  
    case "start": 
      onStartAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
      break;
    case "look":
      onLookAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
      break;
    case "pick": 
      onPickupAction(input,roomId,emitDorisText,emitInvChange);
      break;
    case "use":
      onUseAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
      break;  
    case "say":
      onSayAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
      break;
    case "give":
      onGiveAction(input,roomId,emitDorisText,emitInvChange,emitGivePopup);
      break;
    case "toggle":
      onToggleAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup);
      break;
    case "help":
      onHelpAction(input,roomId,emitDorisText,emitRoomChange);
      break;
    case "back":
      onBackAction(input,roomId,emitDorisText,emitRoomChange);
      break;
    case "push":
      onPushAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup);
      break;
    case "pull":
      onPullAction(input,roomId, emitDorisText, emitRoomChange, emitInvChange);
      break;
    // case "server":
      // callTestFunction(input, emitDorisText,emitGivePopup);
      // break;
    default: 
      response = errorMessage;
      dorisReaction = 'Confused Doris';
      emitDorisText(response,roomId);
  }

  return [response, dorisReaction];
}


function callTestFunction(input,emitDorisText, emitGivePopup) {
  
  var whereCenterDoor = {roomId: "L"};
  userrooms.find(whereCenterDoor,function(error,items){
    console.log(items);
  });

}

/** Functions for handling different Actions **/
function onStartAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange){
  var response = '';

  console.log(input_roomId);
  //check for correct input
  if(input=='start'){
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
      response = "Hello " + room.userName + ". " + startMessage;
      emitDorisText(response,input_roomId);
      emitRoomChange(room.roomImagePath,room.roomId);
      emitInvChange(room.userInventory, input_roomId);

    });
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
}//End OnStartAction

function onLookAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange){
  var response = "";

  //Pull thing from input
  var thing = input.substring(input.indexOf(' ')+1);
  var verb = input.substring(0,input.indexOf(' '));

  //Find Current Room
  userrooms.find({roomId:input_roomId},function(error,rooms){
    var room = rooms[0]; 
    if(rooms.length>1){ 
      console.log("ERROR: " + rooms.length + " rooms returned from currentRoom query."); 
      console.log("roomID: " + input_roomId);
      console.log("input: " + input);
    }
    if(room == undefined){
      console.log("ERROR: room is undefined"); 
      console.log("roomID: " + input_roomId);
      console.log("input: " + input);
    }

    //check for Scenario One: Look room
    if(thing.indexOf('room')!=-1) {
      //Return Description of the room
      response = room.roomDescription;
      emitDorisText(response,input_roomId);
    } 

    //check for Scenario Two: Look door
    else if (thing.indexOf('door')!=-1&&thing!="center door") {  
      if(thing=="left door") {
        //query the doorId of room.roomLeftDoor
        doors.find({doorId:room.roomLeftDoor},function(error,doors){
          var door = doors[0]; 
          //return the description of the left door
          response = door.description;
          emitDorisText(response,input_roomId);
        });//end door query
      } 

      else if(thing=="right door") {  
        //query the doorId of room.roomRightDoor
        doors.find({doorId:room.roomRightDoor},function(error,doors){
          var door = doors[0]; 
          //return the description of the right door
          response = door.description;
          emitDorisText(response,input_roomId);
        });//end door query
      } 
      //Improper Look Door command ERROR
      else if(thing!="center door") {  //center door handling for laundryRoomG
        response = cannotFindDoorMessage;
        emitDorisText(response,input_roomId);
      }
    } 
    //otherwise, Scenario Three: Look item
    else { 

      //Pull from Items Database
      items.find({itemName:thing},function(error,items){
        var item = items[0];

        //Confirm that object exists
        if(item==undefined){
          response = cannotFindMessage;
          emitDorisText(response,input_roomId);
        } 
        
        //Confirm that object is visible and is in room or inventory
        else if (room.itemsInRoom.indexOf(item.itemName)!=-1||room.userInventory.indexOf(item.itemName)!=-1){
          response = item.description;       
          emitDorisText(response,input_roomId);

          //Stage handling
          if(item.itemStageList!= undefined&&item.itemStageList.length>0&&item.unActivated==true){

            //Check for potential item activation
            var currentStage = item.currentStage;
            if(currentStage==undefined){
              currentStage = 0;
            }

            //To be activated: 
            //current stage activateCmd is look, item is not activated and then 
            if(item.itemStageList[currentStage].keyType=='look'){

              //Note that if look is the key, the onStageMessage is the first description
              activateItem(item, input_roomId,emitRoomChange,emitInvChange);
              response = item.itemStageList[currentStage].onStageCompleteMessage;
              if(response==undefined) {
                response = item.description;
              }
              emitDorisText(response,input_roomId);
            }               
          }
        } else {
          //Return response to client
          response = cannotFindMessage;
          emitDorisText(response,input_roomId);
        }
      }); //end of item.find()
    } //end of Scenario: look item    
  }); //end of userroom.find()
} //End of onLookAction

function onPickupAction(input, input_roomId, emitDorisText, emitInvChange) {
  var response = '';
  
  //Confirm second input is 'up'
  if (input.substring(5,7) != 'up'){
    emitDorisText(errorMessage,input_roomId);
  } else {  
    //Pull item ID from input
    var item_name = input.substring(input.indexOf('up ')+3);

    //Find Current Room
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];

      //Check if item is in room 
      if (room.itemsInRoom.indexOf(item_name)!=-1){
         
        //Get Item from db
        items.find({itemName:item_name},function(error,items){
          //(assumption is that the valid names are listed in initial doorList)
          var item = items[0];

          //check if canBeHeld
          if(item.canBeHeld!=true){
            response = itemCannotBeHeldMessage;
            emitDorisText(response,input_roomId);          
          } else {
            //Add item to user Inventory
            var updatedUserInv = room.userInventory;
            updatedUserInv.push(item.itemName);

            //Remove item from itemsInRoom List
            var roomItemsList = room.itemsInRoom;
            roomItemsList.remove(item.itemName);

            //Update Arrays in db
            var whereQuery = {roomId: room.roomId};
            var change = {userInventory: updatedUserInv, itemsInRoom: roomItemsList};
            userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

              //Send new inventory to the front end
              emitInvChange(updatedUserInv, input_roomId);
              
              //Send Doris Text to front end
              response = "You picked up the " + item.itemName;
              emitDorisText(response, input_roomId);

            });//end update userrooms
          }
        });//end items query
      }

      //Check if item is in inventory
      else if (room.userInventory.indexOf(item_name)!=-1){
        response = itemAlreadyHeldMessage;
        emitDorisText(response,input_roomId);
      } 
      //Item does not exist in room
      else {
        response = cannotFindItemMessage;
        emitDorisText(response,input_roomId);
      }
    }); //End userrooms query
  }
} //end onPickupFunction

/*
Two Use Scenarios: 

Use 'password' on door/item
Check if item/door and/or exists and/or activated already?
Check if password matches
if password matches, activate object / open door
  open door: connect two different rooms
  activate object: ??? on Activate action? 

use key on door/item
Check if item exists
Check if user has item that they're using 
Check if item/door and/or exists and/or activated already?
Check if item/door keyItem matches key
if key matches, activate object / open door
  oper door: connect two different rooms function openDoor()
  activate object: ??? on Activate action? function activateItem()
*/

function onUseAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange){ 
   //pull item/target from input
  var usedItem = input.substring(input.indexOf(" ")+1,input.indexOf(" on "));
  var targetName = input.substring(input.indexOf(" on ")+4);

  // console.log(usedItem);
  // console.log(targetName);


  //Pull current userroom from db
  userrooms.find({roomId:input_roomId},function(error,rooms) { 
      var room = rooms[0];

    //** If target is a door **//

      //if target is left or right door:
      if(targetName == 'left door'||targetName == 'right door') {
        var targetDoorId = '';
        //choose which door to use item on 
        if(targetName=='left door') {
          targetDoorId = room.roomLeftDoor;
        } else {
          targetDoorId = room.roomRightDoor;
        }
        //look up actual door
        doors.find({doorId:targetDoorId},function(error,doors){
          var door = doors[0];

          //Get Current Stage
          var currStage = door.currentStage;
          if(currStage==undefined){
            currStage = 0;
          }
          var doorStage = door.doorStageList[currStage];

          //if door is already unlocked ERROR
          if(door.locked!=true){
            response = doorAlreadyUnlocked;
            emitDorisText(response,input_roomId);
          }
          //assume user is using item
          //if user does not have item he/she is trying to use ERROR
          else if (room.userInventory.indexOf(usedItem)==-1){ 
            response = notHoldingMessage;
            emitDorisText(response,input_roomId);
          }
          //user has item he/she is trying to use
          else {
            //correct key
            if(usedItem == doorStage.key&&doorStage.keyType=='key'){
              openDoor(door,input_roomId);
              response = doorStage.onStageCompleteMessage;
              if(response==undefined){ //Check if onStageCompleteMessage was ever defined
                response = "You opened up the " + targetName + ".";
              }
              emitDorisText(response,input_roomId);
            }
            //incorrect key ERROR 
            else {
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
          }
        });//end door query
      }
      //if target is another door? send ERROR
      else if(targetName.indexOf(' door')!=-1&&targetName!="center door"){
        response = cannotFindDoorMessage;
        emitDorisText(response,input_roomId);
      }
    

      //** If target is an item **//
          
      else {
        items.find({itemName:targetName},function(error,items){
          var item = items[0];

          //If target item exists and is in current room
          if(item!=undefined&&item.itemStageList!=undefined&&(room.itemsInRoom.indexOf(item.itemName)!=-1||room.userInventory.indexOf(usedItem)!=-1)){
             //if item is already activate ERROR
            if(item.unActivated!=true){
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }

            //assume user is using item
            //if user does not have item he/she is trying to use ERROR
            else if (room.userInventory.indexOf(usedItem)==-1){ 
              response = notHoldingMessage;
              emitDorisText(response,input_roomId);
            }
            //user has item he/she is trying to use
            else {
              //Get Current Stage
              var currStage = item.currentStage;
              if(currStage==undefined){
                currStage = 0;
              }
              var itemStage = item.itemStageList[currStage];
              
              //correct key
              if(usedItem == itemStage.key&&itemStage.keyType=='key'){
                activateItem(item,input_roomId,emitRoomChange,emitInvChange);
                response = item.itemStageList[currStage].onStageCompleteMessage;
                emitDorisText(response,input_roomId);
              }
              //incorrect key ERROR 
              else {
                response = failedUseMessage;
                emitDorisText(response,input_roomId);
              }
            }
          }
          //Target Item does not exist or is not in room ERROR
          else {
            response = cannotFindItemMessage;
            emitDorisText(response,input_roomId);
          }
        });//end item query
      }
  });//end userroom query
} //end onUseAction()


function onSayAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange){
  //pull item/target from input
  var password = input.substring(input.indexOf(" ")+1,input.indexOf(" to "));
  var targetName = input.substring(input.indexOf(" to ")+4);

  //Pull current userroom from db
  userrooms.find({roomId:input_roomId},function(error,rooms) { 
      var room = rooms[0];

    //** If target is a door **//
      //if target is left or right door:
      if(targetName == 'left door'||targetName == 'right door') {
        var targetDoorId = '';
        //choose which door to use item on 
        if(targetName=='left door') {
          targetDoorId = room.roomLeftDoor;
        } else {
          targetDoorId = room.roomRightDoor;
        }
        //look up actual door
        doors.find({doorId:targetDoorId},function(error,doors){
          var door = doors[0];

          //Get Current Stage
          var currStage = door.currentStage;
          if(currStage==undefined){
            currStage = 0;
          }
          var doorStage = door.doorStageList[currStage];

          //if door is already unlocked ERROR
          if(door.locked!=true){
            response = doorAlreadyUnlocked;
            emitDorisText(response,input_roomId);
          }
          //if using password
          else if(doorStage.keyType =='password') {
            //password matches door password
            if(password == ("'" + doorStage.key + "'") ){
              openDoor(door,input_roomId, emitInvChange);
              response = doorStage.onStageCompleteMessage;
              if(response==undefined){ //Check if onStageCompleteMessage was ever defined
                response = "You opened up the " + targetName + ".";
              }
              emitDorisText(response,input_roomId);
            }
            //wrong password entered ERROR
            else {
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
          }
        });//end door query
      }
      //if target is another door? send ERROR
      else if(targetName.indexOf(' door')!=-1&&targetName!="center door"){
        response = cannotFindDoorMessage;
        emitDorisText(response,input_roomId);
      }

      //** If target is an item **//
          
      else {
        items.find({itemName:targetName},function(error,items){
          var item = items[0];
          //If target item exists and is in current room
          if(item!=undefined&&item.itemStageList!=undefined&&room.itemsInRoom.indexOf(item.itemName)!=-1){

            //Get Current Stage
            var currStage = item.currentStage;
            if(currStage==undefined){
              currStage = 0;
            }
            var itemStage = item.itemStageList[currStage];

             //if item is already activate ERROR
            if(item.unActivated!=true){
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
            //if using password
            else if(itemStage.keyType=='password') {
              //password matches door password

              if(password == ("'" + itemStage.key + "'") ){
                activateItem(item,input_roomId,emitRoomChange,emitInvChange);
                response = item.itemStageList[currStage].onStageCompleteMessage;
                emitDorisText(response,input_roomId);
              }
              //wrong password entered ERROR
              else {
                if(targetName=="keypad"&&password=="'doris'"){
                  response = "Why thank you! But you didn't think it would be that easy to solve the puzzle, did you? ";
                  emitDorisText(response,input_roomId);
                } else {
                  response = failedUseMessage;
                  emitDorisText(response,input_roomId);
                }
              }
            }
            //Target Item does not exist or is not in room ERROR
            else {
              response = cannotFindItemMessage; //TODO change to password-related message
              emitDorisText(response,input_roomId);
            }
          }
      });//end items query
    }
  });//end userroom query
}//End onSayAction

function onGiveAction(input, startRoomId, emitDorisText, emitInvChange, emitGivePopup) {
  var response = '';
  var input_roomId = startRoomId;

  //If input is NOT correctly formatted ERROR
  if (input.indexOf(' to ') == -1){
    emitDorisText(errorMessage,input_roomId);
  } 
  //If input IS correctly formatted
  else { 

    //pull item/target from input
    var givenItem = input.substring(input.indexOf(" ")+1,input.indexOf(" to "));
    var targetName = input.substring(input.indexOf(" to ")+4).toLowerCase();
    var targetPrintName = targetName.substring(0,1).toUpperCase()+targetName.substring(1); //Capitalize UserName for query

    //Pull current userroom from db
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var userroom = rooms[0];
      
      //confirm that user currently has input_item within userInventory / exists
      if(userroom.userInventory.indexOf(givenItem)!=-1){
        //Pull target userroom from db
        userrooms.find({userName:targetPrintName},function(error,rooms2){
          var userroom2 = rooms2[0];
          console.log(userroom2);
          console.log(targetName);
          //if user exists
          if (userroom2 != undefined) {

            //check if targetUserroom is linked to currentUserroom AND check if currentUserroom is linked to targetUserroom
            if(((userroom.linkedRooms.indexOf(userroom2.roomId)!=-1)&&(userroom2.linkedRooms.indexOf(userroom.roomId)!=-1))||givenItem=="money"){

              //remove item from currentUserroom userInventory
              var updatedUserInv = userroom.userInventory;
              updatedUserInv.remove(givenItem);

              //add item to targetUserroom userInventory 
              var updatedTargetInv = userroom2.userInventory;
              updatedTargetInv.push(givenItem);

              //Apply changes to db
              var whereQuery1 = {roomId: userroom.roomId};
              var change1 = {userInventory: updatedUserInv};

              var whereQuery2 = {roomId: userroom2.roomId};
              var change2 = {userInventory: updatedTargetInv};

              //send new Inventories to currentUserroom
              userrooms.findOneAndUpdate(whereQuery1, change1, function(err,rawResponse){
                emitInvChange(updatedUserInv, userroom.roomId);
                
                //Send Doris Text to front end
                response = "You gave away the " + givenItem;
                emitDorisText(response, input_roomId);
              });

              //send new Inventories to targetUserroom
              userrooms.findOneAndUpdate(whereQuery2, change2, function(err,rawResponse){
                 emitInvChange(updatedTargetInv, userroom2.roomId);
                 emitGivePopup(userroom.userName, givenItem, userroom2.roomId);
              });       
            } 
            //if both rooms aren't linked: ERROR
            else {
              //Hard-coded exception: Money can be given to anyone, because it is so small and easy to pass
              console.log("rooms not linked")
              response = roomsAreNotLinkedMessage;
              emitDorisText(response,input_roomId);
            }
          }
          //if user does not exist ERROR
          else {

            console.log("user doesnt exist");
            response = userDoesNotExistMessage;
            emitDorisText(response,input_roomId);
          }
        });//end of second userroom query
      } 
      //if item not in inventory or does not exist ERROR
      else {
        response = notHoldingMessage;
        emitDorisText(response,input_roomId);
      }
    });//End initial userroom query
  }
} //End onGiveAction

var noLightSwitchMessage = "There's no light switch in this room to toggle.";
//TODO Hardcode logic for changing lights in Stage 4 Task 1
function onToggleAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup) {
  var response = '';

  //Hard-coded values
  var roomsWithLightBulbs = ['A','B','C','D','E','F'];
  var resetLightsRoom = 'G';
  var changesGarageLightRoom = 'A';
  var garageRoomId= 'B';

  //Accept one of two inputs
  if(input=='toggle light'||input=='toggle light switch') {
    
    //pull current room from db query
     userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];

          if(roomsWithLightBulbs.indexOf(input_roomId)!=-1){

          var changeLightStatus = true; 
          var lightStatus = "on"; //assume light is off, so we are turning it on
          if(room.roomLightSwitch==true){ //but if light is on, turn it off.
            changeLightStatus = false;
            lightStatus = "off"
          }         

          //Change Room State if necessary
          var img = switchRoomCheck(room, room.roomSafeOpen, changeLightStatus,room.heavyItemMoved);

          if(room.imagePath!=img){
            //Change Light Status/Room
            var whereQuery = {roomId: room.roomId};
            var change = {roomImagePath: img, roomLightSwitch:changeLightStatus, roomImagePath:img}; //Presuming here that no objects need to be removed from roomsList
            
            userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

                response = "You turned " + lightStatus + " the light.";
                emitDorisText(response, input_roomId);
                emitRoomChange(img,input_roomId);
                spiritList += room.roomLightLetter;

              //Scenario Canceled.
              // //Hard-coded scenario to the differences between closet turning off garage
              // if(input_roomId==changesGarageLightRoom){
              //     userrooms.find({roomId:garageRoomId},function(error,rooms){
              //       var room2 = rooms[0];

              //       var garageImg = switchRoomCheck(room2, !(changeLightStatus), room2.roomLightSwitch);
              //       var whereQuery = {roomId: garageRoomId};
              //       var change = {roomImagePath: img, roomSafeOpen:(!changeLightStatus), roomImagePath:garageImg}; //Presuming here that no objects need to be removed from roomsList

              //       userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
              //         emitRoomChange(garageImg,garageRoomId);
              //       });
              //   });

              // } //End hard-coded scenario

              //Hard-coded Scenario, checking for opening the door on completion of the SPIRIT Lightbulb challenge
              if(spiritList=='SPIRIT'){
                console.log('SPIRIT');
                var pickUpHammerAction = "pick up hammer";
                var specificRoom = "H";
                //pickup hammer here
                onServerPickUpAction(pickUpHammerAction, specificRoom, emitInvChange,emitGivePopup);
                
                // change room value and room image
                var whereQuery = {roomId:specificRoom};
                var change = {roomImagePath: img, roomLightSwitch:changeLightStatus, roomImagePath:img}; //Presuming here that no objects need to be removed from roomsList
            
                userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){


                });

              }
            });//end userroom query
          } 
          
       } 

      //No Light Switch available to toggle ERROR
      else {
        response = noLightSwitchMessage;
        emitDorisText(response, input_roomId);
      }
    });//end userroom query
  } 
  //Invalid input ERROR
  else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onToggleAction

var firstPullDoorCommand = "You tried to pull on the door, but were unable to do so on your own. Try teamwork! If someone's room is connected to yours, you may pull on the door using 'pull center door with A,B,C', where A, B, and C are your friend's names."
var notEnoughHelpMessage = "You and your friends tried to pull the door down, but your combined strength was not enough. You'll need to get more friends to help you pull this door down.";
var personDoesNotExistMessage = "I don't think the friend you claim is helping you pull the door down even exists. Are they stuck in another room?";
var failedPullMessage = "Nothing happened when you tried to pull.";

//This function exists only for certain scenarios:   
function onPullAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange) {
  var pulledItem = input.substring(input.indexOf(" ")+1,input.indexOf(" with "));
  var helpfulFriends = input.substring(input.indexOf(" with ")+6);
  helpfulFriends = helpfulFriends.replace(" ","");
  var helpfulFriendsList = helpfulFriends.split(",");
  for(var i = 0; i < helpfulFriendsList.length; i ++){
    helpfulFriendsList[i] = helpfulFriendsList[i].replace(" ","");
  }
  //TODO G - Pull on center door with 7 people's names
  if(input_roomId=='G'&&input=='pull center door'){
    emitDorisText(firstPullDoorCommand,input_roomId);
  } else if(input_roomId=='G'&&pulledItem=='center door'){
      var roomsList = [];
      var whereCenterDoor = {itemName: "center door"};
      items.find(whereCenterDoor,function(error,itemsW){
        var item = itemsW[0];
        
        if(item.currentStage!=undefined&&item.currentStage==1){
                    
          // pull center door with Amy, Brian, Carlos, Dean, Elena, Frank, Hannah
          if(helpfulFriendsList.indexOf("amy")!=-1&&helpfulFriendsList.indexOf("brian")!=-1&&helpfulFriendsList.indexOf("carlos")!=-1&&helpfulFriendsList.indexOf("dean")!=-1&&helpfulFriendsList.indexOf("elena")!=-1&&helpfulFriendsList.indexOf("frank")!=-1&&helpfulFriendsList.indexOf("hannah")!=-1) {
            var whereQuery = {roomId: input_roomId};
            var change = {roomImagePath: "images/rooms/laundryRoomDoorOpen.png", centerDoorGOpen: true}; //Presuming here that no objects need to be removed from roomsList
            userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
              // var room = rooms[0];

              var whereCenterDoor = {itemName: "center door"};
              items.find(whereCenterDoor,function(error,itemz){
                var item = itemz[0];

                var change2 = {description: item.itemStageList[item.currentStage].onStageCompleteDesc}; //Presuming here that no objects need to be removed from roomsList
                items.findOneAndUpdate(whereCenterDoor, change2, function(err,rawResponse){
                  emitDorisText(item.itemStageList[item.currentStage].onStageCompleteMessage, input_roomId);
                  emitRoomChange("images/rooms/laundryRoomDoorOpen.png",input_roomId);
                });
              });//item query
            }); 

          } else if (helpfulFriendsList.length<7) {
            response = notEnoughHelpMessage;
            emitDorisText(response,input_roomId);
          }
          else {
            response = personDoesNotExistMessage;
            emitDorisText(response,input_roomId); 
          }

        }
      });
      
  }
  //TODO C - pull on other user's favorite book
  else if (input_roomId=='C'&&input.indexOf("from bookshelf"!=-1)) {
    var bookPulled = input.substring(input.indexOf(" ")+1,input.indexOf(" from bookshelf"));
    var correctBook = "harry potter";
    
    if(bookPulled==correctBook) { //hardcoded for now
      
      items.find({itemName:bookPulled},function(error,itemsList){
        var item = itemsList[0];
        if(item.unActivated==true){
          activateItem(item, input_roomId, emitRoomChange, emitInvChange);
          emitDorisText(item.itemStageList[0].onStageCompleteMessage,input_roomId);      
        } else {
          emitDorisText(item.description,input_roomId);      
        }

      });
    } 
    else{
      response = failedPullMessage;
      emitDorisText(response,input_roomId);      
    }
  }
  else{
    response = failedPullMessage;
    emitDorisText(response,input_roomId); 
  }

}//end onPullAction

//May hard code this function instead

// function checkPulledDoorHelper(friendsList, currentList,input_roomId, emitDorisText, emitRoomChange){
//   if((currentList.length)==friendsList.length){
//       if(friendsList.length<7){
//         response = notEnoughHelpMessage;
//         emitDorisText(response,input_roomId);
//       } 
//       else { //pull center door with Amy, Brian, Carlos, Dean, Elena, Frank, Hannah
//         console.log("ALL SEVEN YAY");
//         //Post Pull Door Logic
//         // centerDoorGOpen: Boolean,
//         var whereQuery = {roomId: input_roomId};
//         var change = {roomImagePath: "images/rooms/laundryRoomDoorOpen.png", centerDoorGOpen: true}; //Presuming here that no objects need to be removed from roomsList
//         userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
//           var room = rooms[0];

//           var whereCenterDoor = {itemName: "center door"};
//           items.find(whereCenterDoor,function(error,itemz){
//             var item = itemz[0];

//             var change2 = {description: item.stage[1].onStageCompleteDesc}; //Presuming here that no objects need to be removed from roomsList
//             items.findOneAndUpdate(whereCenterDoor, change2, function(err,rawResponse){
//               emitDorisText(item.stage[1].onStageCompleteMessage, input_roomId);
//               emitRoomChange("images/rooms/laundryRoomDoorOpen.png",input_roomId);
//             });

//           });//item query

//         });
//     }
//   } 
//   else {
//       console.log(currentList);
//       var testName = friendsList[currentList.length]; 
//       var testPrintName = testName.substring(0,1).toUpperCase()+testName.substring(1); //Capitalize UserName for query
//       userrooms.find({userName:testPrintName},function(error,rooms){
//         var room = rooms[0];

//         if(room!=undefined){
//           //second if
//           if(room.linkedRooms.indexOf(input_roomId)!=-1){
//             currentList.push(room.roomId);
//             checkPulledDoorHelper(friendsList, currentList, input_roomId, emitDorisText, emitRoomChange);
//           } else {
//             friendsList[currentList.length]=='';
//           }
//         } else {
//           response = personDoesNotExistMessage;
//           emitDorisText(response,input_roomId); 
//         }

//       });//end roomsListQuer

//   }
// }

var morePushDetailsNeededMessage = "What are you trying to push?";
var buttonAPressedMessage = "Something happened when you pressed the button, but you can't tell what";
var pushCouchAloneButtonMessage = "You tried to push, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'push couch with NAME' where name is your friend's first name!";
var pushFridgeAloneButtonMessage = "You tried to push, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'push fridge with NAME' where name is your friend's first name!";
var pushCabinetAloneButtonMessage = "You tried to push, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'push cabinet with NAME' where name is your friend's first name!";
var pushDeskAloneButtonMessage = "You tried to push, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'push desk with NAME' where name is your friend's first name!";
var whoAreYouPushingWithMessage = "I don't think that person's room is connected to yours...if they even exist!'"
var failedPushMessage = "Nothing happened when you pushed that.";

function onPushAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup) {
  var response = '';
  //room does have a heavy object
  var roomsWithHeavyObjectsList = ["C","F","K","P"];

  //Determine what someone is trying to push
  var tmp = input.substring(input.indexOf(' ')+1);
  var objectPushed = tmp.substring(0,tmp.indexOf(' '));
  var friend = input.substring(input.indexOf("with ")+5);
  if(objectPushed==''){
    objectPushed = tmp;
  }


  if(roomsWithHeavyObjectsList.indexOf(input_roomId)!=-1){

    var heavyThing = '';
    //Case switch over expected heavy object
    switch(input_roomId) {
    case "C": 
      heavyThing = "couch";
      response = pushCouchAloneButtonMessage;
      break;
    case "F":
      heavyThing = "fridge";
      response = pushFridgeAloneButtonMessage;
      break;
    case "K":
      heavyThing = "cabinet";
      response = pushCabinetAloneButtonMessage;
      break;
    case "P":
      heavyThing = "desk";
      response = pushDeskAloneButtonMessage;
      break;
    }

    if(input=='push'){
      response = morePushDetailsNeededMessage;
      emitDorisText(response, input_roomId);
    }  
    else if(input==("push "+heavyThing)){
      emitDorisText(response, input_roomId);
    } 
    else if(objectPushed==heavyThing){
      //find friends room to confirm their id
      userrooms.find({roomId:input_roomId},function(error,rooms){
        var selfRoom = rooms[0];

        if(selfRoom.heavyItemMoved==true) {

          response = failedPushMessage;
          emitDorisText(response,input_roomId);            
        } else {
          //find friends room to confirm their id
          userrooms.find({userName:friend},function(error,friendRooms){
            var friendRoom = friendRooms[0];

            //find item being push to determine which friend needs to help push it
            items.find({itemName:heavyThing},function(error,items){
              var item = items[0];

              // console.log(friendRoom);
              // console.log(selfRoom.linkedRooms.indexOf(friendRoom.roomId)==-1);
              // console.log(friendRoom.linkedRooms.indexOf(selfRoom.roomId)==-1);
              //Check that that person exists and that you are connected to them and that they are connected to you
              if(friendRoom==undefined||selfRoom.linkedRooms.indexOf(friendRoom.roomId)==-1||friendRoom.linkedRooms.indexOf(selfRoom.roomId)==-1){
                response = whoAreYouPushingWithMessage;
                emitDorisText(response,input_roomId);
              }

              else {
                // console.log(friendRoom.userName);
                //determine currentStage
                stageNum = item.currentStage;
                if(stageNum==undefined){
                  stageNum = 0;
                }

                //Not checking for number of people because only one person really can help here
                //update user room to reflect this change
                var itemMovedImage = switchRoomCheck(selfRoom,selfRoom.roomSafeOpen,selfRoom.roomLightSwitch,true); 
                // console.log(itemMovedImage);
                var whereQuery = {roomId: input_roomId};
                var change = {roomImagePath: itemMovedImage, heavyItemMoved: true}; //Presuming here that no objects need to be removed from roomsList
                userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
                  activateItem(item, input_roomId, emitRoomChange, emitInvChange);
                  emitDorisText(item.itemStageList[stageNum].onStageCompleteMessage, input_roomId);
                  emitRoomChange(itemMovedImage, input_roomId);  
                });
              }

            });
          });//end friend query
        }

      });
    }//End if works check
    
  }

   //A - Button being pressed resets lights in rooms A,B,C,D,E,F
  else if(input_roomId=='A'&&input=='push button'){

    //Room A Reset
    userrooms.find({roomId:"A"},function(error,rooms){
      roomA = rooms[0];      
      var imageA = switchRoomCheck(roomA,roomA.roomSafeOpen,false,roomA.heavyItemMoved);
      var whereA = {roomSafeOpen: roomA.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageA}

      userrooms.findOneAndUpdate({roomId: "A"},whereA, function(err,raw){
          emitRoomChange(imageA,"A");
      });
    });

    //Room B Reset
    userrooms.find({roomId:"B"},function(error,rooms){
      roomB = rooms[0];      
      var imageB = switchRoomCheck(roomB,roomB.roomSafeOpen,false,roomB.heavyItemMoved);
      var whereB = {roomSafeOpen: roomB.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageB}

      userrooms.findOneAndUpdate({roomId: "B"},whereB, function(err,raw){
          emitRoomChange(imageB,"B");
      });
    }); 

    //Room C Reset
    userrooms.find({roomId:"C"},function(error,rooms){
      roomC = rooms[0];      
      var imageC = switchRoomCheck(roomC,roomC.roomSafeOpen,false,roomC.heavyItemMoved);
      var whereC = {roomSafeOpen: roomC.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageC}

      userrooms.findOneAndUpdate({roomId: "C"},whereC, function(err,raw){
          emitRoomChange(imageC,"C");
      });
    });

    //Room D Reset
    userrooms.find({roomId:"D"},function(error,rooms){
      roomD = rooms[0];      
      var imageD = switchRoomCheck(roomD,roomD.roomSafeOpen,false,roomD.heavyItemMoved);
      var whereD = {roomSafeOpen: roomD.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageD}

      userrooms.findOneAndUpdate({roomId: "D"},whereD, function(err,raw){
          emitRoomChange(imageD,"D");
      });
    });

    //Room E Reset
    userrooms.find({roomId:"E"},function(error,rooms){
      roomE = rooms[0];      
      var imageE = switchRoomCheck(roomE,roomE.roomSafeOpen,false,roomE.heavyItemMoved);
      var whereE = {roomSafeOpen: roomE.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageE}

      userrooms.findOneAndUpdate({roomId: "E"},whereE, function(err,raw){
          emitRoomChange(imageE,"E");
      });
    });

     //Room F Reset
    userrooms.find({roomId:"F"},function(error,rooms){
      roomF = rooms[0];      
      var imageF = switchRoomCheck(roomF,roomF.roomSafeOpen,false,roomF.heavyItemMoved);
      var whereF = {roomSafeOpen: roomF.roomSafeOpen, roomLightSwitch: false, roomImagePath:imageF}

      userrooms.findOneAndUpdate({roomId: "F"},whereF, function(err,raw){
          emitRoomChange(imageF,"F");
      });
    });

    //send message back to console
    emitDorisText(buttonAPressedMessage,input_roomId);

    //Reset spiritList
    spiritList = '';
  }

  else {
    response = failedPushMessage;
    emitDorisText(response,input_roomId);
  }
    
}//end onPushAction

function onHelpAction(input, input_roomId, emitDorisText, emitRoomChange) { 
  var response = '';
  var helpImagePath = "images/commands.png";

  //check for correct input
  if(input=='help'){
    emitDorisText(helpMessage,input_roomId);
    emitRoomChange(helpImagePath,input_roomId);
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onHelpAction

function onBackAction(input, input_roomId, emitDorisText, emitRoomChange) { 
  var response = '';

  //check for correct input
  if(input=='back'){
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
      response = backMessage;
      emitDorisText(response,input_roomId);
      emitRoomChange(room.roomImagePath,room.roomId);  
    }); //end room query
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onBackAction


//Function created to pick up actions after item command
function onServerPickUpAction(server_input, input_roomId, emitInvChange, emitGivePopup){
  //Pull item ID from input
  var input = server_input;
  var item_name = input.substring(input.indexOf('up ')+3);

  //Get current room
  userrooms.find({roomId:input_roomId},function(error,rooms){
    var room = rooms[0];
    //Get Item from db
    items.find({itemName:item_name},function(error,items){
      //(assumption is that the valid names are listed in initial doorList)
      var item = items[0];

      //check if item exists DATA ERROR
      if(item==undefined) {
        console.log("DATA ERROR ON SERVER PICKUP: " + server_input);
      }
       else {
        //Add item to user Inventory
        var updatedUserInv = room.userInventory;
        updatedUserInv.push(item.itemName);

        //Update Arrays in db
        var whereQuery = {roomId: room.roomId};
        var change = {userInventory: updatedUserInv}; //Presuming here that no objects need to be removed from roomsList
        userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

          //Send new inventory to the front end 
          emitInvChange(updatedUserInv, input_roomId);  

          if(input_roomId=='H'&&item.itemName=="hammer") {
            emitGivePopup("Doris", item.itemName, "H");
          }
        });
      }
    });//end item query
  });//end userroom query
} //endServerPickup


// (Assumed: the item being passed into this function is activated now)
//    Set activated = true
//    set description to activated description
//    run onActivateItem command

function activateItem(item, input_roomId, emitRoomChange, emitInvChange){
  //Hard-coded scenario
  var specialKeypadK = "keypad";
  var specialTopHatJ = "top hat";

  //Handle Item Stages
  var stageNum = item.currentStage;
  var activationSet = item.unActivated;
  var itemDescription = item.description;
  if(stageNum==undefined){
    stageNum = 0;
  }

  //Advance to final stage
  if(stageNum==(item.itemStageList.length-1)){
    //item is activated
    activationSet = false;
    itemDescription = item.itemStageList[stageNum].onStageCompleteDesc;
    if (itemDescription==undefined) {
      itemDescription = item.description;
    }
    stageNum += 1;

    //If item is a room safe, checkOpenSafe:
    checkOpenSafe(item.itemName,input_roomId,emitRoomChange);
  } 

  //item not yet activated, advance to next stage
  else {
    activationSet = true;
    itemDescription = (item.itemStageList[stageNum]).onStageCompleteDesc;
    if (itemDescription==undefined) {
      itemDescription = item.description;
    }
    stageNum += 1;
  }

  //update db function
  var whereQuery = {itemName: item.itemName};
  var change = {unActivated: activationSet, currentStage: stageNum, description: itemDescription};
  items.findOneAndUpdate(whereQuery, change, function(err,rawResponse) {
    //insert testlog
    var log = new testlogs({
                          itemOrDoor: "item",
                          itemName: item.itemName,
                          currentStage: item.itemStageList[stageNum-1],
                          currentRoom: input_roomId,
                          timeStamp: (new Date()).toString()
                          });
    log.save(function(err,logSaved){});

    //handle postStageCmd
    var server_input = item.itemStageList[stageNum-1].postStageCmd;
    if(server_input!=undefined&&server_input!=''&&server_input!=null){
      //assume server_input is PickupAction
      onServerPickUpAction(server_input, input_roomId, emitInvChange);

      //handling specialKeypad4k scenario
      if(item.itemName==specialKeypadK){


        var whereQuery = {itemName: specialTopHatJ};
        var changeStage = {
                    keyType: "look", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "I think the hat is actually empty this time.",
                    onStageCompleteMessage: "You found a fishing rod? How did that fit in a hat?!",
                    postStageCmd: "pick up fishing rod"
                 };
        var change = {itemStageList: changeStage};
        items.findOneAndUpdate(whereQuery, change, function(err,rawResponse){});
       }
    }
  }); //endItemQuery

} //End on activateItem



//(Assumed: conditions to open door have been met)
// set locked = false
// set description to unlocked description
// run postStageCmd cmd
//say 'kittens123' to left door
function openDoor(door, input_roomId, emitInvChange){

  //Handle Door Stages
  var stageNum = door.currentStage;
  var lockedSet = door.locked;
  var doorDescription = door.description;
  if(stageNum==undefined){
    stageNum = 0;
  }

  //Advance to final stage
  if(stageNum==(door.doorStageList.length-1)){
    //door is unlocked
    
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
    
      //add door to linkedRooms
      var newRoom = door.doorId.substring(door.doorId.length-1);
      var updatedLinkedRooms = room.linkedRooms;
      if(updatedLinkedRooms==undefined) {
        updatedLinkedRooms = [];
      }

      userrooms.find({roomId:newRoom},function(error,rooms2){
        room2 = rooms2[0];
        connectedRoomList = room2.linkedRooms;
        for(var i = 0; i < connectedRoomList.length; i++){
           //Only add new room to list if it isn't there now
           if(updatedLinkedRooms.indexOf(connectedRoomList[i])==-1){
            updatedLinkedRooms.push(connectedRoomList[i]);
           }
        }
        var whereQuery = {roomId: input_roomId};
        var change = {linkedRooms: updatedLinkedRooms};
        userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){});
      });

    });
  
    //set locked to false
    lockedSet = false;
    doorDescription = door.doorStageList[stageNum].onStageCompleteDesc;
    stageNum+=1;
  } 

  //door not yet unlocked, advance to next stage
  else {
    lockedSet = true;
    doorDescription = (door.doorStageList[stageNum]).onStageCompleteDesc;
    updatedLinkedRooms = [];
    stageNum += 1;
  }

  if(doorDescription==undefined){
      doorDescription = door.description;
  }

  //update db function
  var whereQuery = {doorId: door.doorId};
  var change = {locked: lockedSet, currentStage: stageNum, description: doorDescription};
  doors.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
    //insert testlog
    var log = new testlogs({
                          itemOrDoor: "door",
                          doorId: door.doorId,
                          currentStage: door.doorStageList[stageNum-1],
                          currentRoom: input_roomId,
                          timeStamp: (new Date()).toString()
                          });
    log.save(function(err,logSaved){});
    console.log(door.doorId);

    //handle postStageCmd
    var server_input = door.doorStageList[stageNum-1].postStageCmd;
    if(server_input!=undefined&&server_input!=''&&server_input!=null){
      //assume server_input is PickupAction
      onServerPickUpAction(server_input, input_roomId, emitInvChange);
    }
  });
}//end openDoor 

//For changing the room 
function checkOpenSafe(itemName, input_roomId, emitRoomChange){

  //first find currentRoom
  userrooms.find({roomId:input_roomId},function(error,rooms){
    var room = rooms[0];

    //if item is roomSafe and the roomSafe is locked!
    if(room.roomSafe==itemName&&room.roomSafe){
      var newRoomImage = switchRoomCheck(room,true,room.roomLightSwitch,room.heavyItemMoved);
      var whereQuery = {roomId: input_roomId};
      var change = {roomSafeOpen: true, roomImagePath: newRoomImage};

      userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){});
        if(room.roomImagePath!=newRoomImage){
          emitRoomChange(newRoomImage, input_roomId);
        }
      }
  });//end userroom query
}

//returns, based on the current room conditions, which roomImage should be visible
function switchRoomCheck(room,roomSafeOpen,roomLightSwitch,heavyItemMoved){

  //if room only has one image
  if(room.roomImageList.length==1){
    return room.roomimagePath;
  } 
   //include door logic for if G uses the centerDoor
  else if(room.centerDoorGOpen==true) {
    return "images/rooms/laundryRoomDoorOpen.png";
  }
  //room has multiple potential stages
  else {

   //for garage/B: roomSafeOpen is true when not dark, false when dark

   // Array of images: 1. Safe closed, light off 2. Safe closed light on 3. Safe open light off 4. safe open light on
   var imageArray = room.roomImageList;

//   ["images/rooms/closet.png","images/rooms/closetOn.png","images/rooms/closetOpenOff.png","images/rooms/closetOpenOn.png"]
    //[lightOff-heavyItemInPlace-Safeclosed, 
    // lightOn-heavyItemInPlace-Safeclosed, 
    // lightoff-heavyItemMoved-Safeclosed,
    // lightOn-heavyItemMoved-safeclosed,
    // lightOff-heavyItemMoved-SafeOpened,
    // lightOn-heavyItemMoved-SafeOpened]

//OLD VERSION
   //Light off, heavy item not moved, room safe is locked
   if(roomLightSwitch==false&&heavyItemMoved==false&&roomSafeOpen==false){
      return room.roomImageList[0];
   } 
   //Light on, heavy item not moved, room safe is locked
   else if(roomLightSwitch==true&&heavyItemMoved==false&&roomSafeOpen==false){
      return room.roomImageList[1];
   }
   //Light off, heavy item moved, room safe is locked 
   else if(roomLightSwitch==false&&heavyItemMoved==true&&roomSafeOpen==false){
      return room.roomImageList[2];
   }
   //Light on, heavy item moved, room safe is locked
   else if(roomLightSwitch==true&&heavyItemMoved==true&&roomSafeOpen==false){
      return room.roomImageList[3];
   }
   //Light off, heavy item moved, room safe is open
   else if(roomLightSwitch==false&&heavyItemMoved==true&&roomSafeOpen==true){
      return room.roomImageList[4];
   }
   //Light on, heavy item moved, room safe is open
   else { //if(roomLightSwitch==true&&heavyItemMoved==true&&roomSafeOpen==true){
      return room.roomImageList[5];
   }

  }
} //end SwitchRoomCheck

/*Start Server*/ 
http.listen(3774, function(){
  console.log('listening on *:3774');
//  mongodb://localhost/trapped<use trapped; on mongo>/schema<collection>_name 
  mongoose.connect('mongodb://localhost/trapped_db');
});

//Reset database
db.items.drop();
db.userrooms.drop();
db.doors.drop();  
db.testlogs.drop();
db.createCollection("items");
db.createCollection("userrooms");
db.createCollection("doors");
db.createCollection("testlogs");

//UserRooms - 16
//TODO ADD All roomSafes
db.userrooms.insert(
 [{	
 //1a: use password from physical jigsaw puzzle on doorA_B

	roomId: "A",
	roomDescription: "You seem to be trapped in a large walk-in closet. Who needs this much space? You see a secure safe, a light bulb, a switch, a fine dresser, some fancy hanging clothes, a cardobard box, an expensive purse, and most importantly a strange button that is begging to be pushed. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorA_P", 
	roomRightDoor: "doorA_B",
	roomImagePath: "images/rooms/closet.png",
	roomImageList: ["images/rooms/closet.png","images/rooms/closetOn.png","images/rooms/closetOpenOff.png","images/rooms/closetOpenOn.png"],
	roomSafe: "secure safe",
	roomSafeLocked:  true, //if safe exists, this is true
	roomLightLetter: "P",
	roomLightSwitch: false, //by default, light is off and this is true
	linkedRooms: [],
	itemsInRoom: ["strange button", "spiffy charger", "secure safe"],
	userName: "UserA",
	userInventory: ["note 1A","note 3A-3"]
 },
 {
 //1b: use password from winning rock paper scissors on doorB_A
	roomId: "B",
	roomDescription: "As you look around you surpress a slight shiver. It is quite cold in here. You see a lightbulb, a switch, some type of old car with small cupboards above it, an open paint can,  and a red chest of drawers with a handy toolbox on top of it. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorB_C", 
	roomRightDoor: "doorB_A",
	roomImagePath: "images/rooms/garage.png",
	roomImageList: ["images/rooms/garage.png","images/rooms/garageOn.png","images/rooms/garageDarkOff.png","images/rooms/garageOnDark.png"],
	// roomSafe: "",
	roomSafeLocked:  true, //for this, true if Normal, false is Dark
	roomLightSwitch: false, //by default, light is off and this is true
	roomLightLetter: "S",
	itemsInRoom: ["panelB"],
	linkedRooms: [],
	userName: "userB",
	userInventory: ["note 1B", "note 2B", "note 3B-1"]
 },{
	roomId: "C",
	roomDescription: "You find yourself trapped in some sort of library. You see a lightbulb, a switch, two red bookshelves, a yellow cup, a stack of books, and an uncomfortable looking couch. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorC_D", 
	roomRightDoor: "doorC_B",
	roomImagePath: "images/rooms/library.png",
	roomImageList: ["images/rooms/library.png","images/rooms/libraryOn.png","images/rooms/library.png","images/rooms/libraryOn.png"],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "I",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["bookshelf C"],
	linkedRooms: [],
	userName: "userC",
	userInventory: ["note 1c","note 3C-4"]
 },
 {
	roomId: "D",
	roomDescription: "You find yourself in a small dining room with a light fixture, a lightbulb, switch, oak table, oak chairs, a red cupboard, a brown cup, and a beautiful painting. The figure in the painting looks familiar to you. The only ways out that you can see are two doors on either side of the room. ",
	roomLeftDoor: "doorD_E", 
	roomRightDoor: "doorD_C",
	roomImagePath: "images/rooms/dining.png",
	roomImageList: ["images/rooms/dining.png","images/rooms/diningOn.png","images/rooms/dining.png","images/rooms/diningOn.png"],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "R",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["yummy cheese"],
	linkedRooms: [],
	userName: "userD",
	userInventory: ["note 1D", "note 2D","note 3D-2"]
 },
  {
	roomId: "E",
	roomDescription: "You find yourself in a clean office with a light bulb, a switch, a small desk, a fast computer, a gray trashcan, colorful books, and a face clock. The clock does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorE_F", 
	roomRightDoor: "doorE_D",
	roomImagePath: "images/rooms/office.png",
	roomImageList: ["images/rooms/office.png","images/rooms/officeOn.png","images/rooms/office.png","images/rooms/officeOn.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "S",	
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["panel e"],
	linkedRooms: [],
	userName: "userE",
	userInventory: ["note1E","note 1E-2","note 3E-4"]
 },
 {
	roomId: "F",
	roomDescription: "You find yourself in a retro looking kitchen. There is a lightbulb, a switch, an electric oven, kitchen cupboards, some kitchen drawers, and a fridge with magnets on it. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorF_G", 
	roomRightDoor: "doorF_E",
	roomImagePath: "images/rooms/kitchen.png",
	roomImageList: ["images/rooms/kitchen.png","images/rooms/kitchenOn.png","images/rooms/kitchen.png","images/rooms/kitchenOn.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "T",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["heavy desk", "plain safe"],
	linkedRooms: [],
	userName: "userF",
	userInventory: ["note 1F","note 3F-3"]
 },
 {
	roomId: "G",
	roomDescription: "You find yourself in a strange room with two large machines in them. You think they may be used to clean clothes but you are not sure how. Your mother always does it for you anyway. You also see a light bulb, a safe, some shelves with clothes, a box, and a thing you are pretty sure is called an ironing board. There are three doors in this room, one on either side of the room and a center door  which looks like it has been nailed down.",
	roomLeftDoor: "doorG_H", 
	roomRightDoor: "doorG_F",
	roomImagePath: "images/rooms/laundryRoom.png",
	roomImageList: ["images/rooms/laundryRoom.png","images/rooms/laundryRoom.png","images/rooms/laundryRoom.png","images/rooms/laundryRoom.png"],
	roomSafe: "box",
	roomSafeLocked:  false, //For this Object, the safe is 'box'
	roomLightSwitch: false, //For this Object, the light is the center door
	itemsInRoom: ["charger g", "safe g","center door","vending machine"],
	linkedRooms: [],
	userName: "userG",
	userInventory: ["note 1G","note 3G-1","note 4g"]
 },
 {
	roomId: "H",
	roomDescription: "You find yourself in a bedroom of some sorts. You see a lightbulb, a small bed, a lamp, a chest of drawers, some small cupboards, and for some reason a safe on a shelf. Who puts a safe on a shelf? The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorH_I", 
	roomRightDoor: "doorH_G",
	roomImagePath: "images/rooms/guestRoom.png",
	roomImageList: ["images/rooms/guestRoom.png","images/rooms/guestRoom.png","images/rooms/guestRoomOpen.png","images/rooms/guestRoomOpen.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["safe"],
	linkedRooms: ["panelh", "remoteh"],
	userName: "userH",
	userInventory: ["note 1H", "note 2H","note 3H-2"]
 },
 ]);


////ITEMS LIST////

db.items.insert(
 [
//ALL NOTES
 {
  itemId: "note1A",
  itemName: "note 1A",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1A.png" 
 },
  {
  itemId: "note3A-3",
  itemName: "note 3A-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 },
 {
  itemId: "note1B",
  itemName: "note 1B",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1B.png"
 },
 {
  itemId: "note2B",
  itemName: "note 2B",
  description: "Day 5: I know that there is a hidden panel in here somewhere… Maybe if I tell Doris a certain word it will open",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3B-1",
  itemName: "note 3B-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1C",
  itemName: "note 1C",
  description: "Day 23: Doris has given me the following clue for one of the door passwords: What year did two companies merge in order to form ConcoPhillips? How am I supposed to figure it out without google?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1C.png" 
 },
 {
  itemId: "note3C-4",
  itemName: "note 3C-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1D",
  itemName: "note 1D",
  description: "Day 73: I know I have seen the man in the painting before. Could it have been when I was looking up information on ConocoPhillips the night before I got here? Maybe it is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1D.png" 
 },
  {
  itemId: "note2D",
  itemName: "note 2D",
  description: "Day 91: The stratching sounds will not stop. They are driving me crazy. I think they are coming from under the bookshelf next door.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3D-2",
  itemName: "note 3D-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1E",
  itemName: "note 1E",
  description: "Day 52: Still can not figure out the password to one of the doors. The only thing Doris will say when I ask is Tick Toc...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1E.png" 
 },
 {
  itemId: "note1E-2",
  itemName: "note 1E-2",
  description: "Day 10: I have tried every combination I can think of with birthdays on the safe. What does Doris mean by the the birthday of the one who is most mature?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3E-4",
  itemName: "note 3E-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1F",
  itemName: "note 1F",
  description: "Day 67: I still cannot unscramble the letters on the fridge. I know it has to do with ConocoPhillips but what else? Come on THINK! I think the two words that they spell is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1F.png" 
 },
 {
  itemId: "note3F-3",
  itemName: "note 3F-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1G",
  itemName: "note 1G",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1G.png" 
 },
 {
  itemId: "note3G-1",
  itemName: "note 3G-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note4G",
  itemName: "note 4g",
  description: "Day 107: We have noticed that there are 6 lightbulbs in 6 different rooms. Each are labeled something with a letter and have their own light switch. Additionally there is one light bulb with no letter or a switch. Perhaps if the bulbs were lit in the correct order they would light up. But what do the letters mean? Maybe something to do with ConocoPhillips?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note4g.png" //TODO MAKE THIS NOTE 
 },
 {
  itemId: "note1H",
  itemName: "note 1H",
  description: "Day 101: I seem to have gone blind from the pink. Too bad because I have finally tricked Doris into giving me a clue to the password to one of the doors. Doris said something about looking in the comments? What does that even mean though?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1H.png" 
 },
 {
  itemId: "note2H",
  itemName: "note 2H",
  description: "Day 5: I know that there is a hidden panel in here somewhere…Doris has mentioned that its the most important thing for ConocoPhillips...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3H-2",
  itemName: "note 3H-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },

//ROOM A ITEMS
 {
  itemId: "chargerA",
  itemName: "spiffy charger",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 }, 
 {
  itemId: "safeA",
  itemName: "secure safe",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "plastic remote",
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up key"
                }],
  imagePath: "" 
 },
  {
  itemId: "keyB_C", //found in room A, in safeA
  itemName: "key B_C",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },

 //ROOM B ITEMS
 {
  itemId: "panelB",
  itemName: "panelB",
  description: "This is a hidden panel - if you tell it a password, it may open for you.",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password",
                    key: "integrity",
                    onStageCompleteDesc: "It's just a panel.",
                    onStageCompleteMessage: "You found a remote! I wonder what you could use it on...",
                    postStageCmd: "pick up plastic remote"
                }],
  imagePath: "" 
 }

 {
  itemId: "remoteB",
  itemName: "plastic remote",
  description: "This remote looks dead...",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "spiffy charger",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote.",
                }],
  imagePath: "" 
 },
 //ROOM C ITEMS
 {//TODO CHECK THIS ITEM
  itemId: "bookshelfC",
  itemName: "bookshelf C",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "push",
                    key: "D",
                    onStageCompleteDesc: "There's an assortment of books here, Harry Potter, Game of Thrones, Ender's Game...you should push one that you're interested in. Who knows, maybe something will happen?",
                    onStageCompleteMessage: "You found a mouse sitting behind the bookself...he's holding a key!",
                },
                {
                    keyType: "push",
                    key: "ender's game", //FINDME PERSONALIZED
                    onStageCompleteDesc: "It was pretty smart to push that book. I don't think you can do anything else here.",
                    onStageCompleteMessage: "What a brilliant idea! You found an extension cord.",
                    postStageCmd: "pick up extension cord"
                }
                ]
  imagePath: "" 
 },
 {
  itemId: "mouseC", //found behind bookshelf
  itemName: "mouse",
  description: "I wonder if the critter has anything I need?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "yummy cheese",
                    onStageCompleteDesc: "The mouse scattered away after you fed him. You can't see him anymore.",
                    onStageCompleteMessage: "The mouse appreciated your cheese, and left a key behind him!",
                    postStageCmd: "pick up key c_b"
                }]
  imagePath: "" 
 },
 {
  itemId: "extensionCordC", //found behind bookshelf
  itemName: "extension cord",
  description: "",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "keyC_B", //found from the mouse
  itemName: "key C_B",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 //ROOM D ITEMS
 {
  itemId: "cheeseD",
  itemName: "yummy cheese",
  description: "yummy cheese is yummy",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 //ROOM E ITEMS
 {
  itemId: "panelE",
  itemName: "numeric panel",
  description: "The panel in this room has a keypad, indicating you can enter numbers. No spaces, hashes, or asterisks though. #unfair.",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based vending machine hint
                    key: "2121212121212121", //FINDME Personalized! 
                    onStageCompleteDesc: "The panel is open",
                    onStageCompleteMessage: "You found a power plug...not connected to anything. Is there anything that needs electricity?",
                    postStageCmd: "pick up power plug"
                }],
  imagePath: "" 
 },
 {
  itemId: "power plugE",
  itemName: "power plug",
  description: "This power plug could be used on something to give it power...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 }
 //ROOM F ITEMS
  {
  itemId: "heavyitemF",
  itemName: "heavy desk",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "push",
                    key: "D",
                    onStageCompleteDesc: "There's a safe behind this...it looks like it needs a combination",
                    onStageCompleteMessage: "You found a hidden safe!",
                }]
  imagePath: "" 
 },
 {
  itemId: "safeF",
  itemName: "plain safe",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based on a hint note1E-2
                    key: "110892", //FINDME Personalized! 
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up key F_G"
                }],
  imagePath: "" 
 },
 {
  itemId: "keyF_G",
  itemName: "key F_G",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 //ROOM G ITEMS
 {
  itemId: "safeG",
  itemName: "safe g",
  description: "I wonder if there's anything useful in here...",
  visible: false,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "remoteh",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up key G_F"
                }],
  imagePath: "" 
 },
 {
  itemId: "chargerG",
  itemName: "charger g",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 },
 {
  itemId: "keyG_F",
  itemName: "key G_F",
  description: "This key looks different...it has the words 'exploration and production' engraved on it.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "center door G",
  itemName: "center door",
  description: "This door looks like its nailed shut",
  visible: false,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "hammer",
                    onStageCompleteDesc: "This door looks loose, maybe if you try to pull it, you'll see some results...",
                    onStageCompleteMessage: "You removed the nails, and the door looks like it just needs to be pulled",
                },
                {
                    keyType: "pull",
                    key: "ABCDEFH",
                    onStageCompleteDesc: "You moved the door to the side, great work!",
                    onStageCompleteMessage: "You pulled away the door and revealed a vending machine!",
                }],
  imagePath: "" 
 },
 {
  itemId: "vendingMachineG",
  itemName: "vending machine",
  description: "This vending machine clearly is dead. I think all of the food in it is probably bad. You see a note on the vending machine, it says 'Theres a panel you should open. Use all of the players' birthdays, from youngest to oldest.' Another note reads 'one of you has a favorite book. you should pick it up from the bookshelf.' ", //enter every players birthday in backwords order
  visible: false,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "power plug",
                    onStageCompleteDesc: "The plug doesn't quite reach the wall, but you're so close! Got something to extend it?",
                    onStageCompleteMessage: "The plug doesn't quite reach the wall, but you're so close!",
                },
                {
                    keyType: "key",
                    key: "extension cord",
                    onStageCompleteDesc: "The vending machine is plugged into the wall! Now all you need is some money to get a key from it.",
                    onStageCompleteMessage: "That worked! Now the vending machine has power...got money?",
                },
                {
                    keyType: "key",
                    key: "money",
                    onStageCompleteDesc: "There's nothing else to do here",
                    onStageCompleteMessage: "That worked, and you got a key for your trouble.",
                    postStageCmd: "pick up final key"
                },
                ],
  imagePath: "" 
 },
 //ROOM H ITEMS
 itemId: "panelH",
  itemName: "panelH",
  description: "This is a hidden panel",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //user derives this based on safety obviously being the most important thing to COP
                    key: "safety",
                    onStageCompleteDesc: "You found a remote! I wonder what you could use it on...",
                    onStageCompleteMessage: "The remote has been put in your inventory",
                    postStageCmd: "pick up remoteh"
                }],
  imagePath: "" 
 },
  {
  itemId: "remoteH",
  itemName: "remoteh",
  description: "This remote looks dead...",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "charger g",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote.",
                }],
  imagePath: "" 
 },
 {
  itemId: "safeH",
  itemName: "safe h",
  description: "This remote looks dead...",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "charger g",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote.",
                }],
  imagePath: "" 
 },
 {
  itemId: "hammerH", //TODO activate pickup HammerH when SPIRIT Lights 
  itemName: "hammer",
  description: "This hammer looks perfect for removing nails from something...do I know anywhere that nails are?",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  imagePath: "" 
 },
 ]
 );

//Doors - 29
db.doors.insert(
 [{
	doorId: "doorA_P",
	description: "This door doesn't seem to give you any clue how you will open it.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "RANDOM - this door doesn't open",
	}]
 },
 {
	doorId: "doorA_B",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this goes on the back of a jigsaw puzzle
		key: "kittens123",
	}]
 },
 {
	doorId: "doorB_A",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this is received after rock paper scissors
		key: "puppies456",
	}]
 },
 { 
	doorId: "doorB_C",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyB_C", //user got this from safeA
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorC_D",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1c
		key: "2002",
	}]
 },
 {
	doorId: "doorD_C",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1d
		key: "frank phillips",
	}]
 },
 {
	doorId: "doorC_B",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyC_B", //user received this from a mouse
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
 	//TODO to add more helpful descriptions, including the ghost
	doorId: "doorD_E", //for stageABCD, this is where they enter the passwords from mentors
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //nickname of past prisoner
		key: "kit415",
	},
	{
		keyType: "password", //talk to ghost
		key: "whelp161",
	},
	{
		keyType: "password", //impress a guard
		key: "whelp161",
	},
	{
		keyType: "password", //bribe a janitor
		key: "whelp161",
	}]
 },
 {
	doorId: "doorE_F",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this password from the time on the wall, based on the hint
		key: "3:40",
	}]
 },
 {
	doorId: "doorF_E",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //User picks this up by unscrambling the letters on the fridge, based on the hint
		key: "natural gas",
	}]
 },
 {
	doorId: "doorF_G",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList:  
	[{
		keyType: "key", //received from safeF 
		key: "key F_G",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorG_H",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the back of a puzzle
		key: "pups112",
	}]
 },
 {
	doorId: "doorH_G",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //Password in the code
		key: "joeys101", //FINDME make sure there is a fairly technical person on this for Test2
	}]
 },
 {
	doorId: "doorG_F",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the key in safeG after using remote
		key: "exploration and production",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },{
	doorId: "doorE_D", 	//For EFGH, passwords from mentors
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",  //talk to ghost
		key: "kit415",
	},
	{
		keyType: "password", //impress a guard
		key: "whelp161",
	},
	{
		keyType: "password", //bribe a janitor 
		key: "whelp161",
	},
	{
		keyType: "password", //nickname of past prisoner
		key: "whelp161",
	}]
 },
 ]);
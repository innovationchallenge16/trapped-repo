//Reset database
db.items.drop();
db.userrooms.drop();
db.doors.drop();  
db.testlogs.drop();
db.createCollection("items");
db.createCollection("userrooms");
db.createCollection("doors");
db.createCollection("testlogs");


//UserRooms - 16
//TODO ADD All roomSafes
db.userrooms.insert(
 [{
	roomId: "A",
	roomDescription: "You seem to be trapped in a large walk-in closet. Who needs this much space? You see a secure safe, a light bulb, a switch, a fine dresser, some fancy hanging clothes, a cardobard box, an expensive purse, and most importantly a strange button that is begging to be pushed. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorA_P", 
	roomRightDoor: "doorA_B",
	roomImagePath: "images/rooms/closet.png",
	roomImageList: ["images/rooms/closet.png","images/rooms/closetOn.png","images/rooms/closetOpenOff.png","images/rooms/closetOpenOn.png"],
	roomSafe: "secure safe",
	roomSafeLocked:  true, //if safe exists, this is true
	roomLightLetter: "P",
	roomLightSwitch: false, //by default, light is off and this is true
	linkedRooms: ["B"],
	itemsInRoom: ["secure safe", "fine dresser", "fancy hanging clothes", "cardboard box", "expensive purse", "strange button"],
	userName: "UserA",
	userInventory: []
 },
 {
	roomId: "B",
	roomDescription: "As you look around you surpress a slight shiver. It is quite cold in here. You see a lightbulb, a switch, some type of old car with small cupboards above it, an open paint can,  and a red chest of drawers with a handy toolbox on top of it. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorB_C", 
	roomRightDoor: "doorB_A",
	roomImagePath: "images/rooms/garage.png",
	roomImageList: ["images/rooms/garage.png","images/rooms/garageOn.png","images/rooms/garageDarkOff.png","images/rooms/garageOnDark.png"],
	// roomSafe: "",
	roomSafeLocked:  true, //for this, true if Normal, false is Dark
	roomLightSwitch: false, //by default, light is off and this is true
	roomLightLetter: "S",
	itemsInRoom: ["old car","small cupboards", "open paint can", "red chest of drawers", "handy toolbox"],
	linkedRooms: ["A"],
	userName: "userB",
	userInventory: []
 },
 {
	roomId: "C",
	roomDescription: "You find yourself trapped in some sort of library. You see a lightbulb, a switch, two red bookshelves, a yellow cup, a stack of books, and an uncomfortable looking couch. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorC_D", 
	roomRightDoor: "doorC_B",
	roomImagePath: "images/rooms/library.png",
	roomImageList: ["images/rooms/library.png","images/rooms/libraryOn.png","images/rooms/library.png","images/rooms/libraryOn.png"],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "I",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["two red bookshelves","yellow cup","stack of books", "uncomfortable looking couch"],
	linkedRooms: ["D"],
	userName: "userC",
	userInventory: []
 },
 {
	roomId: "D",
	roomDescription: "You find yourself in a small dining room with a light fixture, a lightbulb, switch, oak table, oak chairs, a red cupboard, a brown cup, and a beautiful painting. The figure in the painting looks familiar to you. The only ways out that you can see are two doors on either side of the room. ",
	roomLeftDoor: "doorD_E", 
	roomRightDoor: "doorD_C",
	roomImagePath: "images/rooms/dining.png",
	roomImageList: ["images/rooms/dining.png","images/rooms/diningOn.png","images/rooms/dining.png","images/rooms/diningOn.png"],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "R",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["oak table", "oak chairs", "red cupboard", "brown cup", "beautiful painting"],
	linkedRooms: ["C"],
	userName: "userD",
	userInventory: []
 },
 {
	roomId: "E",
	roomDescription: "You find yourself in a clean office with a light bulb, a switch, a small desk, a fast computer, a gray trashcan, colorful books, and a face clock. The clock does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorE_F", 
	roomRightDoor: "doorE_D",
	roomImagePath: "images/rooms/office.png",
	roomImageList: ["images/rooms/office.png","images/rooms/officeOn.png","images/rooms/office.png","images/rooms/officeOn.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "S",	
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["small desk", "fast computer", "gray trashcan", "colorful books", "face clock"],
	linkedRooms: ["F"],
	userName: "userE",
	userInventory: []
 },
 {
	roomId: "F",
	roomDescription: "You find yourself in a retro looking kitchen. There is a lightbulb, a switch, an electric oven, kitchen cupboards, some kitchen drawers, and a fridge with magnets on it. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorF_G", 
	roomRightDoor: "doorF_E",
	roomImagePath: "images/rooms/kitchen.png",
	roomImageList: ["images/rooms/kitchen.png","images/rooms/kitchenOn.png","images/rooms/kitchen.png","images/rooms/kitchenOn.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightLetter: "T",
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["electric oven", "kitchen cupboards", "kitchen drawers", "fridge","magnets"],
	linkedRooms: ["E"],
	userName: "userF",
	userInventory: []
 },
 {
	roomId: "G",
	roomDescription: "You find yourself in a strange room with two large machines in them. You think they may be used to clean clothes but you are not sure how. Your mother always does it for you anyway. You also see a light bulb, a safe, some shelves with clothes, a box, and a thing you are pretty sure is called an ironing board. There are three doors in this room, one on either side of the room and a center door  which looks like it has been nailed down.",
	roomLeftDoor: "doorG_H", 
	roomRightDoor: "doorG_F",
	roomImagePath: "images/rooms/laundryRoom.png",
	roomImageList: ["images/rooms/laundryRoom.png","images/rooms/laundryRoom.png","images/rooms/laundryRoom.png","images/rooms/laundryRoom.png"],
	roomSafe: "box",
	roomSafeLocked:  false, //For this Object, the safe is 'box'
	roomLightSwitch: false, //For this Object, the light is the center door
	itemsInRoom: ["machines", "tv", "clothes", "box", "ironing board", "center door"],
	linkedRooms: ["H"],
	userName: "userG",
	userInventory: []
 },
 {
	roomId: "H",
	roomDescription: "You find yourself in a bedroom of some sorts. You see a lightbulb, a small bed, a lamp, a chest of drawers, some small cupboards, and for some reason a safe on a shelf. Who puts a safe on a shelf? The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorH_I", 
	roomRightDoor: "doorH_G",
	roomImagePath: "images/rooms/guestRoom.png",
	//TODO Guest Image safe on/closed? 
	roomImageList: ["images/rooms/guestRoom.png","images/rooms/guestRoom.png","images/rooms/guestRoomOpen.png","images/rooms/guestRoomOpen.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["small bed", "lamp", "chest of drawers", "small cupbords", "safe"],
	linkedRooms: ["G"],
	userName: "userH",
	userInventory: []
 },
 {
	roomId: "I",
	roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. Looking around you find a safe, a bathtub, a toilet, a sink, a storage box, some towels on a shelf, and some assorted toiletries. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorI_J", 
	roomRightDoor: "doorI_H",
	roomImagePath: "images/rooms/MasterBathroom.png",
	roomImageList: ["images/rooms/MasterBathroom.png","images/rooms/MasterBathroom.png","images/rooms/MasterBathroomOpen.png","images/rooms/MasterBathroomOpen.png"],
	roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["bathtub", "toilet", "sink", "storage box", "towels", "assorted toiletries"],
	linkedRooms: ["J"],
	userName: "userI",
	userInventory: []
 },
 {
	roomId: "J",
	roomDescription: "You find yourself in a modern looking living room. There is a comfortable looking couch, a set of cupboards, a TV stand, a speaker, some boxes, and a TV that only shows Doris. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorJ_K", 
	roomRightDoor: "doorJ_I",
	roomImagePath: "images/rooms/livingRoom.png",
	roomImageList: ["images/rooms/livingRoom.png","images/rooms/livingRoom.png","images/rooms/livingRoomOpen.png","images/rooms/livingRoomOpen.png"],
	roomSafe: "", 
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["comfortable looking couch", "set of cupboards", "tv stand", "speaker", "some boxes", "tv"],
	linkedRooms: ["I"],
	userName: "userJ",
	userInventory: []
 },
 {
	roomId: "K",
	roomDescription: "You find yourself in an overwhelmingly pink bedroom. As you squint your eyes agaisnt the offensive color scheme you see a kids bed, a kids desser, and a rug. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorK_L", 
	roomRightDoor: "doorK_J",
	roomImagePath: "images/rooms/kidsRoom.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["kids bed", "kids dresser", "rug"],
	linkedRooms: ["L"],
	userName: "userK",
	userInventory: []
 },
 {
	roomId: "L",
	roomDescription: "You find yourself in a cute nursery with a crib, a picture on the wall,a nursery chest of drawers , a ball, and a clock. The clock does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorL_M", 
	roomRightDoor: "doorL_K",
	roomImagePath: "images/rooms/Nursery.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["crib", "picture", "nursery chest", "ball", "clock"],
	linkedRooms: ["K"],
	userName: "userA",
	userInventory: []
 },
 {
	roomId: "M",
	roomDescription: "You find yourself in trapped in a gym. Looking around you see some lockers with have a strange smell coming from them, a punching bag, and a bar with weights. The only way out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorM_N", 
	roomRightDoor: "doorM_L",
	roomImagePath: "images/rooms/gym.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["lockers", "punching bag", "bar with weights"],
	linkedRooms: ["N"],
	userName: "userM",
	userInventory: []
 },
 {
	roomId: "N",
	roomDescription: "You find yourself trapped in a theater room of some sort. There are some comfortable looking chairs, some speakers on stands, a compact tv stand, and a giant TV. Too bad it is only showing what Doris is saying. Maybe if you could put on a movie it would not be too bad to stay here for a bit.",
	roomLeftDoor: "doorN_O", 
	roomRightDoor: "doorN_M",
	roomImagePath: "images/rooms/theater.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["comfortable looking chairs", "speakers on stands", "compact tv stand", "giant tv"],
	linkedRooms: ["M"],
	userName: "userN",
	userInventory: []
 },
 {
	roomId: "O",
	roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. As you hold your nose from the stink you see a toilet, a sink, a large bathtub, and a set of cupboards. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorO_P", 
	roomRightDoor: "doorO_N",
	roomImagePath: "images/rooms/bathroom.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["bathroom toilet", "bathroom sink", "large bathtub", "bathroom cupboards"],
	linkedRooms: ["P"],
	userName: "userO",
	userInventory: []
 },
 {
	roomId: "P",
	roomDescription: "You look around the simple bedroom and don't find much of interest.The only ways out that you can see are two doors on either side of the room. There is a standard bed, a desk, and a painting on the wall. The figure in the painting looks familiar.",
	roomLeftDoor: "doorP_A", 
	roomRightDoor: "doorP_O",
	roomImagePath: "images/rooms/adultBedroom.png",
	roomImageList: [],
	// roomSafe: "",
	roomSafeLocked:  false, //if safe exists, this is true
	roomLightSwitch: false, //by default, light is off and this is true
	itemsInRoom: ["standard bed", "standard desk", "standard painting"],
	linkedRooms: ["O"],
	userName: "userP",
	userInventory: []
 }]
);

//Doors - 29
db.doors.insert(
 [{
	doorId: "doorA_P",
	description: "This door doesn't seem to give you any clue how you will open it.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "RANDOM - this door doesn't open",
	}]
 },
 {
	doorId: "doorA_B",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "kittens123",
	}]
 },
 {
	doorId: "doorB_A",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "puppies456",
	}]
 },
 {
	doorId: "doorC_D",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Ada Lovelace",
	}]
 },
 {
	doorId: "doorD_C",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Frank Phillips",
	}]
 },
 {
	doorId: "doorE_F",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "3:40",
	}]
 },
 {
	doorId: "doorF_E",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Natural Gas",
	}]
 },
 {
	doorId: "doorG_H",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "pups112",
	}]
 },
 {
	doorId: "doorH_G",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "joeys101",
	}]
 },
 {
	doorId: "doorJ_I",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "upstream",
	}]
 },
 {
	doorId: "doorI_J",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "froglet131",
	}]
 },
 {
	doorId: "doorK_L",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "cubs789",
	}]
 },
 {
	doorId: "doorL_K",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "1:50",
	}]
 },
 {
	doorId: "doorM_N",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "calf131",
	}]
 },
 {
	doorId: "doorN_M",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "WarGames",
	}]
 },
 {
	doorId: "doorO_P",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "joeys101",
	}]
 },
 {
	doorId: "doorP_O",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "2002",
	}]
 },
 {
	doorId: "doorB_C",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyB_C",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorC_B",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyC_B",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorF_G",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyF_G",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorG_F",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyG_F",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorJ_K",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Shale",
	}]
 },
 {
	doorId: "doorK_J",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyK_J",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorN_O",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Trail Mix and Side Show",
	}]
 },
 {
	doorId: "doorO_N",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "keyO_N",
		onStageDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorD_E",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "kit415",
	},
	{
		keyType: "password",
		key: "whelp161",
	}]
 },
 {
	doorId: "doorE_D",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "duckling191",
	},
	{
		keyType: "password",
		key: "fawn718",
	}]
 },
 {
	doorId: "doorL_M",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "foal223",
	},
	{
		keyType: "password",
		key: "eaglet202",
	}]
 },
 {
	doorId: "doorM_L",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "piglet425",
	},
	{
		keyType: "password",
		key: "owlet262",
	}]
 },
 {
	doorId: "doorcommon",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",
		key: "Common",
	}]
 }]
);

//Items - 2
db.items.insert(
 [{
  itemId: "note1A",
  itemName: "Note 1A",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1A.png" 
 },
 {
  itemId: "buttonA",
  itemName: "strange button",
  description: "This thing is just begging to be pushed.",
  visible: true,
  canBeHeld: false,
  unActivated: false,
  imagePath: ""
 },
 {
  itemId: "safeA",
  itemName: "secure safe",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "keyA_B",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up key"
                }],
  imagePath: "" 
 },
 {
  itemId: "keyA_B",
  itemName: "key A_B",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "chargerA",
  itemName: "spiffy charger",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 }, 
 {
  itemId: "note3A",
  itemName: "note 3A",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 },//Start Note B
 {
  itemId: "note1B",
  itemName: "note 1B",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1B.png"
 },
 {
  itemId: "panelB",
  itemName: "panelB",
  description: "This is a hidden panel",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password",
                    key: "integrity",
                    onStageCompleteDesc: "You found a remote! I wonder what you could use it on...",
                    onStageCompleteMessage: "The remote has been put in your inventory",
                    postStageCmd: "pick up remote"
                }],
  imagePath: "" 
 },
 {
  itemId: "remoteB",
  itemName: "plastic remote",
  description: "This remote looks dead...",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "spiffy charger",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote.",
                }],
  imagePath: "" 
 },
 {
  itemId: "note2B",
  itemName: "note 2B",
  description: "Day 5: I know that there is a hidden panel in here somewhere… Maybe if I tell Doris a certain word it will open",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3B-1",
  itemName: "note 3B-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1C",
  itemName: "note 1C",
  description: "Day 23: Doris has given me the following clue for one of the door passwords: What year did two companies merge in order to form ConcoPhillips? How am I supposed to figure it out without google?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1C.png" 
 },
 {
  itemId: "heavyitemC",
  itemName: "heavy item C",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "mouseC",
  itemName: "mouse C",
  description: "I wonder if the critter has anything I need?",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "keyC_B",
  itemName: "key C_B",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3C-4",
  itemName: "note 3C-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1D",
  itemName: "note 1D",
  description: "Day 73: I know I have seen the man in the painting before. Could it have been when I was looking up information on ConocoPhillips the night before I got here? Maybe it is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1D.png" 
 },
 {
  itemId: "cheeseD",
  itemName: "cheese D",
  description: "yummy cheese",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1D",
  itemName: "note 1D",
  description: "Day 91: The stratching sounds will not stop. They are driving me crazy. I think they are coming from under the desk next door.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3D-2",
  itemName: "note 3D-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1E",
  itemName: "note 1E",
  description: "Day 52: Still can not figure out the password to one of the doors. The only thing Doris will say when I ask is Tick Toc...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1E.png" 
 },
 {
  itemId: "note1E-2",
  itemName: "note 1E-2",
  description: "Day 10: I have tried every combination I can think of with birthdays on the safe. What does Doris mean by the the birthday of the one who is most mature?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3E-4",
  itemName: "note 3E-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1F",
  itemName: "note 1F",
  description: "Day 67: I still cannot unscramble the letters on the fridge. I know it has to do with ConocoPhillips but what else? Come on THINK! I think the two words that they spell is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1F.png" 
 },
 {
  itemId: "heavyitemF",
  itemName: "heavy desk",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "keyF_G",
  itemName: "key F_G",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3F-3",
  itemName: "note 3F-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1G",
  itemName: "note 1G",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1G.png" 
 },
 {
  itemId: "buttonG",
  itemName: "grand button",
  description: "This thing is just begging to be pushed.",
  visible: true,
  canBeHeld: false,
  unActivated: false,
  imagePath: ""
 },
 {
  itemId: "safeG",
  itemName: "safeG",
  description: "I wonder if there's anything useful in here...",
  visible: false,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "keyF_G",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up key"
                }],
  imagePath: "" 
 },
 {
  itemId: "chargerG",
  itemName: "charger G",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: ""
 },
 {
  itemId: "note3G-1",
  itemName: "note 3G-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1H",
  itemName: "note 1H",
  description: "Day 101: I seem to have gone blind from the pink. Too bad because I have finally tricked Doris into giving me a clue to the password to one of the doors. Doris said something about looking in the comments? What does that even mean though?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1H.png" 
 },
 {
  itemId: "note2H",
  itemName: "note 2H",
  description: "Day 5: I know that there is a hidden panel in here somewhere… Maybe if I tell Doris a certain word it will open",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "panelH",
  itemName: "panelH",
  description: "This is a hidden panel",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password",
                    key: "Safety",
                    onStageCompleteDesc: "You found a remote! I wonder what you could use it on...",
                    onStageCompleteMessage: "The remote has been put in your inventory",
                    postStageCmd: "pick up remote"
                }],
  imagePath: "" 
 },
 {
  itemId: "remoteH",
  itemName: "remoteH",
  description: "This remote looks dead...",
  visible: false,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "charger G",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote.",
                }],
  imagePath: "" 
 },
 {
  itemId: "note3H-2",
  itemName: "note 3H-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1I",
  itemName: "note 1I",
  description: "Day 35: Doris has given me the following clue: What oil sector is ConocoPhillips classified as that is also known as E&P?  How am I supposed to figure it out without google?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1I.png" 
 },
 {
  itemId: "combolockboxI",
  itemName: "combolockboxI",
  description: "What's inside?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note2I",
  itemName: "note 2I",
  description: "A S E H L",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note2I-B",
  itemName: "note 2I-B",
  description: "Day 329: I don't undertand. Doris said that the combination was a birthday. If it was not mine, whose is it?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3I-3",
  itemName: "note 3I-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1J",
  itemName: "note 1J",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors'",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1J.png" 
 },
 {
  itemId: "combolockboxI",
  itemName: "combolockboxI",
  description: "What's inside?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note2J",
  itemName: "note 2J",
  description: "New technologies have allowed us to more effectively get oil and gass out of what type of rock? Doris said the password is the answer to that question. What could it be? Im not a geologist...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3J-4",
  itemName: "note 3J-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1K",
  itemName: "note 1K",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1K.png" 
 },
 {
  itemId: "cluehomek",
  itemName: "clue home k",
  description: "Doris is playing games with me. She said the password is the Lat and Long for our location but how I can know that. Its not as if I can just google it.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "heavyitemK",
  itemName: "heavy item K",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "parotK",
  itemName: "parot K",
  description: "parot parot parot parot key",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
{
  itemId: "keyK_J",
  itemName: "key K_J",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3K-2",
  itemName: "note 3K-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1L",
  itemName: "note 1L",
  description: "Day 52: Still can not figure out the password to the door on the right. The only thing Doris will say when I ask is Tick Toc...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1L.png" 
 },
 {
  itemId: "crackersK",
  itemName: "crackersK",
  description: "Day 52: Still can not figure out the password to the door on the right. The only thing Doris will say when I ask is Tick Toc...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1L.png" 
 },
 {
  itemId: "note3L-1",
  itemName: "note 3L-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },


 {
  itemId: "note1M",
  itemName: "note 1M",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1M.png" 
 },
 {
  itemId: "combinationlockM",
  itemName: "combination lock M",
  description: "contains note about password [2NP]; Opens with birthday of N",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note 2NP",
  itemName: "note 2NP",
  description: "<Has note about password being answer to the pictogram puzzles>",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note 2M",
  itemName: "note 2M",
  description: "Day 329: I don't undertand. Doris said that the combination was a birthday. If it was not mine, whose is it?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3M-3",
  itemName: "note 3M-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are ____'s crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1N",
  itemName: "note 1N",
  description: "Day 34: The only clue I can get from Doris is that the password to one of the doors is the name of a movie that has her brother, Joshua, in it and that he likes to play games? I wonder if her brother is as crazy as she is. If only I had access to google I could figure it out...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1N.png" 
 },
 {
  itemId: "combinationlockN",
  itemName: "combination lock N",
  description: "Contains note with Pictograms [2MP]; Opens with birthday of M",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note 2MP",
  itemName: "note 2MP",
  description: "Note with two Pictograms on it",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3N-1",
  itemName: "note 3N-1",
  description: "Day 384: _____ , _____, ____, and ____. There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1O",
  itemName: "note 1O",
  description: "Day 101:  I have finally tricked Doris into giving me a clue to the password for one of the doors. Doris said something about looking in the comments? What does that even mean though?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1O.png" 
 },
 {
  itemId: "cheeseO",
  itemName: "cheese O",
  description: "yummy cheese",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "lockedboxO",
  itemName: "locked box O",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "keyO_N",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up key"
                }],
  imagePath: "" 
 },
 {
  itemId: "keyO_N",
  itemName: "key O_N",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note2O",
  itemName: "note 2O",
  description: "Day 2: I have found a Bobby pin. Maybe I can use it to pick a lock. Like the one I saw in the room next door.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2O.png" 
 },
 {
  itemId: "note3O-2",
  itemName: "note 3O-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is ___. Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note1P",
  itemName: "note 1P",
  description: "Day 97: After months of questioning I have made a break through. The password to one of the doors has something to do with Doris's Mother. She said that there is a picture of her mother in the room. How does a computer have a mother? If only I had access to google I might be able to figure it out...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1O.png" 
 },
 {
  itemId: "mouseP",
  itemName: "mouse P",
  description: "This mouse seems be carrying a bobby pin",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "bobbypinP",
  itemName: "bobby pin P",
  description: "I bet this thing will be useful..",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "note3P-4",
  itemName: "note 3P-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like ___ did. Or was it ___? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 }
 ]);


 	


//Final Count should be 2-2-2
db.items.count();
db.userrooms.count();
db.doors.count();

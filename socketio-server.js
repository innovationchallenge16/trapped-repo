var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var RPSPlayer = 0;
var RPSDoris = 0;
var won = false;
var lost = false;

//Where to send commands
app.get('/', function(req, res){
//  res.sendFile(__dirname + '/studentIndex_socketIO.html');
  res.sendFile(__dirname + '/studentIndex2.html');
});

app.use(express.static(path.join(__dirname, 'public')));

/** Temporary Data Objects for Front End Development **/
var KeyA = {
	itemId: "KeyA",
	itemName: "Dusty Key",
	activated: false,
  activateType: '',
	canBeUsed: false,
  canBeHeld: true,
  owner: null,
	description: "This key can be used for something...",
	contains: [],
	imagePath: "images/key.png" 
}

var BedA = {
	itemId: "BedA",
	itemName: "Bed",
	activated: false,
	canBeUsed: false,
  canBeHeld: false,
	activeMsg: "Nothing is under the bed",
	description: "There is a key under the bed!",
	contains: ["keyA"],
	imagePath: "" 
}

var PasswordA = {
  password: 'password',
  used: false
}

var DoorA_P = {  //Password Door
  name: "DoorA_P",
  locked: true,
  activateType: "password",
  password: "kittens123"
}

var DoorA_B = { //Key Door
	name: "DoorA_B",
	locked: true,
  activateType: "key",
	dependentList: ["KeyA"]
}

var RoomA = {
	name: "A",
	description: "You are in a small room. There is a bed on one side and a room.",
	itemList: ["BedA","KeyA"],
	doorList: ["DoorA_B"]
}

var UserA = {
  userId: "A",
  userName: "Gabby",
  inventory: []
}

/** End temporary object list **/

/** Built in messages **/
var errorMessage = "I don't understand what you are trying to say.";
var helpMessage = "There are other commands too, get creative!";
var startMessage = "Hello. My name is Doris. I have trapped you and all the other visitors in your rooms. Please do not try to escape. Please do not type HELP for commands."
var backMessage = "Get all the help you need. You will never get out!"

/**	Server Actions	**/

//On Connection Actions
io.on('connection', function(socket){
  // console.log(socket);

  //TODO Add Doris Welcome Message
  io.emit('doris message', 'Welcome, Human.');
  
  //TODO Handle Room and User here
  socket.on('action', function(input, roomId){
      
      //Determine which action is being taken
      var verb = input.substring(0,input.indexOf(' '));
      if(verb==""){ //Check for one-word commands
        verb = input;
      }

      var response = '';
      var dorisReaction = 'Doris';
      switch(verb) {
      //TODO Add start function
      //TODO Do I need an open function? 
        case "look":
          response = onLookAction(input,roomId);
          break;
        case "use":
          response = onUseAction(input,roomId);
          break;
        case "pick": //Pick up
          response = onPickupAction(input,roomId);
          break;
        case "give":
          response = onGiveAction(input,roomId);
          break;
        case "help":
          response = helpMessage;
          io.emit('room change', changeRoomScreen(input,roomId), roomId);
          break;
        case "back":
          response = backMessage;
          io.emit('room change', changeRoomScreen(input,roomId), roomId);
          break;
        default: 
          response = errorMessage;
          dorisReaction = 'Confused Doris';
      }
      io.emit('doris', response, dorisReaction);  

  }); //end onAction

}); //end onConnection


/** Functions for handling different Actions **/
function changeRoomScreen(input, roomId) {
    var helpImagePath = "images/commands.png";
    var roomImagePath = "images/room";
    if(input=='help'){
      return helpImagePath;
    }
    return roomImagePath + roomId + ".jpg";
}

function onLookAction(input, roomId){
  var response = "";

  //Pull item ID from input
  var thing = input.substring(input.indexOf(' ')+1) + roomId;

  //Check if item Exists
  //TODO DB Logic to pull from DB

  // ****** Temporary Logic For Non-db version*******
  if(thing=="bedA"){
    response = BedA.description;
  }
  else if(thing=="keyA"){
    response = KeyA.description;
  }
  else if(thing=="lockA"){
    response = LockA.description;
  } 
  else if(thing=="doorA_B"){
    if(DoorA_B.locked){
      response = "The door is still locked.";
    } else {
      response = "The door is open!"; 
    }
  } 
  else if(thing=="roomA"){
    response = RoomA.description;
  }
  // ****** End Temporary Logic For Non-db version*******

  //Send message back
  else {
    response = "I don't see that in this room.";  
  }

  return response;
}

/*
Two Use Scenarios: 

Use 'password' on door/item
Check if item/door and/or exists and/or activated already?
Check if password matches
if password matches, activate object / open door
  open door: connect two different rooms
  activate object: ??? on Activate action? 

use key on door/item
Check if item exists
Check if user has item that they're using 
Check if item/door and/or exists and/or activated already?
Check if item/door keyItem matches key
if key matches, activate object / open door
  oper door: connect two different rooms function openDoor()
  activate object: ??? on Activate action? function activateObject()
*/

function onUseAction(input, roomId){
  var response = '';

  //Scenario Three - Use 'password' on item
  if(input.indexOf("' on ")!=-1) {

    //Retrieve second object from DB
    var receivingItemId = input.substring(input.indexOf(" on ")+4);

    //If Receiving object can't be found in DB
    if(receivingItemId+roomId != 'doorA'){ //Temporary Logic
      response = "I don't see what you're trying to use this on.";
      return response;
    }

    var receivingItem = DoorA_P; //This is temp logic, figure out how to determine doors later 

    //Determine if Object can received passwords 
    if(receivingItem.activateType!='Password'){
      response = "Nothing happened."
      return response;
    } 

    //Check if password input matches password in system.
    var pw = input.substring(input.indexOf("'")+1, input.indexOf("' on"))
    
    if(receivingItem.password=pw){
      //Open Door
      receivingItem.locked = false;
      //TODO Check if two rooms are now connected.

      response = "The door opened!";    
    //Check what kind of object this is: door, item? 
    //TODO
    //If door and password matches, open the door!
    //if Item, check if this password activates it.
    }
  }

  //Scenario Two - Use item on item
  else if(input.indexOf(" on ")!=-1){
    // response = 'YAY ON';
  }

  // var thing = input.substring(input.indexOf('up ')+3);
  return response;
}

function openDoor(){

}

function activateItem(){

}


function onGiveAction(input, startRoomId) {
  var response = '';
  // TODO fill out this function
  return response;
}
g
/**/ 


//TODO figure out how picking up an object changes other objects descriptions
function onPickupAction(input, roomId) {
  var response = '';
  
  //Confirm second input is 'up'
  if (input.substring(5,7) != 'up'){
    return errorMessage;
  }
  //Pull item ID from input
  var thing = input.substring(input.indexOf('up ')+3);
  var thingId = input.substring(input.indexOf('up ')+3) + roomId;

  //TODO Get object from database
  if(thingId=='keyA')  var item = KeyA;
  if(thingId=='bedA') var item = BedA;

  //Check that object exists
  // ****** Start Temporary Logic For Non-db version*******
  if(thingId!='keyA'&&thingId!='bedA') {
    response = "I can't find that object in this room";
    return response;
  }

  // ****** End Temporary Logic For Non-db version*******
     
    //If object can be picked up 
    if(item.canBeHeld==true) {
      //Determine Current User
      currUser = UserA;  //TODO Get User from backend based on roomId
    
      if (item.owner==currUser){
        response = "You are already holding this item!";
      } else {
  
        //Add Key to Backend Inventory
       var newInv = [];
        //TODO figure out how to send an array with socket io
       newInv.push(currUser.inventory);
       newInv.push(item);
//       currUser.inventory = newInv; //WHY DOES THIS CAUSE PROBLEMS?!
//       var newInv = currUser.inventory;
       //Add owner to Key
        item.owner = currUser;
        
        //Add Key to UI Inventory
        io.emit('inventory change', newInv, roomId);

        response = "You picked up the " + thing; 
      }
    } 

  //If object can't be picked up
  else {
    response = "You cannot pick up the " + thing;
  }
    
  return response;
}



http.listen(3774, function(){
  console.log('listening on *:3774');
});

var addItem = function(name, description, img) {
var well = document.getElementById("itemWell");
var modal = document.getElementById("itemModels");
var sample = document.getElementById("samplemodal");

well.innerHTML += '<img id="item" class= "items" src="images/' + img + '.png" class="img-responsive" data-toggle="modal" data-target="#' + name + '">'; 
var modalCode = sample.innerHTML.replace("sampMod", name);
modalCode = modalCode.replace("Descrip", description);
modalCode = modalCode.replace("imgURL", img);
modal.innerHTML += modalCode;
};

var itemGivenPop = function (name,itemName, img){
var givenM = document.getElementById("givenModal");
//var showMod = document.getElementById("givenMod");

var modalCode = givenM.innerHTML;

modalCode = modalCode.replace("givenName", name);
modalCode = modalCode.replace("itemName", itemName);
modalCode = modalCode.replace("imgURL", img);
givenM.innerHTML = modalCode;
$('#givenMod').modal('show');
};


var playRockPaperScissors = function() {

$('#rockPaperS').modal('show');

//Check to see if already won or lost
if (won){ //Display password
	if (studentName == "A")
		result.innerHTML = "You beat Doris! She reluctantly tells you the password is Kittens123.";	
	if (studentName == "B")
		result.innerHTML = "You beat Doris! She reluctantly tells you the password is owlet123.";
}
if (lost){ //Must reset
	RPSPlayer = 0;
	RPSDoris = 0;
	var gameSpace = document.getElementById("RPSGameSpace");
	var RPSChoice = document.getElementById("RPSChoice");
	RPSChoice.style.display = 'block';
	gameSpace.style.display = 'none';
	RPSscore.innerHTML = " ";
}
}


var nextRPS = function() {
	
var gameSpace = document.getElementById("RPSGameSpace");
var RPSChoice = document.getElementById("RPSChoice");

//Set User Choice Picture
var weapon = document.getElementById('studentPlay').value;
var yourChoice = document.getElementById("yourChoice");
yourChoice.src= "images/" + weapon + ".png"

//Set Doris Choice
var dorisChoice = document.getElementById("dorisChoice");

dorisChoice.src= "images/" + weapon + ".png"
var v = Math.floor(Math.random() * 3);
    if(v==0)
         dorisChoice.src= "images/Rock.png"
    if(v==1)
         dorisChoice.src= "images/Paper.png"
    if(v==2)
         dorisChoice.src= "images/Scissors.png"

//Decide Winner
var result = document.getElementById("result");
var RPSscore = document.getElementById("RPSscore");
RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
if (v==0 && weapon=="Rock")
		result.innerHTML = "Tie! Try again.";
if (v==0 && weapon=="Paper"){
		result.innerHTML = "You Win! Keep Going.";
		RPSPlayer = RPSPlayer + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	}		
if (v==0 && weapon=="Scissors"){
		result.innerHTML = "Doris Wins! Try again.";
		RPSDoris = RPSDoris + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	}
if (v==1 && weapon=="Paper")
		result.innerHTML = "Tie! Try again.";
if (v==1 && weapon=="Rock"){
		result.innerHTML = "Doris Wins! Try again.";
		RPSDoris = RPSDoris + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	}		
if (v==1 && weapon=="Scissors"){
		result.innerHTML = "You Win! Keep Going.";
		RPSPlayer = RPSPlayer + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	}	
if (v==2 && weapon=="Scissors")
		result.innerHTML = "Tie! Try again.";
if (v==2 && weapon=="Rock"){
		result.innerHTML = "You Win! Keep Going.";
		RPSPlayer = RPSPlayer + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	}		
if (v==2 && weapon=="Paper"){
		result.innerHTML = "Doris Wins! Try again.";
		RPSDoris = RPSDoris + 1;
		RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	} 
	
gameSpace.style.display = 'inline-block';	

//Check for Win
if (RPSPlayer == 3){
	RPSChoice.style.display = 'none';
	won = true;
	lost = false;
	if (studentName == "A")
		result.innerHTML = "You beat Doris! She reluctantly tells you the password is Kittens123.";	
	if (studentName == "B")
		result.innerHTML = "You beat Doris! She reluctantly tells you the password is owlet123.";	
}	
if (RPSDoris == 3){
	RPSChoice.style.display = 'none';
	result.innerHTML = "Doris Wins! Maybe next time...";
	lost = true;
}	

};

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var doorSchema = new Schema({
	//define schema for our room document
	name: String,
	locked: String,
	type: String,
	depids: [{ itemid: String, depids:[{depid: String}]}]
});

var doors = mongoose.model('sandbox_doors', doorSchema, 'sandbox_doors');
module.exports = doors;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  //user connect
  console.log('user connected');
  
  //Shout
  socket.on('shout', function(playerId, message){
    console.log('Player ' + playerId + ": " + message);
        io.sockets.emit('test', 'Megan Fox');
        //io.emit('test', message);

  }); 
});

http.listen(1339, function(){
  console.log('listening on *:1339');
});
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//Where to send commands
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index_example.html');
});


//Server Actions

//On Connection Actions
io.on('connection', function(socket){
  io.emit('chat message', 'Welcome, Human.');
  
//On 'chat message'
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});

http.listen(3555, function(){
  console.log('listening on *:3555');
});

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var path = require('path');
var Schema = mongoose.Schema;

/* Mongo Schemas and Collections */

//UserRoom
//potentially add ceilingDoor?
var userroomSchema = new Schema({ //combined to reduce calls to db
  roomId: String, //used to identify current user and room
  roomDescription: String, //returned on look room 
  roomLeftDoor: String, //left door for leaving the room
  roomRightDoor: String, //right door for leaving the room
  roomImagePath: String, //the image that this room is pointing to
  roomSafe: String, //identifies what item acts as the 'roomSafe'
  roomSafeLocked: Boolean, //if roomSafe is activated, then change this Variable for the room it is in 
  roomImageList: Array, // Array of images: 1. Safe closed, light on 2. Safe closed light off 3. Safe open light on 4. safe open light off
  roomLightSwitch: Boolean,
  itemsInRoom: Array, //list of items in the room so that user can't pick up item not in room
  linkedRooms: {type: Array, default: []}, //marks that the doors have come down between these rooms. 
  userName: String, //First name of user within this room
  userInventory: Array //inventory shown on front end for this room

});
var userrooms = mongoose.model('userrooms', userroomSchema, 'userrooms');
module.exports = userrooms;

//Door
var doorSchema = new Schema({
    doorId: String, //doorId : door<currRoomId>_<LinkedRoomId>
    doorStageList: [{ key: String, //if key, key item; if password, password
                      keyType: String, //identifies either password or key
                      postStageCompleteDesc: String,  //Description Changes to this after stage complete
                      onStageCompleteMessage: String, //Message sent to console when this stage is completed
                      postStageCmd: String }] , 
    currentStage: Number,//if undefined, assume   0 
    description: String, //returned on look <left|right> door
    locked: Boolean, //determines if door has been opened
    onDoorUnlock: String
//    keyType: String, //either password or key
//    password: String, //if keyType is password, password here
//    key: String //if keyType is key, key itemName here
});
var doors = mongoose.model('doors', doorSchema, 'doors');
module.exports = doors;

//Item
var itemSchema = new Schema({
  itemId: String, //only used for admin to identify how items fit into puzzles
  itemName: String, //used by server to identify item
  description: String, //returned when user looks at an item
  visible: Boolean, //determines if item currently hidden
  canBeHeld: Boolean, // determines if a user can pick up an item 
  activated: Boolean, //allows one change to happen to item
  itemStageList: [{ keyType: String, //key, password OR look (eliminates activateCmd)
                    key: String, //if keyType=key, this is the key item. If keyType=password, this is the password
                    postStageCommand: String, //command to execute if stage activated
                    onStageCompleteMessage: String
                    }],
  currentStage: Number,

  // keyType: String, //either password, key, or undefined
  // key: String, // if keyType is key, that key itemName is here
  // password: String, //if keyType is password, that password is here
  // activateCmd: String, //command that 'activates' item
  // onActivateCmd: String, //what happens following the activate command (probably server pick up)
  // onActivateMessage: String, //message returned if item has been activated
  imagePath: String, //image path for the front end to illustrate items here
  largeImagePath: String
});

var items = mongoose.model('items', itemSchema, 'items');
module.exports = items;


//TestLogs
//TODO Get openDoor() and activateItem() to log to this
//http://www.w3schools.com/js/js_dates.asp
// http://stackoverflow.com/questions/10520501/how-to-insert-a-doc-into-mongodb-using-mongoose-and-get-the-generated-id
var testLogSchema = new Schema({
    itemOrDoor: String, //item if itemLog, door if doorLog
    itemName: String,
    doorId: String,
    currentStage: Object,
    currentRoom: String,
    timeStamp: String
});

var testlogs = mongoose.model('testlogs', testLogSchema, 'testlogs');
module.exports = testlogs;


//Where to send commands
app.get('/', function(req, res){
//  res.sendFile(__dirname + '/studentIndex_socketIO.html');
  res.sendFile(__dirname + '/zstudentIndex2_withDB.html');
});

app.use(express.static(path.join(__dirname, 'public')));


/** Built in messages **/
var startMessage = "My name is Doris. I have trapped you and all the other visitors in your rooms. Please do not try to escape. Please do not type HELP for commands."
var helpMessage = "There are other commands too, get creative!";
var backMessage = "Get all the help you need. You will never get out!"

var defaultOpenedDoorMessage = "The door is open.";

var errorMessage = "I don't understand what you are trying to say.";
var cannotFindMessage = "I don't see that here";
var cannotFindItemMessage = "I don't see the thing you want to pick up here";
var cannotFindDoorMessage = "I only see the left door and the right door....so I don't know what you're talking about."
var itemAlreadyHeldMessage = "You are already holding this item!";
var itemCannotBeHeldMessage = "You dummy - this item cannot be held.";
var unlockedDoorMessage = "You unlocked a door? How could you?!";
var doorAlreadyUnlocked = "The door is already unlocked.";
var failedUseMessage = "Nothing happened...";
var notHoldingMessage = "You cannot use an item that you aren't holding."
var roomsAreNotLinkedMessage = "You can't give items to someone if the door between you is locked.";
var userDoesNotExistMessage = "I don't know who you are talking about.";


/** Server Actions  **/

//On Connection Actions
io.on('connection', function(socket){

  //Receive actions from the console input
  socket.on('action', function(input, roomId){
    
    //Callback functions
    var emitDorisText = function(response,roomId){
       dorisReaction = 'Doris';
       io.emit('doris', response, dorisReaction, roomId);
    };

    var emitRoomChange = function(imagePath,roomId){
      io.emit('room change', imagePath, roomId);
    };

    var emitInvChange = function(inventory,roomId){
      io.emit('inventory change', inventory, roomId);
    };

    //Send text to be handled by other functions
    input = input.toLowerCase();
    var responseArr = handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
  
  }); //end onAction
}); //end onConnection


//Function for handling a command
function handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange){
  var response = '';
  var dorisReaction = 'Doris';

  //Determine which action is being taken
  var verb = input.substring(0,input.indexOf(' '));
  if(verb==""){ //Check for one-word commands
    verb = input;
  }

  var response = '';
  var dorisReaction = 'Doris';
  switch(verb) {
  
    case "start": 
      onStartAction(input,roomId,emitDorisText,emitRoomChange);
      break;
    case "look":
      onLookAction(input,roomId,emitDorisText,emitInvChange);
      break;
    case "pick": 
      onPickupAction(input,roomId,emitDorisText,emitInvChange);
      break;
    case "use":
      onUseAction(input,roomId,emitDorisText,emitRoomChange,emitInvChange);
      break;
    case "say":
      //TODO Added to break out the logic in 'use' to make both functions more manageable
      break;
    case "give":
      onGiveAction(input,roomId,emitDorisText,emitInvChange);
      break;
    case "toggle":
      onToggleAction(input,roomId,emitDorisText, emitRoomChange);
      break;
    case "help":
      onHelpAction(input,roomId,emitDorisText,emitRoomChange);
      break;
    case "back":
      onBackAction(input,roomId,emitDorisText,emitRoomChange);
      break;
    case "server": //TODO Remove this later - this is temporary for testing other commands
      callTestFunction();
    default: 
      response = errorMessage;
      dorisReaction = 'Confused Doris';
  }

  return [response, dorisReaction];
}

function callTestFunction() {
  console.log(new Date());

    doors.find({doorId:'doorA_B'},function(error,doors){
      var door = doors[0];
//      console.log(door);

//      openDoor(door, "A");
    });
}

/** Functions for handling different Actions **/
function onStartAction(input, input_roomId, emitDorisText, emitRoomChange){
  var response = '';

  //check for correct input
  if(input=='start'){
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
      response = "Hello " + room.userName + ". " + startMessage;
      emitDorisText(response,input_roomId);
      emitRoomChange(room.roomImagePath,room.roomId);
    });
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
}//End OnStartAction

function onLookAction(input, input_roomId, emitDorisText, emitInvChange){
  var response = "";

  //Pull thing from input
  var thing = input.substring(input.indexOf(' ')+1);
  var verb = input.substring(0,input.indexOf(' '));

  //Find Current Room
  userrooms.find({roomId:input_roomId},function(error,rooms){
    var room = rooms[0]; 
    if(rooms.length>1){ 
      console.log("ERROR: " + rooms.length + " rooms returned from currentRoom query."); 
      console.log("roomID: " + input_roomId);
      console.log("input: " + input);
    }
    if(room == undefined){
      console.log("ERROR: room is undefined"); 
      console.log("roomID: " + input_roomId);
      console.log("input: " + input);
    }

    //check for Scenario One: Look room
    if(thing.indexOf('room')!=-1) {
      //Return Description of the room
      response = room.roomDescription;
      emitDorisText(response,input_roomId);
    } 

    //check for Scenario Two: Look door
    else if (thing.indexOf('door')!=-1) {  
      if(thing=="left door") {
        //query the doorId of room.roomLeftDoor
        doors.find({doorId:room.roomLeftDoor},function(error,doors){
          var door = doors[0]; 
          //return the description of the left door
          response = door.description;
          emitDorisText(response,input_roomId);
        });//end door query
      } 

      else if(thing=="right door") {  
        //query the doorId of room.roomRightDoor
        doors.find({doorId:room.roomRightDoor},function(error,doors){
          var door = doors[0]; 
          
          //return the description of the right door
          response = door.description;
          emitDorisText(response,input_roomId);
        });//end door query
      } else {
        //Return Cannot Find Door
        response = cannotFindDoorMessage;
        emitDorisText(response,input_roomId);
      }
    } 
    //otherwise, Scenario Three: Look item
    else {
      //Pull from Items Database
      items.find({itemName:thing},function(error,items){
        var item = items[0];
         
        //Confirm that object exists
        if(item==undefined){
          response = cannotFindMessage;
          emitDorisText(response,input_roomId);

        } 
        //Confirm that object is visible and is in room
        else if (item.visible&&room.itemsInRoom.indexOf(item.itemName)!=-1){
          response = item.description;       
          emitDorisText(response,input_roomId);

          //Check for potential item activation
          if(item.activateCmd.toLowerCase()==verb&&item.activated){
            activateItem(item, input_roomId);
          }
        } else {
          //Return response to client
          response = cannotFindMessage;
          emitDorisText(response,input_roomId);
        }
      }); //end of item.find()
    } //end of Scenario: look item    
  }); //end of userroom.find()
} //End of onLookAction

function onPickupAction(input, input_roomId, emitDorisText, emitInvChange) {
  var response = '';
  
  //Confirm second input is 'up'
  if (input.substring(5,7) != 'up'){
    emitDorisText(errorMessage,input_roomId);
  } else {  
    //Pull item ID from input
    var item_name = input.substring(input.indexOf('up ')+3);

    //Find Current Room
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];

      //Check if item is in room 
      if (room.itemsInRoom.indexOf(item_name)!=-1){
         
        //Get Item from db
        items.find({itemName:item_name},function(error,items){
          //(assumption is that the valid names are listed in initial doorList)
          var item = items[0];

          //check if visible
          if(item.visible!=true) {
            response = cannotFindItemMessage;
            emitDorisText(response,input_roomId);
          }
          //check if canBeHeld
          else if(item.canBeHeld!=true){
            response = itemCannotBeHeldMessage;
            emitDorisText(response,input_roomId);          
          } else {
            //Add item to user Inventory
            var updatedUserInv = room.userInventory;
            updatedUserInv.push(item.itemName);

            //Remove item from itemsInRoom List
            var roomItemsList = room.itemsInRoom;
            roomItemsList.remove(item.itemName);

            //Update Arrays in db
            var whereQuery = {roomId: room.roomId};
            var change = {userInventory: updatedUserInv, itemsInRoom: roomItemsList};
            userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

              //Send new inventory to the front end 
              sendInventoryToClient(rawResponse.userInventory, input_roomId, emitInvChange);
              
              //Send Doris Text to front end
              response = "You picked up the " + item.itemName;
              emitDorisText(response, input_roomId);

            });//end update userrooms
          }
        });//end items query
      }

      //Check if item is in inventory
      else if (room.userInventory.indexOf(item_name)!=-1){
        response = itemAlreadyHeldMessage;
        emitDorisText(response,input_roomId);
      } 
      //Item does not exist in room
      else {
        response = cannotFindItemMessage;
        emitDorisText(response,input_roomId);
      }
    }); //End userrooms query
  }
} //end onPickupFunction

/*
Two Use Scenarios: 

Use 'password' on door/item
Check if item/door and/or exists and/or activated already?
Check if password matches
if password matches, activate object / open door
  open door: connect two different rooms
  activate object: ??? on Activate action? 

use key on door/item
Check if item exists
Check if user has item that they're using 
Check if item/door and/or exists and/or activated already?
Check if item/door keyItem matches key
if key matches, activate object / open door
  oper door: connect two different rooms function openDoor()
  activate object: ??? on Activate action? function activateItem()
*/

function onUseAction(input, input_roomId, emitDorisText, emitRoomChange, emitInvChange){ 
   //pull item/target from input
  var usedItem = input.substring(input.indexOf(" ")+1,input.indexOf(" on "));
  var targetName = input.substring(input.indexOf(" on ")+4);

  //Pull current userroom from db
  userrooms.find({roomId:input_roomId},function(error,rooms) { 
      var room = rooms[0];

    //** If target is a door **//

      //if target is left or right door:
      if(targetName == 'left door'||targetName == 'right door') {
        var targetDoorId = '';
        //choose which door to use item on 
        if(targetName=='left door') {
          targetDoorId = room.roomLeftDoor;
        } else {
          targetDoorId = room.roomRightDoor;
        }
        //look up actual door
        doors.find({doorId:targetDoorId},function(error,doors){
          var door = doors[0];

          //Get Current Stage
          var currStage = door.currentStage;
          if(currStage==undefined){
            currStage = 1;
          }
          var doorStage = door.doorStageList[currStage];

          //if door is already unlocked ERROR
          if(door.locked!=true){
            response = doorAlreadyUnlocked;
            emitDorisText(response,input_roomId);
          }
          //if using password
          else if(doorStage.keyType = 'password') {
            //password matches door password
            if(usedItem == ("'" + doorStage.key + "'") ){
              openDoor(door,input_roomId);
              response = doorStage.onStageCompleteMessage;
              if(response==undefined){ //Check if onStageCompleteMessage was ever defined
                response = "You opened up the " + targetDoorId + ".";
              }
              emitDorisText(response,input_roomId);
            }
            //wrong password entered ERRORs
            else {
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
          }
          //otherwise assume using item
          //if user does not have item he/she is trying to use ERROR
          else if (room.userInventory.indexOf(usedItem)==-1){ 
            response = notHoldingMessage;
            emitDorisText(response,input_roomId);
          }
          //user has item he/she is trying to use
          else {
            //correct key
            if(usedItem == doorStage.key){
              openDoor(door,input_roomId);
              response = doorStage.postStageCompleteDesc;
              emitDorisText(response,input_roomId);
            }
            //incorrect key ERROR 
            else {
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
          }
        });//end door query
      }
      //if target is another door? send ERROR
      else if(targetName.indexOf(' door')!=-1){
        response = cannotFindDoorMessage;
        emitDorisText(response,input_roomId);
      }
    

      //** If target is an item **//
          
      else {
        items.find({itemName:usedItem},function(error,items){
          var item = items[0];
          //If target item exists and is in current room
          if(item!=undefined&&room.itemsInRoom.indexOf(item)!=-1){
             //if item is already activate ERROR
            if(item.activated!=true){
              response = failedUseMessage;
              emitDorisText(response,input_roomId);
            }
            //if using password
            else if(item.keyType = 'password') {
              //password matches door password
              if(usedItem = ("'" + item.password + "'") ){
                activateItem(item,input_roomId);
                response = item.onActivateMessage;
                emitDorisText(response,input_roomId);
              }
              //wrong password entered ERROR
              else {
                response = failedUseMessage;
                emitDorisText(response,input_roomId);
              }
            }
            //otherwise assume using item
            //if user does not have item he/she is trying to use ERROR
            else if (room.userInventory.indexOf(usedItem)==-1){ 
              response = notHoldingMessage;
              emitDorisText(response,input_roomId);
            }
            //user has item he/she is trying to use
            else {
              //correct key
              if(usedItem == item.key){
                activateItem(item,input_roomId);
                response = item.onActivateMessage;
                emitDorisText(response,input_roomId);
              }
              //incorrect key ERROR 
              else {
                response = failedUseMessage;
                emitDorisText(response,input_roomId);
              }
            }
          }
          //Target Item does not exist or is not in room ERROR
          else {
            response = cannotFindItemMessage;
            emitDorisText(response,input_roomId);
          }
        });//end item query
      }
  });//end userroom query
} //end onUseAction()

function onSayAction(input, input_roomId, emitDorisText, emitRoomChange){
}//End onSayAction

function onGiveAction(input, startRoomId, emitDorisText, emitInvChange) {
  var response = '';
  var input_roomId = startRoomId;

  //If input is NOT correctly formatted ERROR
  if (input.indexOf(' to ') == -1){
    emitDorisText(errorMessage,input_roomId);
  } 
  //If input IS correctly formatted
  else { 
    //pull item/target from input
    var givenItem = input.substring(input.indexOf(" ")+1,input.indexOf(" to "));
    var targetName = input.substring(input.indexOf(" to ")+4);
    targetName = targetName.substring(0,1).toUpperCase()+targetName.substring(1); //Capitalize UserName for query

    //Pull current userroom from db
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var userroom = rooms[0];
      
      //confirm that user currently has input_item within userInventory / exists
      if(userroom.userInventory.indexOf(givenItem)!=-1){
        //Pull target userroom from db
        userrooms.find({userName:targetName},function(error,rooms2){
          var userroom2 = rooms2[0];

          //if user exists
          if (userroom2 != undefined) {

            //check if targetUserroom is linked to currentUserroom AND check if currentUserroom is linked to targetUserroom
            if((userroom.linkedRooms.indexOf(userroom2.roomId)!=-1)&&(userroom2.linkedRooms.indexOf(userroom.roomId)!=-1)){

              //remove item from currentUserroom userInventory
              var updatedUserInv = userroom.userInventory;
              updatedUserInv.remove(givenItem);

              //add item to targetUserroom userInventory 
              var updatedTargetInv = userroom2.userInventory;
              updatedTargetInv.push(givenItem);

              //Apply changes to db
              var whereQuery1 = {roomId: userroom.roomId};
              var change1 = {userInventory: updatedUserInv};
              
              var whereQuery2 = {roomId: userroom2.roomId};
              var change2 = {userInventory: updatedTargetInv};

              //send new Inventories to currentUserroom
              userrooms.findOneAndUpdate(whereQuery1, change1, function(err,rawResponse){
                sendInventoryToClient(rawResponse.userInventory, input_roomId, emitInvChange);
                
                //Send Doris Text to front end
                response = "You gave away the " + givenItem;
                emitDorisText(response, input_roomId);
              });

              //send new Inventories to targetUserroom
              userrooms.findOneAndUpdate(whereQuery2, change2, function(err,rawResponse){
                sendInventoryToClient(rawResponse.userInventory, input_roomId, emitInvChange);
              });       
            } 
            //if both rooms aren't linked: ERROR
            else {
              response = roomsAreNotLinkedMessage;
              emitDorisText(response,input_roomId);
            }
          }
          //if user does not exist ERROR
          else {
            response = userDoesNotExistMessage;
            emitDorisText(response,input_roomId);
          }
        });//end of second userroom query
      } 
      //if item not in inventory or does not exist ERROR
      else {
        response = notHoldingMessage;
        emitDorisText(response,input_roomId);
      }
    });//End initial userroom query
  }
} //End onGiveAction


var noLightSwitchMessage = "You aren't able to find a light switch to toggle.";

//TODO Hardcode logic for changing lights in Stage 4 Task 1
function onToggleAction(input, input_roomId, emitDorisText, emitRoomChange) {
  var response = '';

  //Accept one of two inputs
  if(input=='toggle light'||input=='toggle light switch') {
    //pull current room from db query
     userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
      
      //check that room has light switch 
      if(room.roomLightSwitch!=undefined){
        var lightStatus = "on"; //assume light is off, so we are turning it on
        var changeLightStatus = true; 
        if(room.roomLightSwitch==true){ //but if light is on, turn it off.
          lightStatus = "off"
          changeLightStatus = false;
        } 
        response = "You turned " + lightStatus + " the light.";
        emitDorisText(response, input_roomId);

        //TODO changeRoomState Here        
      } 
      //No Light Switch available to toggle ERROR
      else {
        response = noLightSwitchMessage;
        emitDorisText(response, input_roomId);
      }
    });
  } 
  //Invalid input ERROR
  else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onToggleAction

function onHelpAction(input, input_roomId, emitDorisText, emitRoomChange) { 
  var response = '';
  var helpImagePath = "images/commands.png";

  //check for correct input
  if(input=='help'){
    emitDorisText(helpMessage,input_roomId);
    emitRoomChange(helpImagePath,input_roomId);
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onHelpAction

function onBackAction(input, input_roomId, emitDorisText, emitRoomChange) { 
  var response = '';

  //check for correct input
  if(input=='back'){
    userrooms.find({roomId:input_roomId},function(error,rooms){
      var room = rooms[0];
      response = backMessage;
      emitDorisText(response,input_roomId);
      emitRoomChange(room.roomImagePath,room.roomId);  
    }); //end room query
  } else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
} //End onBackAction


//TODO Implement this function where needed
//Function created to pick up actions after item command
function onServerPickUpAction(server_input, input_roomId, room, emitInvChange){
  //Pull item ID from input
  var item_name = input.substring(input.indexOf('up ')+3);

  //Get Item from db
  items.find({itemName:item_name},function(error,items){
    //(assumption is that the valid names are listed in initial doorList)
    var item = items[0];

    //check if item exists DATA ERROR
    if(item==undefined) {
      console.log("DATA ERROR ON SERVER PICKUP: " + server_input);
    }
     else {
      //Add item to user Inventory
      var updatedUserInv = room.userInventory;
      updatedUserInv.push(item.itemName);

      //Update Arrays in db
      var whereQuery = {roomId: room.roomId};
      var change = {userInventory: updatedUserInv, itemsInRoom: roomItemsList};
      userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

      //Send new inventory to the front end 
      sendInventoryToClient(rawResponse.userInventory, input_roomId, emitInvChange);   
      });
    }
  });
} //endServerPickup


// (Assumed: the item being passed into this function is activated now)
//    Set activated = true
//    set description to activated description
//    run onActivateItem command

function activateItem(item, input_roomId){

  //Handle Item Stages
  var stageNum = item.currentStage;
  var activationSet = item.activated;
  var itemDescription = item.description;
  if(stageNum==undefined){
    stageNum = 1;
  }

  //Advance to final stage
  if(stageNum==item.itemStageList.length){
    //item is activated
    activationSet = false;
    itemDescription = item.itemStageList[stageNum-1].postStageCompleteDesc;
  } 

  //item not yet activated, advance to next stage
  else {
    stageNum += 1;
    itemDescription = (item.itemStageList[stageNum-1]).postStageCompleteDesc;

  }

  //update db function
  var whereQuery = {itemName: item.itemName};
  var change = {activated: activationSet, currentStage: stageNum, description: itemDescription};
  items.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
    //TODO insert testlog
    //TODO handle onUnLockedCmd
  });

} //End on activateItem



//(Assumed: conditions to open door have been met)
// set locked = false
// set description to unlocked description
// run onUnlocked cmd
function openDoor(door, input_roomId){

  //add door to linkedRooms
  var newRoom = door.doorId.substring(door.doorId.length-1);
  var updatedLinkedRooms = door.linkedRooms;

  //Handle Door Stages
  var stageNum = door.currentStage;
  var lockedSet = door.locked;
  var doorDescription = door.description;
  if(stageNum==undefined){
    stageNum = 1;
  }

  //Advance to final stage
  if(stageNum==door.doorStageList.length){
    //door is unlocked
    lockedSet = false;
    doorDescription = door.doorStageList[stageNum-1].postStageCompleteDesc;

    //set locked to false, add door to linked doors
    if(updatedLinkedRooms==undefined) {
      updatedLinkedRooms = [];
    }
    updatedLinkedRooms.push(newRoom);
  } 

  //door not yet unlocked, advance to next stage
  else {
    stageNum += 1;
    doorDescription = (door.doorStageList[stageNum-1]).postStageCompleteDesc;

  }

  //update db function
  var whereQuery = {doorId: door.doorId};
  var change = {locked: lockedSet, currentStage: stageNum, description: doorDescription, linkedRooms: updatedLinkedRooms};
  doors.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
    //TODO insert testlog
    //TODO handle onUnLockedCmd
  });
}//end openDoor 

//TODO Fill this out - how to handle room changes??
//consoleChange is true if the changeRoomState needs to send a change to the front end Console
function changeRoomState(room, consoleChange){

}


function sendInventoryToClient(inventoryArray, input_roomId, emitInvChange){
  var inventoryLength = inventoryArray.length;
  var responseArr = [];
  for(var i = 0; i < inventoryArray.length; i++){
    items.find({itemName:inventoryArray[i]},function(error,items){
      item = items[0];
      responseArr.push(item);

        //this activates on conclusion
      if(responseArr.length==inventoryLength){
        emitInvChange(responseArr,input_roomId);
      }
    });//end items query  
  }
}


/*Start Server*/ 
http.listen(3774, function(){
  console.log('listening on *:3774');
//  mongodb://localhost/trapped<use trapped; on mongo>/schema<collection>_name 
  mongoose.connect('mongodb://localhost/trapped_db');
});

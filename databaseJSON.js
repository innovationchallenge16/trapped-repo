//Reset database
use trapped_db;

db.items.drop();
db.userrooms.drop();
db.doors.drop();  
db.testlogs.drop();
db.createCollection("items");
db.createCollection("userrooms");
db.createCollection("doors");
db.createCollection("testlogs");

//UserRooms - 8
//TODO ADD All roomSafes
db.userrooms.insert(
 [{	
 //1a: use password from physical jigsaw puzzle on doorAB

	roomId: "A",
	roomDescription: "You seem to be trapped in a large walk-in closet. Who needs this much space? You see a secure safe, a lightbulb with a switch, a spiffy charger that could be useful if you only had something to charge, and most importantly a button that is begging to be pushed. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorAP", 
	roomRightDoor: "doorAB",
	roomImagePath: "images/rooms/closet.png",
	roomImageList: ["images/rooms/closet.png","images/rooms/closetOn.png","images/rooms/closet.png","images/rooms/closetOn.png","images/rooms/closetOpenOff.png","images/rooms/closetOpenOn.png"],
	roomSafe: "secure safe",
	roomSafeOpen:  false, //if safe exists, this is true
	roomLightLetter: "P",
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "",
  heavyItemMoved: true, //This is true because theres no heavy item in the room
	linkedRooms: ["A"],
	itemsInRoom: ["strange button", "spiffy charger", "secure safe"],
	userName: "Amy",
	userInventory: ["note 1a","note 3a-3"]
 },
 {
  //TODO CHECK Garage ImageList
 //1b: use password from winning rock paper scissors on doorBA
	roomId: "B",
	roomDescription: "As you look around you suppress a slight shiver. It is quite cold in here. You see a lightbulb with a switch, but nothing else really  draws your eye. The only ways out that you can see are two doors on either side of the room.",
	roomLeftDoor: "doorBC", 
	roomRightDoor: "doorBA",
	roomImagePath: "images/rooms/garage.png", 
	roomImageList: ["images/rooms/garage.png","images/rooms/garageDarkOn.png","images/rooms/garage.png","images/rooms/garageDarkOn.png","images/rooms/garageUpOff.png","images/rooms/garageDarkOn.png"],
	roomSafe: "hidden panel",
	roomSafeOpen:  false, //for this, true if panel  open, false is not
	roomLightLetter: "I",
  roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "",
  heavyItemMoved: true, 
	itemsInRoom: ["hidden panel"],
	linkedRooms: ["B"],
	userName: "Brian",
	userInventory: ["note 1b", "note 2b", "note 3b-1"]
 },
 {
	roomId: "C",
	roomDescription: "You find yourself trapped in some sort of library. You see a lightbulb with a switch and a bookshelf on either side of the room. Every so often you can hear a faint scratching sound. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorCD", 
	roomRightDoor: "doorCB",
	roomImagePath: "images/rooms/library.png",
	roomImageList: ["images/rooms/library.png","images/rooms/libraryOn.png","images/rooms/libraryMovedOff.png","images/rooms/libraryMoved.png","images/rooms/libraryMovedOff.png","images/rooms/libraryMoved.png"],
	roomSafe: "",
	roomSafeOpen:  true, //if safe does not exist, this is true
	roomLightLetter: "I",
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "couch",
  heavyItemMoved: false, //false on start, true when moved
	itemsInRoom: ["couch", "bookshelf", "mouse"],
	linkedRooms: ["C"],
	userName: "Carlos",
	userInventory: ["note 1c","note 3c-4"]
 },
 {
	roomId: "D",
	roomDescription: "You find yourself in a small dining room with a lightbulb and switch, some yummy cheese. The figure in the painting on the wall looks familiar to you. The only ways out that you can see are two doors on either side of the room. ",
	roomLeftDoor: "doorDE", 
	roomRightDoor: "doorDC",
	roomImagePath: "images/rooms/dining.png",
	roomImageList: ["images/rooms/dining.png","images/rooms/diningOn.png","images/rooms/dining.png","images/rooms/diningOn.png","images/rooms/dining.png","images/rooms/diningOn.png"],
	roomSafe: "",
	roomSafeOpen:  true, //if safe does not exist, this is true
	roomLightLetter: "R",
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "",
  heavyItemMoved: true, //false on start, true when moved
	itemsInRoom: ["yummy cheese"],
	linkedRooms: ["D"],
	userName: "Dean",
	userInventory: ["note 1d", "note 2d","note 3d-2"]
 },
  {
	roomId: "E",
	roomDescription: "You find yourself in a clean office. As you look around you notice a light bulb and switch and a number panel. The clock on the wall does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorEF", 
	roomRightDoor: "doorED",
	roomImagePath: "images/rooms/office.png",
	roomImageList: ["images/rooms/office.png","images/rooms/officeOn.png","images/rooms/office.png","images/rooms/officeOn.png","images/rooms/office.png","images/rooms/officeOn.png"],
	roomSafe: "panel e",
	roomSafeOpen:  false, //if safe does not exist, this is true
	roomLightLetter: "S",	
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "",
  heavyItemMoved: true, //false when start, true when moved
	itemsInRoom: ["number panel"],
	linkedRooms: ["E"],
	userName: "Elena",
	userInventory: ["note 1e","note 1e-2","note 3e-4"]
 },
 {
	roomId: "F",
	roomDescription: "You find yourself in a retro looking kitchen. As you look around you see a lightbulb and switch, a plain safe, and a fridge with magnets on it. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorFG", 
	roomRightDoor: "doorFE",
	roomImagePath: "images/rooms/kitchen.png",
	roomImageList: ["images/rooms/kitchen.png","images/rooms/kitchenOn.png","images/rooms/kitchenMovedOff.png","images/rooms/kitchenMovedOn.png","images/rooms/kitchenMovedOffOpen.png","images/rooms/kitchenMovedOnOpen.png"],
	roomSafe: "plain safe",
	roomSafeOpen:  false, //if safe does not exist, this is true
	roomLightLetter: "T",
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "fridge",
  heavyItemMoved: false, //false when start, true when moved
	itemsInRoom: ["fridge", "plain safe"],
	linkedRooms: ["F"],
	userName: "Frank",
	userInventory: ["note 1f","note 3f-3"]
 },
 { //TODO add special use case toc CheckRoomChange for G
	roomId: "G",
	roomDescription: "You find yourself in a strange room. You think that this room may be used to clean clothes but you are not sure how. Your mother always does it for you anyway. As you look around a lightbulb with a switch, a safe g and some sort of charger g catches your eye. There are three doors in this room, one on either side of the room and a center door which looks like it has been nailed down. I wonder what is behind there?",
	roomLeftDoor: "doorGH", 
	roomRightDoor: "doorGF",
	roomImagePath: "images/rooms/laundryRoom.png",
	roomImageList: ["images/rooms/laundryRoom.png","images/rooms/laundryRoomOpen.png","images/rooms/laundryRoom.png","images/rooms/laundryRoomOpen.png","images/rooms/laundryRoom.png","images/rooms/laundryRoomOpen.png",],
	roomSafe: "safe g",
	roomSafeOpen:  false, //if safe does not exist, this is true
	roomLightSwitch: false, //For this Object, the light is the center door
  heavyItem: "",
  heavyItemMoved: true, //false when start, true when moved
  centerDoorGOpen:true,
	itemsInRoom: ["charger g", "safe g","center door","vending machine"],
	linkedRooms: ["G"],
	userName: "Gertrude",
	userInventory: ["note 1g","note 3g-1","note 4g"]
 },
 { 
  //For practical purposes, heavyItemMoved references panelh
	roomId: "H",
	roomDescription: "You find yourself in a bedroom of some sorts. You see a remote that looks dead, and for some reason a safe h on a shelf. Who puts a safe on a shelf? Near the shelf there is a lightbulb but no switch. The only ways out that you can see are the two doors on either side of the room.",
	roomLeftDoor: "doorHI", 
	roomRightDoor: "doorHG",
	roomImagePath: "images/rooms/guestRoom.png",
	roomImageList: ["images/rooms/guestRoom.png","images/rooms/guestRoom.png","images/rooms/guestRoomUp.png","images/rooms/guestRoomUp.png","images/rooms/guestRoomOpen.png","images/rooms/guestRoomOpen.png"],
	roomSafe: "secret panel",
	roomSafeOpen:  false, //if safe does not exist, this is true
	roomLightSwitch: false, //by default, light is off and this is true
  heavyItem: "safe h",
  heavyItemMoved: false, //false when start, true when moved
	itemsInRoom: ["safe","secret panel", "old remote"],
	linkedRooms: ["H"],
	userName: "Hannah",
	userInventory: ["note 1h", "note 2h","note 3h-2"]
 }
 ]);


////ITEMS LIST//// - 45

db.items.insert([
//ALL NOTES
 {
  itemId: "note1A",
  itemName: "note 1a",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1A.png" 
 },
  {
  itemId: "note3A-3",
  itemName: "note 3a-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are 's crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: "images/notes/note3A-3.png"
 },
 {
  itemId: "note1B",
  itemName: "note 1b",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1B.png"
 },
 {
  itemId: "note2B",
  itemName: "note 2b",
  description: "Day 5: I know that there is a hidden panel in here somewhere… Maybe if I tell Doris a certain word it will open",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2B.png" 
 },
 {
  itemId: "note3B-1",
  itemName: "note 3b-1",
  description: "Day 384:  , , , and . There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3B-1.png" 
 },
 {
  itemId: "note1C",
  itemName: "note 1c",
  description: "Day 23: Doris has given me the following clue for one of the door passwords: What year did two companies merge in order to form ConcoPhillips? How am I supposed to figure it out without google?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1C.png" 
 },
 {
  itemId: "note3C-4",
  itemName: "note 3c-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like  did. Or was it ? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3C-4.png" 
 },
 {
  itemId: "note1D",
  itemName: "note 1d",
  description: "Day 73: I know I have seen the man in the painting before. Could it have been when I was looking up information on ConocoPhillips the night before I got here? Maybe it is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1D.png" 
 },
  {
  itemId: "note2D",
  itemName: "note 2d",
  description: "Day 91: The stratching sounds will not stop. They are driving me crazy. I think they are coming from under the couch next door.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2D.png" 
 },
 {
  itemId: "note3D-2",
  itemName: "note 3d-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is . Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3D-2.png" 
 },
 {
  itemId: "note1E",
  itemName: "note 1e",
  description: "Day 52: Still can not figure out the password to one of the doors. The only thing Doris will say when I ask is Tick Toc...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1E.png" 
 },
 {
  itemId: "note1E-2",
  itemName: "note 1e-2",
  description: "Day 10: I have tried every combination I can think of with birthdays on the safe. What does Doris mean by the the birthday of the one who is most mature?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2E.png" 
 },
 {
  itemId: "note3E-4",
  itemName: "note 3e-4",
  description: "Day 101: Today is not a good day. Yet another of our small group has fallen ill to the mystious illness going around. We all hope that they will recover. I have stuffed the sheets from my bed into the crack of the doors and have barricaded myself in my room. I will not get sick like  did. Or was it ? Now that I think about it I do not remember their name.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3E-4.png" 
 },
 {
  itemId: "note1F",
  itemName: "note 1f",
  description: "Day 67: I still cannot unscramble the letters on the fridge. I know it has to do with ConocoPhillips but what else? Come on THINK! I think the two words that they spell is the password to one of the doors",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1F.png" 
 },
 {
  itemId: "note3F-3",
  itemName: "note 3f-3",
  description: "Day 23: Captivity has been quite dull. The only thing that breaks the long periods of boredom are 's crazy antics. Yesterday they made up an entire song and dance about how horrible Doris is. It was hilarious. Unfortunately it made Doris furious. Hopefully nothing bad happens to them.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3F-3.png" 
 },
 {
  itemId: "note1G",
  itemName: "note 1g",
  description: "Look under your chair...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1G.png" 
 },
 {
  itemId: "note3G-1",
  itemName: "note 3g-1",
  description: "Day 384:  , , , and . There are only four of us now, technically three. We have been trapped for over a year now. Maybe more. Sometimes it is hard to remember to write everyday. I can barley remember why we are here. They told us that we were going to be mentors but no one know what we were signing up for. One has befriended Doris and has become a guard. The second made Doris angry and is being forced to act as the Janitor. The third has passed and become a ghost of his former self. I feel that I may be losing my mind which is why I now have to write everything down. Problem is I don't remember who is who....",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notesnote3G-1.png" 
 },
 {
  itemId: "note4G",
  itemName: "note 4g",
  description: "Day 107: We have noticed that there are 6 lightbulbs in 6 different rooms. Each are labeled something with a letter and have their own light switch. Additionally there is one light bulb with no letter or a switch. Perhaps if the bulbs were lit in the correct order they would light up. But what do the letters mean? Maybe something to do with ConocoPhillips?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note4G.png" //TODO MAKE THIS NOTE 
 },
 {
  itemId: "note1H",
  itemName: "note 1h",
  description: "Day 101: I seem to have gone blind from the pink. Too bad because I have finally tricked Doris into giving me a clue to the password to one of the doors. Doris said something about inspecting the code comments? What does that even mean though?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1H.png" 
 },
 {
  itemId: "note2H",
  itemName: "note 2h",
  description: "Day 5: I know that there is a secret panel in here somewhere…Doris has mentioned that its the most important thing for ConocoPhillips...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2H.png" 
 },
 {
  itemId: "note3H-2",
  itemName: "note 3h-2",
  description: "Day 3: At first what we feared was a joke has quickly turned out to be anything but. Doris has taken over the compound and refuses to let anyone out. The only person I think she gets along with is . Hopefully she will realize how crazy this is and let us out soon. She can't keep us here forever can she?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note3H-2.png" 
 }]);

//ROOM A ITEMS --24
db.items.insert([
 {
  itemId: "chargerA",
  itemName: "spiffy charger",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: "images/items/chargerA.png"
 }, 
 {
  itemId: "safeA",
  itemName: "secure safe",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "plastic remote",
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up key bc"
                }],
  imagePath: "" 
 },
  {
  itemId: "keyBC", //found in room A, in safeA
  itemName: "key bc",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key.png" 
 },

 //ROOM B ITEMS
 {
  itemId: "panelB",
  itemName: "hidden panel",
  description: "This is a hidden panel - if you tell it a password, it may open for you.",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password",
                    key: "integrity",
                    onStageCompleteDesc: "It's just a panel.",
                    onStageCompleteMessage: "You found a remote! I wonder what you could use it on...",
                    postStageCmd: "pick up plastic remote"
                }],
  imagePath: "" 
 },
 {
  itemId: "remoteB",
  itemName: "plastic remote",
  description: "This remote looks dead...",
  visible: true,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "spiffy charger",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote."
                }],
  imagePath: "images/items/remoteB.png" 
 },
 //ROOM C ITEMS
 {//TODO CHECK THIS ITEM
  itemId: "couchC",
  itemName: "couch",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "push",
                    key: "D",
                    onStageCompleteDesc: "Now that you've moved the couch once, you really don't feel like moving it any more.",
                    onStageCompleteMessage: "You found a mouse sitting behind the bookself...he's holding a key!"
                }],
  imagePath: "" 
 },
 {
  itemId: "bookshelfC",
  itemName: "bookshelf",
  description: "There's an assortment of books here, Harry Potter, A Tale of Two Cities, Wild...you should pull one that you're interested in. Who knows, maybe something will happen?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "pull",
                    key: "", //FINDME PERSONALIZED
                    onStageCompleteDesc: "It was pretty smart to pull that book. I don't think you can do anything else here.",
                    onStageCompleteMessage: "What a brilliant idea! You found an extension cord.",
                    postStageCmd: "pick up extension cord"
                }],
  imagePath: "" 
 },
 {
  itemId: "bookC-1",
  itemName: "harry potter",
  description: "Maybe something magical will happen if you pull this from bookshelf?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "pull",
                    key: "harry potter", //FINDME PERSONALIZED
                    onStageCompleteDesc: "It was pretty smart to pull this book. You don't have time to read this book now.",
                    onStageCompleteMessage: "What a brilliant idea! You found an extension cord.",
                    postStageCmd: "pick up extension cord"
                }],
  imagePath: "" 
 },
 {
  itemId: "mouseC", //found behind couch
  itemName: "mouse",
  description: "I wonder if the critter has anything I need?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "yummy cheese",
                    onStageCompleteDesc: "The mouse scattered away after you fed him. You can't see him anymore.",
                    onStageCompleteMessage: "The mouse appreciated your cheese, and left a key behind him!",
                    postStageCmd: "pick up key cb"
                }],
  imagePath: "" 
 },
 {
  itemId: "extensionCordC", //found behind couch
  itemName: "extension cord",
  description: "this cord could connect a shorter cord to power...",
  visible: true,
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/extensionC.png" 
 },
 {
  itemId: "keycb", //found from the mouse
  itemName: "key cb",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key2.png" 
 },
 //ROOM D ITEMS
 {
  itemId: "cheeseD",
  itemName: "yummy cheese",
  description: "yummy cheese is yummy",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/cheeseO.png" 
 },
 //ROOM E ITEMS
 {
  itemId: "panelE",
  itemName: "number panel",
  description: "The panel in this room has a keypad, indicating you can enter numbers. No spaces, hashes, or asterisks though. #unfair.",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based vending machine hint
                    key: "1718191920202224", //FINDME Personalized! - ages A-H sorted by least to most
                    onStageCompleteDesc: "The panel is open",
                    onStageCompleteMessage: "You found a power plug...not connected to anything. Is there anything that needs electricity?",
                    postStageCmd: "pick up power plug"
                }],
  imagePath: "" 
 },
 {
  itemId: "power plugE",
  itemName: "power plug",
  description: "This power plug could be used on something to give it power...",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/extensionC.png" 
 },
 //ROOM F ITEMS
 {
  itemId: "heavyItemF",
  itemName: "fridge",
  description: "This is heavy, I wonder if there's anything behind it?",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "push",
                    key: "E",
                    onStageCompleteDesc: "There's a safe behind this...it looks like it needs a combination",
                    onStageCompleteMessage: "You found a hidden safe!"
                }],
  imagePath: "" 
 },
 {
  itemId: "safeF",
  itemName: "plain safe",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based on a hint note1E-2
                    key: "121295", //FINDME Personalized! userE birthday
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up key fg"
                }],
  imagePath: "" 
 },
 {
  itemId: "keyFG",
  itemName: "key fg",
  description: "Where can I possibly use this?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key.png" 
 },
 //ROOM G ITEMS
 {
  itemId: "safeG",
  itemName: "safe g",
  description: "I wonder if there's anything useful in here...",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "old remote",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up key gf"
                }],
  imagePath: "" 
 },
 {
  itemId: "chargerG",
  itemName: "charger g",
  description: "This item is used to charge electronic objects",
  visible: true,
  canBeHeld: true,
  unActivated: false,
  imagePath: "images/items/chargerH.png"
 },
 {
  itemId: "keyGF",
  itemName: "key gf",
  description: "This key looks different...it has the words 'exploration and production' engraved on it.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key2.png" 
 },
 {
  itemId: "center door G",
  itemName: "center door",
  description: "This door looks like its nailed shut",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "hammer",
                    onStageCompleteDesc: "This door looks loose, maybe if you try to pull it, you'll see some results...",
                    onStageCompleteMessage: "You removed the nails, and the door looks like it just needs to be pulled"
                },
                {
                    keyType: "pull",
                    key: "ABCDEFH",
                    onStageCompleteDesc: "You moved the door to the side, great work!",
                    onStageCompleteMessage: "You pulled away the door and revealed a vending machine!"
                }],
  imagePath: "" 
 },
 {
  itemId: "vendingMachineG",
  itemName: "vending machine",
  description: "This vending machine clearly is dead. I think all of the food in it is probably bad. You see a note on the vending machine, it says 'Theres a panel you should open. Use all of the players' birthdays, from youngest to oldest.' Another note reads 'one of you has a favorite book. you should pick it up from the couch.' ", //enter every players birthday in backwords order
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "power plug",
                    onStageCompleteDesc: "The plug doesn't quite reach the wall, but you're so close! Got something to extend it?",
                    onStageCompleteMessage: "The plug doesn't quite reach the wall, but you're so close!"
                },
                {
                    keyType: "key",
                    key: "extension cord",
                    onStageCompleteDesc: "The vending machine is plugged into the wall! Now all you need is some money to get a key from it.",
                    onStageCompleteMessage: "That worked! Now the vending machine has power...got money?"
                },
                {
                    keyType: "key",
                    key: "money",
                    onStageCompleteDesc: "There's nothing else to do here",
                    onStageCompleteMessage: "That worked, and you got a key for your trouble.",
                    postStageCmd: "pick up final key"
                }],
  imagePath: "" 
 },
 {
  itemId: "final key",
  itemName: "final key",
  description: "This key feels more important than any other key you've seen this whole game.",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key.png" 
 },
 {
 //ROOM H ITEMS
 itemId: "panel H",
  itemName: "secret panel",
  description: "This is a secret panel",
  visible: true,
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //user derives this based on safety obviously being the most important thing to COP
                    key: "safety",
                    onStageCompleteDesc: "You found a remote! I wonder what you could use it on...",
                    onStageCompleteMessage: "The remote has been put in your inventory",
                    postStageCmd: "pick up old remote"
                }],
  imagePath: "" 
 },
  {
  itemId: "remoteH",
  itemName: "old remote",
  description: "This remote looks dead...",
  visible: true,
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "charger g",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote."
                }],
  imagePath: "images/items/remoteB.png" 
 },
 {
  itemId: "safeH", //TODO THIS MIGHT NOT BE AN ISSUE
  itemName: "safe h",
  description: "",
  visible: true,
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password",
                    key: "safety",
                    onStageCompleteDesc: "the safet is emplty",
                    onStageCompleteMessage: "The charger fully charged the remote."
                }],
  imagePath: "" 
 },
 {
  itemId: "hammerH", 
  itemName: "hammer",
  description: "This hammer looks perfect for removing nails from something...do I know anywhere that nails are?",
  visible: true,
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/hammerH.png" 
 }
 ]);

//Doors - 15
db.doors.insert(
 [{
	doorId: "doorAP",
	description: "This door doesn't seem to give you any clue of how you will open it.",
	locked: true,
	doorStageList: 
	[{
    keyType: "key", //received from vending machine
    key: "final key",
    onStageCompleteDesc: "You are the champion - you win!"
  }]
 },
 {
	doorId: "doorAB",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this goes on the back of a jigsaw puzzle
		key: "kittens123", //THIS CANNOT CHANGE, already on puzzle
    onStageCompleteDesc: "The door is open"

	}]
 },
 {
	doorId: "doorBA",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this is received after rock paper scissors
		key: "puppies456",
    onStageCompleteDesc: "The door is open"

	}]
 },
 { 
	doorId: "doorBC",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "key bc", //user got this from safeA
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorCD",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1c
		key: "2002",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorDC",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1d
		key: "frank phillips",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorCB",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "key cb", //user received this from a mouse
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
 	
	doorId: "doorDE", //for stageABCD, this is where they enter the passwords from mentors
	description: "You will definitely need some help for this - One of the previous prisoners still lingers around Doris’s House to this day. The previous prisoner has lost his mind from being trapped too long. Maybe if you help them remember who they are they will help you get out of this door.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //nickname of past prisoner
		key: "san diego",
    onStageCompleteDesc: " One of the past prisoners who has passed still roams the halls of Doris’s House. Rumor has it that they are actually a friendly ghost is just bored. Maybe if you play a game like 20 questions with them, they will help you get out." ,
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //talk to ghost
		key: "donald trump",
    onStageCompleteDesc: "Although the guard tries his best to keep order in Doris’s house, they have a tendency to let out celebrities that they “recognize”. Maybe if you sing them a song or act out a part of their favorite movie they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //impress a guard
		key: "kit415",
    onStageCompleteDesc: "Although the Janitor normally would not help out prisoners for fear of getting Doris mad, it is said that they will do anything for certain candy. Maybe if you give them the correct candies they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //bribe a janitor
		key: "hersheys",
    onStageCompleteDesc: "The door is FINALLY open"
	}]
 },
 {
	doorId: "doorEF",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this password from the time on the wall, based on the hint
		key: "3:40",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorFE",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //User picks this up by unscrambling the letters on the fridge, based on the hint
		key: "natural gas",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorFG",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList:  
	[{
		keyType: "key", //received from safeF 
		key: "key fg",
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorGH",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the back of a puzzle
		key: "wolfpups112", //CANNOT CHANGE, this is on puzzle already
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorHG",
	description: "This door has a space for a words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{						//This must match doorOP
		keyType: "password", //Password in the code
		key: "joeys101", //FINDME make sure there is a fairly technical person on this for Test2
    onStageCompleteDesc: "The door is open" 
	}]
 },
 {
	doorId: "doorGF",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the key in safeG after using remote
		key: "exploration and production",
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },{
	doorId: "doorED", 	//For EFGH, passwords from mentors
	description: "You will definitely need some help for this - One of the past prisoners who has passed still roams the halls of Doris’s House. Rumor has it that they are actually a friendly ghost is just bored. Maybe if you play a game like 20 questions with them, they will help you get out.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",  //talk to ghost
		key: "spongebob",
    onStageCompleteDesc: "Although the guard tries his best to keep order in Doris’s house, they have a tendency to let out celebrities that they “recognize”. Maybe if you sing them a song or act out a part of their favorite movie they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //impress a guard
		key: "fawn718",
    onStageCompleteDesc: "Although the Janitor normally would not help out prisoners for fear of getting Doris mad, it is said that they will do anything for certain candy. Maybe if you give them the correct candies they will help you get out.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //bribe a janitor 
		key: "milk duds",
    onStageCompleteDesc: "One of the previous prisoners still lingers around Doris’s House to this day. The previous prisoner has lost his mind from being trapped too long. Maybe if you help them remember who they are they will help you get out of this door.",
    onStageCompleteMessage: "That was great - however, only part of the door came down. You see a message on the door suggesting how to get the door to open more."
	},
	{
		keyType: "password", //nickname of past prisoner
		key: "jerry soda",
    onStageCompleteDesc: "The door is FINALLY open"

	}]
 },
 {
  doorId: "doorHI",
  description: "This door doesn't even look like it opens any more...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "RANDOM", //found in riddle in note
    key: "",
    onStageCompleteDesc: "user will never enter this door"
  }]
 }
 ]);

db.items.count();
db.userrooms.count();
db.doors.count();
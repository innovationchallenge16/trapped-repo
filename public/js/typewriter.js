// typewriter
// 3215287
// bertaec32@gmail.com
var showText

(function($, w, d, undefined) {

  function typewriter() {

    // Globals 
    var self = this, speed;
	

    function init(element, options) {
            // Set Globals
      var str;
      var indice = 0;

      self.options = $.extend( {}, $.fn.typewriter.options, options );
      $currentElement = $(element);
      elementStr = $currentElement.text().replace(/\s+/g, ' ');
      dataSpeed  = $currentElement.data("speed") || self.options.speed;
      console.log(dataSpeed)
      $currentElement.empty();
      showText = setInterval(
				function(){
					if (indice++ < elementStr.length) {
			      $currentElement.append(elementStr[indice]);
			    }else{
			    	clearInterval(showText);
			    }
				}, dataSpeed);
      // self.animation = setInterval(function(){animate_calification()}, 20);
	  return showText;
    }

    
    
    // Metodos publicos
    return {
      init: init
    }
  }

  // Plugin jQuery
  $.fn.typewriter = function(options) {
    return this.each(function () {
    	var writer =  new typewriter();
      writer.init(this, options);
      $.data( this, 'typewriter', writer);
    });
  };

  $.fn.typewriter.options = {
    'speed' : 300
  };

})(jQuery, window, document);

var startTyping2 = function(targetName, html) {
  target = document.getElementById(targetName);
  target.innerHTML = html.charAt(0) + html // Fixes issues of eating the first letter
  $(target).typewriter({'speed':25});
}

var startTyping = function(targetName) {
  target = document.getElementById(targetName);
  startTyping2(targetName,target.innerHTML);
 
};

var throwError = function() {
  throw new Error("Typing interupted!");
};

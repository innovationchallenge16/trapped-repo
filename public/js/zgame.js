var images = document.getElementById("images");
var description = document.getElementById("description");
var text = document.getElementById("dorisText");
var line = document.getElementById("blinky");
var Cname = document.getElementById("CompName");
var studentInput = document.getElementById('studentInput');
var studentName;
var typing = false;

//START GAME HERE!

//Somehow establish who each client is; Set up the homeroom picture depending on which client
//Adance to Welcome senario
// var startGame = function(){
// 	studentName = document.getElementById('studentName').value;
// 	scenario.welcome.text = "HHello " + studentName + ". My name is Doris. I have trapped you and all the other visitors in your rooms. Please do not try to escape. Please do not type HELP for commands.";
//  switch(studentName) {
//     case "A":
//      scenario.welcome.image = "images/room.jpg";
//      break;
// 	case "B":
//      scenario.welcome.image = "images/room2.jpg";
//      break;
//  case "C": cZ /caZ
//      scenario.welcome.image = "images/myRoom.png";
//      break;
// 	case "D":
// 		scenario.welcome.image = "images/myRoomWithCMD.png";
// 		break;
// 	case "E":
// 		scenario.welcome.image = "images/myRoomWithBed.png";
// 		break;
// 	case "F":
// 		scenario.welcome.image = "images/myRoomWithBed2.png";
// 		break;
// 	case "G":
// 		scenario.welcome.image = "images/myRoomWithBedColor.png";
// 		break;
// 	case "H":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "I":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "J":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "K":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "L":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "M":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "N":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "O":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	case "P":
// 		scenario.welcome.image = "images/room2.jpg";
// 		break;
// 	}
// 	advanceTo(scenario.welcome);
// }

//This overlaps with SocketIO functionality
//This is what will drive the game! When Someone presses enter.
// studentInput.onkeypress = function(event) {
//   if (event.key == "Enter" || event.keyCode == 13) {
// 	//If Doris is currently typing, Cancel that and move on;
// 	if (typing) {
// 		typing=false;
// 		clearInterval(showText);
// 	}

// 	nextScenario(studentInput.value); // Call function to deal with input
// 	studentInput.value = "" // Clear input
//   }
  
// };

//This function is going to be doing a lot of the heavy lifting. Will take in raw input and call necessary functions to deal with it.
// var nextScenario = function(studentInput){
// 	firstWord = studentInput.split(" ")[0]; //Take just the first word. Should be enough to get by.
// 	switch(firstWord.toLowerCase()) {
//     case "help": //This case I can deal with.
//         advanceTo(scenario.help);
//         break;
// 	case "why?": case "why": //This case I can deal with.
//         advanceTo(scenario.why);
//         break;
//     case "let": //This case I can deal with.
// 		advanceTo(scenario.two);
// 			break;
// 	case "back": //This case I can deal with. Might need to think of a new home output
//         advanceTo(scenario.two);
//         break;
// 	case "inspect": //How do I deal with this? Maybe check that item is in room. Hard code responses? Have one default
// 		advanceTo(scenario.car);
// 		break;
// 	case "open": //Try to open a door. Ask if door is locked. Deal with the result in another function. Maybe Prompt for key if there is one?
// 	//Maybe all doors have a key. Completing a puzzle will grant you the key? Idk See how they deal with it and code around
// 		advanceTo(scenario.open);
// 		break;
// 	case "use": //Try to use an object. Ask server and tell the user what happend?
// 		advanceTo(scenario.key);
// 		break;
//     default:
//         advanceTo(scenario.tryAgain);
// 	}
	
// }

// var changeName = function(nm) {
//   Cname.innerHTML = nm;
// }; 

// var changeText = function(words) {
//   text.innerHTML = words;
// };

var changeImage = function(img) {
  if (img == " ") return;
  images.src= img;
};

//This is where I will have to do some fancy things with their inventory. Will probaby update using the Person.inventory
var changeItem = function(itm) {
  if (itm.url == "") return; //If there is no new item then just return
  var item1 = document.getElementById("item1");
  var largeItem1 = document.getElementById("largeItem1");
  var item2 = document.getElementById("item2");
  var largeItem2 = document.getElementById("largeItem2");
  var item3 = document.getElementById("item3");
  var largeItem3 = document.getElementById("largeItem3");
  var item4 = document.getElementById("item4");
  var largeItem4 = document.getElementById("largeItem4");
  item1.src= itm.url;
  largeItem1.src = itm.url;
  description.innerHTML = itm.des;
};

var addBar = function(){
	line.innerHTML+= "|";
	
}

//Advances the game by changing text and images as needed
var advanceTo = function(s) {
  changeImage(s.image);
  changeItem(s.item);
  changeText(s.text);
  changeName(s.name);
  startTyping('dorisText');
  typing = true;
};

//Will need to rework the inventory once I get more information how how this is going to work.
// scenario = {}
// var scenario = {
//   welcome: {
//     image: " ", 
//     text: " ",
// 	name: "?",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
// 	addItem: false,
// 	useItem: false,
//   },
//   two: {
//     image: " ",
//     text: "TThere is no way out! Unless...",
// 	name: "Doris",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
//   },
//   help: {
//     image: "images/commands.png",
//     text: "TThere are other hidden commands too. Get creative! ",
// 	name: "Doris",
// 		item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
//   },
//   tryAgain: {
//     image: " ",
//     text: "II am but a lowly computer and do not undertand what you are trying to say. Type HELP for available commands.",
// 	name: "Confused Doris",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
//   },
//   why: {
//     image: " ",
//     text: "BBECAUSE I CAN",
// 	name: "Doris",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here",
// 	},
//   },
//   open: {
//     image: " ",
//     text: "PPlease type in the password for this door.",
// 	name: "Doris",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
//   },
//   car: {
//     image: " ",
//     text: "YYou found a hidden key! Darn I knew I should have hidden it in a better spot...",
// 	name: "Doris",
// 	item: {
// 		url: "images/key.png",
// 		des: "A key. I wonder what it unlocks...",
// 	},
//   },
//    key: {
//     image: " ",
//     text: "YYou opened the door! Darn...",
// 	name: "Doris",
// 	item: {
// 		url: "",
// 		des: "Nothing Seems to be here.",
// 	},
//   }
  
// };



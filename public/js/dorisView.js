var scroll = true;
var textList = ["Reminder: Doris Knows all and sees all","Please do not try to Escape","YOU WILL NEVER GET OUT", "So Bored..."]
var textIndex = 0;

//TEMP
//var stageFiveIntro = "Hey! How did you guys get out of your rooms?! .... Oh well. I will make you a deal. If you can complete my last 4 tasks I will let you out of my house. "
var stageFiveIntro = "Darn, you beat me... Thank-you for keeping me entertained. I can't wait until the next group arrives."

var stageFiveT1 = "Task One: You must line up in the order of your birth months, From January to December. BUT! You may not speak to one another."
var stageFiveT2a = "Task Two: You must nominate four to complete this task. Let me know when you have decided."
var stageFiveT2b = "The four nominated ones must complete different Minute to Win it challenges. My minions will direct you further."
var stageFiveAngry = "HOW DARE YOU BEAT MY CHALLENGES?! My Minions must be helping you! Fine. From now on my minions must complete the challenges as well! "
var stageFiveT3 = "Task Three: You must present my faithful admins with letters that spell one word."
var stageFiveT4 = "Task Four: I wish to be entertained! Five of you must volunteer to show everyone one talent that you have. Each person will be given one minute to showcase their talent."
var stageFiveT5 = "Task Four Part Two: The five of you who participated in the talent show must now line up everyone else in order of size of their spirit animal from smallest to largest. Those who did not participate may not make any sounds except those that their spirit animal make."
var stageFiveComplete = "Meh. Good enough."
var StagesCompleted = "Darn, you beat me... Thank-you for keeping me entertained. I can't wait until next year's group arrives."
var responseList = [stageFiveIntro,stageFiveT1,stageFiveT2a,stageFiveT2b,stageFiveAngry,stageFiveT3,stageFiveT4,stageFiveT5,stageFiveComplete,StagesCompleted]
var current = 0;

//This is called every 20 seconds for funnsies; Also Changes text after every scroll
function scrollText(){
	textTarget = document.getElementById('scrolling');
	textTarget.innerHTML = textList[textIndex];
	$(textTarget)
	.transition({ x:1800}, 6000, 'linear')
	.transition({ y: 200 },10)
	.transition({ x: -700 },10)
	.transition({ y: 0 },10);
	if(textIndex == (textList.length - 1 ) ) { textIndex = 0;}
	else {textIndex++;} 
}

textScroll = setInterval(function(){ scrollText(); }, 20000);

function prevLetter(letter) {
    return String.fromCharCode(letter.charCodeAt(0) - 1);
}


//Must always put in alphabetical order! Makes the second one disappear and the first expand to take up the space.
//Might come back to be able to input just letters in any order... but might not have to.
function  AdvanceStageTwo (targetName, targetNameTwo) {
	target = document.getElementById(targetName);
	target2 = document.getElementById(targetNameTwo);
	
	if (targetName == "roomA" || targetName == "roomC"){
		$(target).transition({width: '280px', x:145}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomE" || targetName == "roomG"){
		$(target).transition({height: '190px'}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomI" || targetName == "roomK"){
		$(target).transition({width: '280px'}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomM"|| targetName == "roomO"){
		$(target).transition({height: '190px', y:-100}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
        target2.innerHTML = " ";
	}
	document.getElementById('audiotag4').play();
}

function prevLetter(letter) {
    return String.fromCharCode(letter.charCodeAt(0) - 1);
}

//Refer to the big rooms by the first Letter. Might come back and fix this to refer to them as AB but we will see.
function  AdvanceStageThree (targetName, targetNameTwo) {
	target = document.getElementById(targetName);
	target2 = document.getElementById(targetNameTwo);
	
	if (targetName == "roomA"){
		$(target).transition({width: '570px', x:435}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomE"){
		$(target).transition({height: '390px'}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomI"){
		$(target).transition({width: '570px'}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomM"|| targetName == "roomO"){
		$(target).transition({height: '390px', y:-300}, 2000);
		$(target2).transition({width: 0}).transition({height: 0}).fadeOut();
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	document.getElementById('audiotag4').play();
}

function  AdvanceStageFour (targetName, targetNameTwo) {
	target = document.getElementById(targetName);
	target2 = document.getElementById(targetNameTwo);
	
	if (targetName == "roomA"){
		$(target).transition({width: '680px', x:545}, 2000);
		$(target2).transition({border: 0});
		$(target2).css("background", "url(images/LShape.png) no-repeat");
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	if (targetName == "roomI"){
		$(target).transition({width: '680px'}, 2000);
		$(target2).transition({border: 0});
		$(target2).css("background", "url(images/LShape2.png) no-repeat");
		target.innerHTML = " ";
		target2.innerHTML = " ";
	}
	document.getElementById('audiotag4').play();
}

function  AdvanceStageFive () {
	target = document.getElementById("roomE");
	target2 = document.getElementById("roomM");
	target3 = document.getElementById("roomA");
	target4 = document.getElementById("roomI");
	
	$(target).transition({height: '490px'}, 2000);
	$(target3).transition({border: 0});
	$(target).css("background", "url(images/wholeShape.png) no-repeat");
	$(target3).css("background", "url(images/wholeShape3.png) no-repeat");
	$(target4).transition({border: 0});
	$(target2).css("background", "url(images/wholeShape2.png) no-repeat");
	$(target2).transition({height: '495px', y:-402}, 2000);
	$(target4).css("background", "url(images/wholeShape4.png) no-repeat");
	target.innerHTML = " ";
	target2.innerHTML = " ";
	target3.innerHTML = " ";
	target4.innerHTML = " ";
	document.getElementById('audiotag4').play();
}

wait = function(ms) {
    var defer = $.Deferred();
    setTimeout(function() { defer.resolve(); }, ms);
    return defer;
};
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

//This can be used to delay between two rooms. Will need more functions for other numbers
function DelayPuzzleComplete(target,target2){
	trg2 = target2;
	puzzleComplete(target);
	timeoutID = window.setTimeout('puzzleComplete(trg2)', 1000); 
}
//This can be used to delay between text args sent in a list; DOES NOT WORK
function DelayPuzzleCompleteMulti(target){
	trg = target;
	puzzleComplete(trg[0]);
	var delayTime = 1000;
	/*for (i = 1; i < trg.length; i++) {
		nextText = trg[i];
		window.setTimeout('puzzleComplete(nextText)', delayTime);
		delayTime += 1000;
	}*/
	 window.setTimeout('for(var i = 1; i < trg.length; i++){ puzzleComplete(trg[i]); delayTime+= 1000 }', delayTime);
}

function puzzleComplete(targetName){
	target = document.getElementById(targetName);
	target.innerHTML = "Puzzle Complete";
	target.innerHTML = "<span id='text" +targetName+ "'>Puzzle Complete</span>";
	document.getElementById('audiotag1').play();
}

var stageFive = function() {
clearInterval(textScroll);
clearInterval(showText);
var dorisViewContent = document.getElementById("dorisViewContent");
dorisViewContent.innerHTML = '<div class="monitor" ><div id="dorisText" class="typewriter">' + responseList[0] +'</div><div id= "blinky" class="blinky">|</div></div>'
startTyping('dorisText');
}

//Could add in some other key functions. Like if we push U Doris says
//Right now is this slightly broken. Maybe call stageFive if different key?
$(document).keypress(function (e) {
    if (e.which == 13) { //Press enter. Causes console error if pressed before 5
		current = current + 1;
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
    }
	else if (e.which == 49){ //Press 1
		current = 0;
		stageFive();
		document.getElementById('audiotag3').play(); //Angry Sound
	}
	else if (e.which == 50){ //Press 2
		current = 1;// Task One
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 51){ //Press 3
		current = 2;// Task Two Nominate
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 52){ //Press 4
		current = 3;// Task Two actual
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 53){ //Press 5
		current = 4;// ANGRY!!
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
		document.getElementById('audiotag3').play(); //Angry Sound
	}
	else if (e.which == 54){ //Press 6
		current = 5;// Task Three
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 55){ //Press 7
		current = 6;// Task Four part one
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 56){ //Press 8
		current = 7;// Task Four part two
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 57){ //Press 9
		current = 8;// Meh Good Enough
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 122){ //Press z
		current = 9;// FINISHED!
        console.log('Moving to next stage: ' + current);
		clearInterval(showText);
		var text = document.getElementById("dorisText");
		text.innerHTML = responseList[current];
		startTyping('dorisText');
	}
	else if (e.which == 97){ //Press a
		document.getElementById('audiotag1').play(); //Bell sound
	}
	else if (e.which == 100){ //Press d
		document.getElementById('audiotag3').play(); //Angry Sound
	}
	else if (e.which == 115){ //Press s
		document.getElementById('audiotag2').play(); //Buzzer Sound
	}
});

    var text = document.getElementById("dorisText");
	var line = document.getElementById("blinky");
	var Cname = document.getElementById("CompName");
	var studentInput = document.getElementById('studentInput');
	var typing =  true; //false;
	var RPSPlayer = 0;
	var RPSDoris = 0;
	var won = false;
	var lost = false;
	var currList =
	[{
		doorId: "doorAP",
		locked: true,
	},
	{
		doorId: "doorAB",
		locked: true,
	},
	{
		doorId: "doorBA",
		locked: true,
	},
	{
		doorId: "doorBC",
		locked: true,
	},
	{
		doorId: "doorCD",
		locked: true,
	},
	{
		doorId: "doorDC",
		locked: true,
	},
	{
		doorId: "doorCB",
		locked: true,
	},
	{
		doorId: "doorDE",
		locked: true,
	},
	{
		doorId: "doorEF",
		locked: true,
	},
	{
		doorId: "doorFE",
		locked: true,
	},
	{
		doorId: "doorFG",
		locked: true,
	},
	{
		doorId: "doorGH",
		locked: true,
	},
	{
		doorId: "doorHG",
		locked: true,
	},
	{
		doorId: "doorGF",
		locked: true,
	},
	{
		doorId: "doorED",
		locked: true,
	},
	{
		doorId: "doorHI",
		locked: true,
	},
	{
		doorId: "doorIJ", 
		locked: true,
	},
	{
		doorId: "doorJI", 
		locked: true,
	},
	{
		doorId: "doorJK", 
		locked: true,
	},
	{
		doorId: "doorKL", 
		locked: true,
	},
	{
		doorId: "doorLK", 
		locked: true,
	},
	{
		doorId: "doorKJ", 
		locked: true,
	},
	{
		doorId: "doorLM", 
		locked: true,
	},
	{
		doorId: "doorMN", 
		locked: true,
	},
	{
		doorId: "doorNM", 
		locked: true,
	},
	{
		doorId: "doorNO", 
		locked: true,
	},
	{
		doorId: "doorOP", 
		locked: true,
	},
	{
		doorId: "doorPO", 
		locked: true,
	},
	{
		doorId: "doorON", 
		locked: true,
	},
	{
		doorId: "doorML", 
		locked: true,
	},
	{
		doorId: "doorPA", 
		locked: true,
	},
	{
		doorId: "doorIH", 
		locked: true,
	}]
	
	//Socket IO Code:
	var socket = io();
  	var roomId = document.getElementById('usr').value;	
  //Client Submits Actions
  $('form').submit(function(){

	  	//TODO figure out another way to implement RoomId
	    //RoomId for this room 

		//If Doris is currently typing, Cancel that and move on;
		if (typing) {
			typing=false;
			clearInterval(showText);
		}

	  	//Receive Input
	  	var input = $('#studentInput').val().toLowerCase();
		
  		//Only handle input if not empty
  		if(input!=''){
		
			//handle Rock Paper Scissors
			if (input.indexOf('challenge') > -1){
			    $('#dorisText').innerHTML = '';
				playRockPaperScissors();
				changeText("You're on!");
			}
			else if(input.indexOf('builddorisview') > -1){ //Set ID to DorisView if you call build DorisView.
				roomId = 'dorisview'
				socket.emit('action', input, roomId);
				$('#dorisText').innerHTML = '';
			}
			else{//Send action to server
				socket.emit('action', input, roomId);
				$('#dorisText').innerHTML = '';
			}
			//Clear Input
			$('#studentInput').val('');
		}
	    return false;
	});

	//Server Responses

	socket.on('startGame', function(newRoomId, delivery){
		if(delivery==roomId) {
			roomId = newRoomId;
			console.log("RoomID changed to: " + roomId);
			if(roomId!=null) {
				socket.emit('action', 'start 2', newRoomId);
			} else {
				document.getElementById("passwordPrompt").innerHTML = "<font color='red'>Password not found</font><br><br>Please try again:"
				timeoutID = window.setTimeout("$('#onLoadModal').modal('show');", 500);
			}

		}
	});
	
	socket.on('doris', function(msg, dorisReaction, delivery){
		// console.log("MSG: "+msg);

		if (delivery==roomId) {
			//Change Doris Text Output
	  		changeText(msg);

	  		//Handle Doris Name changes
	  		changeName(dorisReaction);
  		}
	});

	socket.on('room change', function(imageList, delivery){
		var imageContainer = document.getElementById("imageContainer");

		if (imageList.length != 0 && delivery==roomId) {
			//Clear Image container
			imageContainer.innerHTML = '';

			//add base image to imageContainer
			imageContainer.innerHTML += '<img id="baseimage" class="img-responsive" src="' + imageList[0] + '" alt="baseimage">'

			//Add layers of top images
			for (i = 1; i < imageList.length; i++) { 
		       var imageContainer = document.getElementById("imageContainer");
				  imageContainer.innerHTML += '<img id="topImage" class="img-responsive" src="' + imageList[i] + '" alt="">'
			   // Might need to give each a different id and give a different z index. Seems to work without it. We will see if it comes to that.
			}
		}
		return false;
	});


	socket.on('inventory change', function(inventory, delivery){
		var INV = null;
		if (delivery==roomId) {
			//Clears the visual inventory list
			var modalList = document.getElementById("itemModels");
			modalList.innerHTML = '';
			var itemWell = document.getElementById("itemWell");
			itemWell.innerHTML = '';

			INV = inventory;
			//Dynamically Build HTML Elements
			var item = null;
			console.log(inventory)
			for(var i = 0; i<inventory.length; i++) {
			 	item = inventory[i];
				addItem(item.itemName, item.description, item.imagePath, itemWell, itemModels);
			 }
		}
	});

	socket.on('give message', function(fromUserName, newItem, delivery, option){
		if (delivery==roomId) {
			itemGivenPop(fromUserName,newItem.itemName,newItem.imagePath);
		}
	});

	socket.on('dorisview start', function(delivery){
		if(delivery==roomId){ 
			buildDorisView();
		}	
	});

	var buildDorisView = function(){
		//Erases almost all html and replaces with an iframe with dorisViewIndex. Seems to work!
		document.getElementById('studentContent').innerHTML = '<iframe  frameborder= "0" id="iframe1" name="iframe1" src="dorisViewIndex.html"><p>Your browser does not support iframes.</p></iframe>';
		timeoutID = window.setTimeout("socket.emit('action', 'update', 'dorisview');", 1000); //Need to timeout before trying to update doors. Otherwise the frame has not finished building yet		
	}


	
	socket.on('dorisview doors', function(doorsList, delivery){// For locked status: True = locked, false = open
		if(delivery==roomId){
			var frames = window.frames; //How we get functions in dorisView.js
			for (rId = 0; rId < doorsList.length; rId++) { //Go through every door
				//console.log("Received: " + doorsList[rId].doorId + " = " + doorsList[rId].locked); //Log to Console
				//console.log("Current: " + currList[rId].doorId + " = " + currList[rId].locked);
				if ( (currList[rId].locked != doorsList[rId].locked) && (doorsList[rId].locked == false) ){//compare to a "current list" to find changes in door status. Sanity check to see if unlocked also.
					console.log("Change found in door: " + currList[rId].doorId);
					currList[rId].locked = doorsList[rId].locked; //Update current list					
					switch(currList[rId].doorId) {//For every change figure out which function to call      
						case "doorAB": case "doorCD": case "doorEF": case "doorGH": case "doorIJ": case "doorKL": case "doorMN": case "doorOP"://Stage One +1
							var room1 = "room" + currList[rId].doorId.charAt(4);
							var room2 = "room" + currList[rId].doorId.charAt(5);
							if (currList[rId].locked == currList[rId+1].locked){ //Both unlocked to call animation change
								console.log(currList[rId].doorId + " " + currList[rId+1].doorId + " Calling Animation for Change");
								frames[0].AdvanceStageTwo(room1,room2);
							}
							else { //Only one side unlocked so call puzzle complete
								console.log("Calling Puzzle print on: " + room1);
								frames[0].puzzleComplete(room1);
							}
							break;
						case "doorBA": case "doorDC": case "doorFE": case "doorHG": case "doorJI": case "doorLK": case "doorNM": case "doorPO": //Stage One -1
							var room2 = "room" + currList[rId].doorId.charAt(4);
							var room1 = "room" + currList[rId].doorId.charAt(5);
							if (currList[rId].locked == currList[rId-1].locked){
								console.log("Calling Animation for Change!!!");
								frames[0].AdvanceStageTwo(room1,room2);
							}
							else {
								frames[0].puzzleComplete(room2);
								console.log("Calling Puzzle print!!!");
							}
							break;
						case "doorBC": case "doorFG": case "doorJK": case "doorNO": //Stage Two +3
							var room1 = currList[rId].doorId.charAt(4);
							room1 = frames[0].prevLetter(room1.toLowerCase());
							room1 = "room" + room1.toUpperCase();
							var room2 = "room" + currList[rId].doorId.charAt(5);
							if (currList[rId].locked == currList[rId+3].locked){
								console.log(currList[rId].doorId + " " + currList[rId+1].doorId + " Calling Animation for Change");
								frames[0].AdvanceStageThree(room1,room2);
							}
							else {
								console.log("Calling Puzzle print on: " + room1);
								frames[0].puzzleComplete(room1);
							}
							break;
						case "doorCB": case "doorGF": case "doorKJ": case "doorON": //Stage Two -3
							var room2 = "room" + currList[rId].doorId.charAt(4);
							var room1 = currList[rId].doorId.charAt(5);
							room1 = frames[0].prevLetter(room1.toLowerCase());
							room1 = "room" + room1.toUpperCase();
							if (currList[rId].locked == currList[rId-3].locked){
								console.log("Calling Animation for Change on " + room1 + " and " + room2);
								frames[0].AdvanceStageThree(room1,room2);
							}
							else {
								frames[0].puzzleComplete(room2);
								console.log("Calling Puzzle print!!!");
							}
							break;
						case "doorDE": case "doorLM": //Stage Three + 7
							var room1 = currList[rId].doorId.charAt(4);
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back one
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back two
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back three
							room1 = "room" + room1.toUpperCase();
							var room2 = "room" + currList[rId].doorId.charAt(5);
							if (currList[rId].locked == currList[rId+3].locked){
								console.log(currList[rId].doorId + " " + currList[rId+7].doorId + " Calling Animation for Change");
								frames[0].AdvanceStageFour(room1,room2);
							}
							else {
								console.log("Calling Puzzle print on: " + room1);
								frames[0].puzzleComplete(room1);
							}
							break;
						case "doorED": case "doorML": //Stage Three - 7
							var room2 = "room" + currList[rId].doorId.charAt(4);
							var room1 = currList[rId].doorId.charAt(5);
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back one
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back two
							room1 = frames[0].prevLetter(room1.toLowerCase()); // goes back three
							room1 = "room" + room1.toUpperCase();
							if (currList[rId].locked == currList[rId-3].locked){
								console.log("Calling Animation for Change on " + room1 + " and " + room2);
								frames[0].AdvanceStageFour(room1,room2);
							}
							else {
								frames[0].puzzleComplete(room2);
								console.log("Calling Puzzle print!!!");
							}
							break;
						case "doorAP": //Stage four
							var room1 = "room" + currList[rId].doorId.charAt(4);
							if (currList[rId].locked == currList[rId+3].locked){
								console.log("Calling Animation for Whole room Change")
								frames[0].AdvanceStageFive();
							}
							else {
								console.log("Calling Puzzle print on: " + room1);
								frames[0].puzzleComplete(room1);
							}
							break;
						case "doorPA": //Stage four
							var room1 = "roomI";
							if (currList[rId].locked == currList[rId-3].locked){
								console.log("Calling Animation for Whole room Change");
								frames[0].AdvanceStageFive();
							}
							else {
								frames[0].puzzleComplete(room1);
								console.log("Calling Puzzle print!!!");
							}
							break;
						default: //Something is wrong
							console.log("Houston we Have a problem");
				    }

				}				
			}
		}

	});
	
	//Maye add onload to send off server so that I get this back? Could work.

	var startGame = function() {
		roomId = document.getElementById('usr').value;
		console.log("Input: " + roomId);
		if(roomId == 'Doris') {roomId = 'dorisview';}
		socket.emit('action', 'start', roomId);
	};

	var changeText = function(words) {
		// text.innerHTML = words;
		startTyping2('dorisText',words);
		typing = true;
	};

	var changeName = function(nm) {
	  Cname.innerHTML = nm;
	}; 

	var addItem = function(name, description, img, well, modal) {
		if (name.indexOf('note') < 0) {description = name + ": " + description;}
		nameNoSpace = name.replace(" ","");
		var sample = document.getElementById("samplemodal");

		imgModified = img.substring(0,img.indexOf(".png"));
		imgModified = imgModified.substring(img.indexOf("images/")+7);

		well.innerHTML += '<img id="item" class= "items" src="images/' + imgModified + '.png" class="img-responsive" data-toggle="modal" data-target="#' + nameNoSpace + '">'; 
		var modalCode = sample.innerHTML.replace("ItemName", name);
		modalCode = modalCode.replace("Descrip", description);
		modalCode = modalCode.replace("imgURL", imgModified);
		modalCode = modalCode.replace("sampMod",nameNoSpace);
		modal.innerHTML += modalCode;
	};

	var itemGivenPop = function (name,itemName, img, option){
		var givenM = document.getElementById("givenModal");
		//var showMod = document.getElementById("givenMod");

		var modalCode = "<div id='givenModal'><div class='modal fade' id='givenMod' role='dialog'><div class='modal-dialog'> <!-- Modal content--> <div class='modal-content'><div class='modal-header'> <button type='button' class='close' data-dismiss='modal'>&times;</button> <h4 class='modal-title'>How nice of them!</h4></div><div class='modal-body'> <center><img id='largeItemPic' class= 'largeItems' class='img-responsive' src='images/imgURL.png'><p id= 'description'>givenName has given you itemName</p> </center></div><div class='modal-footer'> <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div> </div> </div></div></div>";

		imgModified = img.substring(0,img.indexOf(".png"));
		imgModified = imgModified.substring(img.indexOf("images/")+7);

		modalCode = modalCode.replace("givenName", name);
		modalCode = modalCode.replace("itemName", itemName);
		modalCode = modalCode.replace("imgURL", imgModified);
		givenM.innerHTML = modalCode;
		$('#givenMod').modal('show');
	};
	
	//Won or lost status is lost when refreshed. Need to send something to backend to remember this.
	var playRockPaperScissors = function() {
		$('#rockPaperS').modal('show');

		//Check to see if already won or lost
		if (won){ //Display password
			if (roomId == "B")
				result.innerHTML = "You beat Doris! She reluctantly tells you the password is puppies456.";	
			else if (roomId == "K")
				result.innerHTML = "You beat Doris! She reluctantly tells you the password is cubs789.";
			else if (roomId == "M")
				result.innerHTML = "You beat Doris! She reluctantly tells you the password is calf131.";
			else
				result.innerHTML = "You beat Doris! She does not give you a password though...";
		}
		if (lost){ //Must reset
			RPSPlayer = 0;
			RPSDoris = 0;
			var gameSpace = document.getElementById("RPSGameSpace");
			var RPSChoice = document.getElementById("RPSChoice");
			RPSChoice.style.display = 'block';
			gameSpace.style.display = 'none';
			RPSscore.innerHTML = " ";
		}
	}


	var nextRPS = function() {
		
	var gameSpace = document.getElementById("RPSGameSpace");
	var RPSChoice = document.getElementById("RPSChoice");

	//Set User Choice Picture
	var weapon = document.getElementById('studentPlay').value;
	var yourChoice = document.getElementById("yourChoice");
	yourChoice.src= "images/" + weapon + ".png"

	//Set Doris Choice
	var dorisChoice = document.getElementById("dorisChoice");

	dorisChoice.src= "images/" + weapon + ".png"
	var v = Math.floor(Math.random() * 3);
		if(v==0)
			 dorisChoice.src= "images/Rock.png"
		if(v==1)
			 dorisChoice.src= "images/Paper.png"
		if(v==2)
			 dorisChoice.src= "images/Scissors.png"

	//Decide Winner
	var result = document.getElementById("result");
	var RPSscore = document.getElementById("RPSscore");
	RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
	if (v==0 && weapon=="Rock")
			result.innerHTML = "Tie! Try again.";
	if (v==0 && weapon=="Paper"){
			result.innerHTML = "You Win! Keep Going.";
			RPSPlayer = RPSPlayer + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		}		
	if (v==0 && weapon=="Scissors"){
			result.innerHTML = "Doris Wins! Try again.";
			RPSDoris = RPSDoris + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		}
	if (v==1 && weapon=="Paper")
			result.innerHTML = "Tie! Try again.";
	if (v==1 && weapon=="Rock"){
			result.innerHTML = "Doris Wins! Try again.";
			RPSDoris = RPSDoris + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		}		
	if (v==1 && weapon=="Scissors"){
			result.innerHTML = "You Win! Keep Going.";
			RPSPlayer = RPSPlayer + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		}	
	if (v==2 && weapon=="Scissors")
			result.innerHTML = "Tie! Try again.";
	if (v==2 && weapon=="Rock"){
			result.innerHTML = "You Win! Keep Going.";
			RPSPlayer = RPSPlayer + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		}		
	if (v==2 && weapon=="Paper"){
			result.innerHTML = "Doris Wins! Try again.";
			RPSDoris = RPSDoris + 1;
			RPSscore.innerHTML = "Score: " + RPSPlayer + " to " + RPSDoris;
		} 
		
	gameSpace.style.display = 'inline-block';	

	//Check for Win
	if (RPSPlayer == 3){
		RPSChoice.style.display = 'none';
		won = true;
		lost = false;
		if (roomId == "B")
			result.innerHTML = "You beat Doris! She reluctantly tells you the password is puppies456.";	
		else if (roomId == "K")
			result.innerHTML = "You beat Doris! She reluctantly tells you the password is cubs789.";
		else if (roomId == "M")
			result.innerHTML = "You beat Doris! She reluctantly tells you the password is calf131.";
		else
			result.innerHTML = "You beat Doris! She does not give you a password though...";
	}	
	if (RPSDoris == 3){
		RPSChoice.style.display = 'none';
		result.innerHTML = "Doris Wins! Maybe next time...";
		lost = true;
	}	

	};
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var path = require('path');
var Schema = mongoose.Schema;

/* Mongo Schemas and Collections */

//UserRoom
//potentially add ceilingDoor?
var userroomSchema = new Schema({ //combined to reduce calls to db
  roomId: String, //used to identify current user and room
  roomDescription: String, //returned on look room 
  roomLeftDoor: String, //left door for leaving the room
  roomRightDoor: String, //right door for leaving the room
  roomImagePath: String, //the image that this room is pointing to
  roomImageList: [{ imageId: String, //This should be the [roomId, <left>doorId, <right>doorId, itemName, itemName....]
                    //imagePath: String, //Path to the image to be used
                    imageState: Number }], //False by default, true if changed
                    //imageChangeCmd: String }], //What command must be used to change this command
                    // DELETEME Array of images: 1. Safe closed, light on 2. Safe closed light off 3. Safe open light on 4. safe open light off
  roomLightLetter: String,
  itemsInRoom: Array, //list of items in the room so that user can't pick up item not in room
  linkedRooms: {type: Array, default: []}, //marks that the doors have come down between these rooms. 
  userName: String, //First name of user within this room
  userPassword: String, //Password for start function
  userInventory: Array //inventory shown on front end for this room
});

var userrooms = mongoose.model('userrooms', userroomSchema, 'userrooms');
module.exports = userrooms;

//Door
var doorSchema = new Schema({
    doorId: String, //doorId : door<currRoomId>_<LinkedRoomId>
    doorStageList: [{ keyType: String, //if key, key item; if password, password
                      key: String, //identifies either password or key
                      onStageCompleteDesc: String,  //Description Changes to this after stage complete
                      onStageCompleteMessage: String, //Message sent to console when this stage is completed
                      postStageCmd: String }] , 
    currentStage: Number,//if undefined, assume   0 
    description: String, //returned on look <left|right> door; hint on what opens the door
    locked: Boolean, //determines if door has been opened
    doorOpenImage: String, //doorImage
    onDoorUnlock: String
});
var doors = mongoose.model('doors', doorSchema, 'doors');
module.exports = doors;

//Item
var itemSchema = new Schema({
  itemId: String, //only used for admin to identify how items fit into puzzles
  itemName: String, //used by server to identify item and what user knows item as
  description: String, //returned when user looks at an item
  visible: Boolean, //determines if item currently hidden always TRUE
  canBeHeld: Boolean, // determines if a user can pick up an item 
  unActivated: Boolean, //allows one change to happen to item
  itemStageList: [{ keyType: String, //key, password OR look (eliminates activateCmd)
                    key: String, //if keyType=key, this is the key item. If keyType=password, this is the password
                    onStageCompleteDesc: String,
                    onStageCompleteMessage: String,
                    postStageCmd: String //command to execute if stage activated
                    }],
  currentStage: Number,
  pushNumber: Number,
  pullNumber: Number,
  roomLightLetter: String,
  imagePath: String, //image path for the front end to illustrate items here
  roomImagePath: String, //Base image Path
  changeImagePath: String, //Path for image once its been changed
  changeImageCmd: String // Command that signals item will need to be changed
});

var items = mongoose.model('items', itemSchema, 'items');
module.exports = items;

//TestLogs
var testLogSchema = new Schema({
    itemOrDoor: String, //item if itemLog, door if doorLog
    itemName: String,
    doorId: String,
    currentStage: Object,
    currentRoom: String,
    timeStamp: String
});

var testlogs = mongoose.model('testlogs', testLogSchema, 'testlogs');
module.exports = testlogs;

//Where to send commands
app.get('/', function(req, res){
 // res.sendFile(__dirname + '/studentIndex_socketIO.html');
  res.sendFile(__dirname + '/zstudentIndex2_withDB.html');
});

app.use(express.static(path.join(__dirname, 'public')));


/** Built in messages **/
var startMessage = "My name is Doris. I have trapped you and all the other visitors in your rooms. Please do not try to escape. Please do not type HELP for advice."
var helpMessage = "Get all the help you need - feel free to talk to anyone who can hear you shout. You've started with notes left behind by prisoners who were stuck before you. Click on them to read their clues about getting out.";
var backMessage = "Are you ready to try to work again finally?"
var defaultOpenedDoorMessage = "The door is open.";

var errorMessage = "I don't understand what you are trying to say.";
var cannotFindMessage = "I don't see that here";
var cannotFindItemMessage = "I don't see the thing you want to pick up here";
var cannotFindDoorMessage = "Which door are you talking about?";
var itemAlreadyHeldMessage = "You are already holding this item!";
var itemCannotBeHeldMessage = "You dummy - this item cannot be held.";
var unlockedDoorMessage = "You unlocked a door? How could you?!";
var doorAlreadyUnlocked = "The door is already unlocked.";
var failedUseMessage = "Nothing happened...";
var notHoldingMessage = "You cannot use an item that you aren't holding."
var roomsAreNotLinkedMessage = "You can't give items to someone if the door between you is locked.";
var userDoesNotExistMessage = "I don't know who you are talking about.";
var spiritList = '';



//On Connection Actions
io.on('connection', function(socket){

  //Receive actions from the console input
  socket.on('action', function(input, roomId) {


    //Callback functions
    var emitStartFunction = function(newRoomId, delivery){
      io.emit('startGame',newRoomId, delivery);
    }

    var emitDorisText = function(response,roomId){
       dorisReaction = 'Doris';
       io.emit('doris', response, dorisReaction, roomId);
    };

    var startDorisView = function(roomId){
       io.emit('dorisview start',roomId);
    };

    var dorisDoors = function(doorsList, roomId){
       io.emit('dorisview doors', doorsList, roomId);
    };


    var emitRoomChange = function(room, input_roomId){
      var responseArr = [room.roomImagePath];
      emitRoomHelper(responseArr, room.roomImageList, input_roomId);      
    };

    var emitRoomHelper = function(responseArr, roomImageList, input_roomId) {
      //Check if all of roomImageList has been accounted for
      if(responseArr.length == roomImageList.length){
        io.emit('room change', responseArr, input_roomId);
      } else {
        var index = responseArr.length;

        //If on door index
        if(index==1){
          doors.find({doorId:roomImageList[index].imageId},function(error,doors){
            if(doors[0].locked==false){
              responseArr.push('images/newRooms/closet/closetOpenLeft.png');
            } else {
              responseArr.push('images/newRooms/closet/closetBlank.png');
            }
            emitRoomHelper(responseArr, roomImageList, input_roomId);
          });
        }else if(index==2){
          doors.find({doorId:roomImageList[index].imageId},function(error,doors){
            if(doors[0].locked==false){
              responseArr.push('images/newRooms/closet/closetOpenRight.png');
            } else {
              responseArr.push('images/newRooms/closet/closetBlank.png');
            }
            emitRoomHelper(responseArr, roomImageList, input_roomId);
          });
        } 
        // otherwise item image
        else {

          items.find({itemName:roomImageList[index].imageId},function(error,items){
            var item = items[0];
            if(roomImageList[index].imageState==1){
              responseArr.push(item.changeImagePath);
            } else {
              responseArr.push(item.roomImagePath);
            }
            emitRoomHelper(responseArr, roomImageList, input_roomId);
          });
        }
      }
    };

    var emitInvChange = function (inventoryArray, input_roomId){
      var responseArr = [];
      emitInvHelper(responseArr, inventoryArray, input_roomId);  
    }

    var emitInvHelper = function(responseArr, inventoryArray, input_roomId){
      //this activates on conclusion
      if(responseArr.length==inventoryArray.length){
        io.emit('inventory change', responseArr, input_roomId);
      } else {
        //Add one item from array to response array
        var i = responseArr.length;
        items.find({itemName:inventoryArray[i]},function(error,items){
          var item = items[0];
          responseArr.push(item);
          emitInvHelper(responseArr,inventoryArray, input_roomId)
          
        });//end items query  
      }
    }

    var emitGivePopup = function(input_userName, input_itemName, targetRoomId){ 

      var userNm = input_userName;
      if(userNm.split(" ").length > 1) {
        userNm = userNm.substring(0, userNm.length-1)+userNm.substring(userNm.length-1).toUpperCase();
      }

      items.find({itemName:input_itemName},function(error,items){
        var item = items[0];
        io.emit('give message', userNm, item, targetRoomId);
      });
    }

    //Send text to be handled by other functions
    input = input.toLowerCase();
    handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup,dorisDoors,startDorisView,emitStartFunction);
  
  }); //end onAction
}); //end onConnection

  //Function for handling a command
  function handleCommandInput(input,roomId,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup, dorisDoors,startDorisView, emitStartFunction){
    var response = '';
    var dorisReaction = 'Doris';

    //Determine which action is being taken
    var verb = input.substring(0,input.indexOf(' '));
    if(verb==""){ //Check for one-word commands
      verb = input;
    }

    var response = '';
    var dorisReaction = 'Doris';

    var roomQuery = {roomId:roomId};
    if(input=="start") {
      roomQuery =  {userPassword: roomId};
    }

    //Find Current Room
    userrooms.find(roomQuery,function(error,rooms){
      input_room = rooms[0];
      if (roomId=="dorisview"||roomId=="doris"){input_room ="dorisview"}

      //Potential Error Handling
      if(rooms.length>1){ 
        console.log("ERROR: " + rooms.length + " rooms returned from currentRoom query."); 
        console.log("roomID: " + roomId);
        console.log("input: " + input);
      }
      if(input_room == undefined){
        console.log("ERROR: room is undefined"); 
        console.log("roomID: " + roomId);
        console.log("input: " + input);
      }

      switch(verb) {      
        case "start": 
          onStartAction(input,roomId,input_room,emitDorisText,emitRoomChange,emitInvChange,startDorisView,emitStartFunction);
          break;
        case "builddorisview": 
          onBuildDorisView(input, input_room,emitDorisText,startDorisView);
          break;
        case "look":
         onLookAction(input,input_room,emitDorisText,emitRoomChange,emitInvChange);
          break;
        case "pick": 
          onPickupAction(input,input_room,emitDorisText,emitRoomChange,emitInvChange);
          break;
        case "use":
          onUseAction(input, input_room,emitDorisText,emitRoomChange,emitInvChange,dorisDoors);
          break;  
        case "try":
          onTryAction(input, input_room,emitDorisText,emitRoomChange,emitInvChange,dorisDoors);
          break;
        case "give":
          onGiveAction(input,input_room,emitDorisText,emitInvChange,emitGivePopup);
          break;
        case "toggle":
          onToggleAction(input,input_room,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup);
          break;
        case "help":
          onHelpAction(input,input_room,emitDorisText);
          break;
        case "push":
          onPushAction(input,input_room,emitDorisText,emitRoomChange,emitInvChange,emitGivePopup);
          break;
        case "pull":
          onPullAction(input,input_room, emitDorisText, emitRoomChange, emitInvChange);
          break;
        case "server":
          callTestFunction(input, input_room, emitDorisText,emitRoomChange,emitInvChange, emitGivePopup, dorisDoors);
          break;
        case "update":
          updateDoris(input, input_room, emitDorisText,emitRoomChange,emitInvChange, emitGivePopup, dorisDoors);
          break;
        default: 
          response = errorMessage;
          dorisReaction = 'Confused Doris';
          emitDorisText(response,roomId);
      }
    });//end userroomquery
  }//end handleCommandInput()





function updateDoris(input, input_room, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup, dorisDoors) {

    doors.find({},function(error,doors){
      dorisDoors(doors,"dorisview");
    });
}

function onStartAction(input, input_ID, input_room, emitDorisText, emitRoomChange, emitInvChange,startDorisView, emitStartFunction){
  var response = '';

  //check for correct input
  if(input=='start'){
	  if(input_room=="dorisview"){ //This makes the dropdown Doris work
		startDorisView(input_room); //Builds DorisView
	  }
    //Handles if input_room pw does not find real room
    else if(input_room==undefined){
      emitStartFunction(null, input_ID);
    }

    else {
      emitStartFunction(input_room.roomId, input_room.userPassword);
	  }
  } 
  else if (input=="start 2"){
    var userNm = input_room.userName;
    if(userNm.split(" ").length > 1) {
      userNm = userNm.substring(0, userNm.length-1)+userNm.substring(userNm.length-1).toUpperCase();
    }

    response = "Hello " + userNm + ". " + startMessage;
    emitDorisText(response,input_room.roomId);
    emitRoomChange(input_room, input_room.roomId);
    emitInvChange(input_room.userInventory, input_room.roomId); 

  }
  else {
    response = errorMessage;
    emitDorisText(response,input_roomId);
  }
}//End OnStartAction

function onHelpAction(input,input_room,emitDorisText){
  emitDorisText(helpMessage, input_room.roomId);
}


//This handles if you enter 'builddorisview' into console; Does not work right now because if checks roomID first
function onBuildDorisView(input, input_room,emitDorisText,startDorisView){
  var response = '';
  
  //check for correct input
  if(input=='builddorisview'){
    startDorisView(input_room);
  } 
  else {
    response = errorMessage;
    emitDorisText(response,input_room);
  }
}//End OnBuildDorisViewAction



//TODO add look light
//TODO add look button
//Function used to return description of target
function onLookAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange){
  var response = "";

  //Pull thing from input
  var thing = input.substring(input.indexOf(' ')+1);
  var verb = input.substring(0,input.indexOf(' '));

  //Scenario One: Look room
  //TODO: Potentially, change to include descriptions of each item in roomItemList
  if(thing.indexOf('room')!=-1) {

    //Return Description of the room
    response = input_room.roomDescription;
    emitDorisText(response,input_room.roomId);
  } 

  //Scenario Two: Look door
  else if(thing == 'left door'||thing == 'right door') {
    var targetDoorId = '';
    //choose which door to use item on 
    if(thing=='left door') {
      targetDoorId = input_room.roomLeftDoor;
    } else {
      targetDoorId = input_room.roomRightDoor;
    }

     //query the doorId of room.roomLeftDoor
    doors.find({doorId:targetDoorId},function(error,doors){
      var door = doors[0]; 
      //return the description of the left door
      response = door.description;

      emitDorisText(response,input_room.roomId);
      //TODO: Potential return whether or not the opposite door is open

    });//end look door query
  }//end look door scenario

  //Scenario 2b: Improper Look Door command ERROR
  else if(thing.indexOf('door')!=-1&&thing!="center door") {  //center door handling for laundryRoomG
    response = cannotFindDoorMessage;
    emitDorisText(response,input_room.roomId);
  }
   
  //otherwise, Scenario Three: Look item
  else { 

    //add handling for if looking at light
    if(thing=="light"){
      thing = thing + " " + input_room.roomId.toLowerCase(); 
    }

    //Pull from Items Database
    items.find({itemName:thing},function(error,items){
    var item = items[0];

    //If item searched for does not exist in DB ERROR
    if(item==undefined){
      response = cannotFindMessage;
      emitDorisText(response,input_room.roomId);
    } 
        
    //Confirm that item searched for is not in room or user inventory ERROR
    else if (input_room.itemsInRoom.indexOf(item.itemName)==-1&&input_room.userInventory.indexOf(item.itemName)==-1){
    //Return response to client
      response = cannotFindMessage;
      emitDorisText(response,input_room.roomId);
    }

    //Otherwise, assume valid case
    else {
      //Send Item description to client
      response = item.description;       
      emitDorisText(response,input_room.roomId);

      /** START STAGE HANDLING **/
      //Check for stage handling: That item has stages, and that if so, item has not already been activated
      if(item.itemStageList!= undefined&&item.itemStageList.length>0&&item.unActivated==true){

        //Check for potential item activation
        var currentStage = item.currentStage;
        if(currentStage==undefined){
          currentStage = 0;
        }

        //To be activated: 
        //current stage activateCmd is look, item is not activated and then 
        if(item.itemStageList[currentStage].keyType=='look'){

          //Note that if look is the key, the onStageMessage is the first description
          activateItem(item, input_room,emitRoomChange,emitInvChange);
          response = item.itemStageList[currentStage].onStageCompleteMessage;
          if(response==undefined) {
            response = item.description;
          }
          emitDorisText(response,input_room.roomId);
        }               
      }
    /** END STAGE HANDLING **/
    } 
  }); //end of item.find()
  } //end of Scenario: look item
} //end onLookAction()

//Function picks up target item currently in room, and places it in inventory
function onPickupAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange) {
  var response = '';

  //Pull itemName from input
  var target = input.substring(input.indexOf('up ')+3);
  
  //If second input is not 'up' ERROR
  if (input.substring(5,8) != 'up '){
    response = errorMessage;
    emitDorisText(response,input_room.roomId);
  } 

  //If item is in inventory already ERROR
  if (input_room.userInventory.indexOf(target)!=-1)
  {
    response = itemAlreadyHeldMessage;
    emitDorisText(response,input_room.roomId);
  }

  //If item is not in room ERROR
  else if(input_room.itemsInRoom.indexOf(target)==-1) {
    response = cannotFindItemMessage;
    emitDorisText(response,input_room.roomId);
  }

  else {
    //Get Item from db
    items.find({itemName:target},function(error,items){
      var item = items[0];

      //if item cannot be held ERROR
      if(item.canBeHeld!=true){
        response = itemCannotBeHeldMessage;
        emitDorisText(response,input_room.roomId);          
      } 

      //Assume valid case
      else {
        //Add item to user Inventory
        var updatedUserInv = input_room.userInventory;
        updatedUserInv.push(item.itemName);

        //Remove item from itemsInRoom List
        var roomItemsList = input_room.itemsInRoom;
        roomItemsList.remove(item.itemName);

        //Update Arrays in db
        var whereQuery = {roomId: input_room.roomId};
        var change = {userInventory: updatedUserInv, itemsInRoom: roomItemsList};
       userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

          //Send new inventory to the front end
          emitInvChange(updatedUserInv, input_room.roomId);
          
          //Send Doris Text to front end
          response = "You picked up the " + item.itemName;
          emitDorisText(response, input_room.roomId);

          //Check changeImageCmd change on current command
          var itemImageIndex = -1;
          var itemImageState = 1;
          for(var i = 0; i < input_room.roomImageList.length; i++){
            if(input_room.roomImageList[i].imageId==target){
              itemImageIndex = i;
              itemImageState = input_room.roomImageList[i].imageState;
            }            
          }
          input_room.roomImageList.indexOf(item.itemName);

          // ONLY IF - changeItemCmd is pick, item is in roomImageList and imageState==0
          if(item.changeImageCmd=="pick"&&itemImageIndex!=-1&&itemImageState==0){
            advanceImageStates(target, itemImageIndex, input_room, emitRoomChange);
          }

        });//end update userrooms
      }
    });//end items query
  }
} //end onPickupFunction


//TODO REFACTOR
function onUseAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange,dorisDoors){ 
   //pull item/target from input
  var usedItem = input.substring(input.indexOf(" ")+1,input.indexOf(" on "));
  var target = input.substring(input.indexOf(" on ")+4);

  // bad input ERROR
  if(input!=("use " + usedItem + " on " + target)){
    response = failedUseMessage;
    emitDorisText(response, input_room.roomId);
  }

  //if user does not have item he/she is trying to use ERROR
  else if (input_room.userInventory.indexOf(usedItem)==-1){ 
    response = notHoldingMessage;
    emitDorisText(response,input_room.roomId);
  }

  //** If target is a door **//
  else if(target == 'left door'||target == 'right door') {
    var targetDoorId = '';
    //choose which door to use item on 
    if(target=='left door') {
      targetDoorId = input_room.roomLeftDoor;
    } else {
      targetDoorId = input_room.roomRightDoor;
    }

    //look up actual door
    doors.find({doorId:targetDoorId},function(error,doors){
      var door = doors[0];

      //if door is already unlocked ERROR
      if(door.locked!=true){
        response = doorAlreadyUnlocked;
        emitDorisText(response,input_room.roomId);
      }

      //Get Current Stage
      var currStage = door.currentStage;
      if(currStage==undefined){
        currStage = 0;
      }
      var doorStage = door.doorStageList[currStage];

      //Handling error from 6/8 testing
      if(doorStage==undefined){
        response = failedUseMessage;
        emitDorisText(response,input_room.roomId);
      }

      //if item isn't activated with a password ERROR
      if(doorStage.keyType!='key'||doorStage.key!=usedItem){
        response = failedUseMessage;
        emitDorisText(response,input_room.roomId);
      } 

      //user has item he/she is trying to use
      else {
        //correct key
        if(usedItem == doorStage.key&&doorStage.keyType=='key'){
          openDoor(door,input_room, emitRoomChange, emitInvChange,dorisDoors);

          response = doorStage.onStageCompleteMessage;
          if(response==undefined){ //Check if onStageCompleteMessage was ever defined
            response = "You opened up the " + target + ".";
          }
          emitDorisText(response,input_room.roomId);

          //remove usedItem from inventory
          var updatedUserInv = input_room.userInventory;
          updatedUserInv.remove(usedItem);

          //Apply changes to db
          var whereQuery1 = {roomId: input_room.roomId};
          var change1 = {userInventory: updatedUserInv};

          //send new Inventories to currentUserroom
          userrooms.findOneAndUpdate(whereQuery1, change1, function(err,rawResponse){
            emitInvChange(updatedUserInv, input_room.roomId);
          });

        }
        //incorrect key ERROR 
        else {
          response = failedUseMessage;
          emitDorisText(response,input_room.roomId);
        }
      }
    });//end door query
  }
  //if target is another door? send ERROR
  else if(target.indexOf(' door')!=-1&&target!="center door"){
    response = cannotFindDoorMessage;
    emitDorisText(response,input_room.roomId);
  }

  //** If target is an item **//
  else {
    items.find({itemName:target},function(error,items){
      var item = items[0];

      // if targetItem does not exist ERROR
      if(item==undefined){
        response = cannotFindItemMessage;
        emitDorisText(response,input_room.roomId);
      }

      //If target item does not have a dependency and is not in current room/inventory ERROR
      else if(item.itemStageList==undefined||(input_room.itemsInRoom.indexOf(item.itemName)==-1&&input_room.userInventory.indexOf(usedItem)==-1)) {
        response = cannotFindItemMessage;
        emitDorisText(response,input_room.roomId);
      }

      //if item is already activated ERROR
      else if (item.unActivated!=true){
        response = failedUseMessage;
        emitDorisText(response,input_room.roomId);
      }

      else {
        //Get Current Stage
        var currStage = item.currentStage;
        if(currStage==undefined){
          currStage = 0;
        }
        var itemStage = item.itemStageList[currStage];

                //if itemStage does not exist ERROR
        if(itemStage==undefined){
          response = failedUseMessage;
          emitDorisText(response,input_room.roomId)

        }
        
        //if item isn't activated with a password ERROR
        else if(itemStage.keyType!='key'||itemStage.key!=usedItem){
          response = failedUseMessage;
          emitDorisText(response,input_room.roomId);
        } 
        //otherwise, correct case
        else {
          activateItem(item,input_room,emitRoomChange,emitInvChange);

          response = item.itemStageList[currStage].onStageCompleteMessage;
          emitDorisText(response,input_room.roomId);

          //remove usedItem from inventory
          var updatedUserInv = input_room.userInventory;
          updatedUserInv.remove(usedItem);
          //hard coded scenario
          if(input=='use rod on handle'){
            updatedUserInv.remove('handle');
          }
          if(input=='use reel on pole'){
            updatedUserInv.remove('pole');
          }

          //Apply changes to db
          var whereQuery1 = {roomId: input_room.roomId};
          var change1 = {userInventory: updatedUserInv};

          //send new Inventories to currentUserroom
          userrooms.findOneAndUpdate(whereQuery1, change1, function(err,rawResponse){
            emitInvChange(updatedUserInv, input_room.roomId);
          });

          //Check changeImageCmd change on current command
          var itemImageIndex = -1;
          var itemImageState = 1;
          for(var i = 0; i < input_room.roomImageList.length; i++){
            if(input_room.roomImageList[i].imageId==target){
              itemImageIndex = i;
              itemImageState = input_room.roomImageList[i].imageState;
            }            
          }

          // ONLY IF - changeItemCmd is pick, item is in roomImageList and imageState==0
          if(item.changeImageCmd=="use"&&itemImageIndex!=-1&&itemImageState==0){
            advanceImageStates(target, itemImageIndex, input_room, emitRoomChange);
          }
        }
      }
    });//end item query
  }
} //end onUseAction()

var failedPasswordMessage = "That password didn't work.";
var usingPasswordOnWhatMessage = "What are you trying that password on?";
var cannotFindTarget = "I can't find what you're trying this on..."

//function uses password on target door/item 
function onTryAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange, dorisDoors){
  //pull item/target from input
  var password = input.substring(input.indexOf(" ")+1,input.indexOf(" on "));
  var target = input.substring(input.indexOf(" on ")+4);

  // bad input ERROR
  if(input!=("try " + password + " on " + target)){
    response = usingPasswordOnWhatMessage;
    emitDorisText(response, input_room.roomId);
  }

  //** If target is a door **//
  else if(target == 'left door'||target == 'right door') {

    //choose which door to use item on 
    var targetDoorId = '';
    if(target=='left door') {
      targetDoorId = input_room.roomLeftDoor;
    } else {
      targetDoorId = input_room.roomRightDoor;
    }
    //look up actual door
    doors.find({doorId:targetDoorId},function(error,doors){
      var door = doors[0];

      /*** STAGE HANDLING ***/
      var currStage = door.currentStage;
      if(currStage==undefined){
        currStage = 0;
      }
      var doorStage = door.doorStageList[currStage];

      //if door is already unlocked ERROR
      if(door.locked!=true){
        response = doorAlreadyUnlocked;
        emitDorisText(response,input_room.roomId);
      } 

      //if door isn't unlocked with a password ERROR
      else if(doorStage.keyType!='password'){
        response = failedPasswordMessage;
        emitDorisText(response,input_room.roomId)
      }
      
      else {
      //if door unlocks with password
        //if input-password does not equal door-password ERROR
        if(password != (doorStage.key) ){
          response = failedPasswordMessage;
          emitDorisText(response,input_room.roomId);
        }

        //otherwise, assume valid case and open door with password
        else {
          openDoor(door, input_room, emitRoomChange, emitInvChange,dorisDoors);

          response = doorStage.onStageCompleteMessage;
          if(response==undefined){ //Check if onStageCompleteMessage was ever defined
            response = "You opened up the " + target + ".";
          }
          emitDorisText(response,input_room.roomId);
        }
      }
    });//end door query
  }

  //if target is another door ERROR
  else if(target.indexOf(' door')!=-1&&target!="center door"){
    response = cannotFindTarget;
    emitDorisText(response,input_room.roomId);
  }

  //** If target is an item **//        
  else {
    items.find({itemName:target},function(error,items){
      var item = items[0];

      //Item does not exist ERROR
      if(item==undefined){
        response = cannotFindTarget;
        emitDorisText(response,input_room.roomId);
      }
      //If target item does not have a dependency and is not in current room/inventory ERROR
      else if(item.itemStageList==undefined||(input_room.itemsInRoom.indexOf(target)==-1&&input_room.userInventory.indexOf(target)==-1)) {
        response = cannotFindTarget;
        emitDorisText(response,input_room.roomId);
      }
      //if item is already activate ERROR
      else if(item.unActivated!=true){
        response = failedPasswordMessage;
        emitDorisText(response,input_room.roomId);
      } 

      else{
        /*** STAGE HANDLING ***/
        var currStage = item.currentStage;
        if(currStage==undefined){
          currStage = 0;
        }
        var itemStage = item.itemStageList[currStage];
        
        //if itemStage does not exist ERROR
        if(itemStage==undefined){
          response = failedPasswordMessage;
          emitDorisText(response,input_room.roomId)

        }

        //if item isn't activated with a password ERROR
        else if(itemStage.keyType!='password'){
          response = failedPasswordMessage;
          emitDorisText(response,input_room.roomId)
        }

        //if password doesn't match input_password ERROR
        else if(password != (itemStage.key) ){
          //Hardcoded scenario
          if(target=="keypad"&&password=="doris"){
            response = "Why thank you! But you didn't think it would be that easy to solve the puzzle, did you? ";
            emitDorisText(response,input_room.roomId);
          } else {
            response = failedPasswordMessage;
            emitDorisText(response,input_room.roomId);
          } 
        }
        //Valid input, password matches door password
        else  {
          activateItem(item,input_room,emitRoomChange,emitInvChange);
          response = item.itemStageList[currStage].onStageCompleteMessage;
          emitDorisText(response,input_room.roomId);
        
          //Check changeImageCmd change on current command
          var itemImageIndex = -1;
          var itemImageState = 1;
          for(var i = 0; i < input_room.roomImageList.length; i++){
            if(input_room.roomImageList[i].imageId==target){
              itemImageIndex = i;
              itemImageState = input_room.roomImageList[i].imageState;
            }            
          }

          // ONLY IF - changeItemCmd is try, item is in roomImageList and imageState==0
          if(item.changeImageCmd=="try"&&itemImageIndex!=-1&&itemImageState==0){
            advanceImageStates(target, itemImageIndex, input_room, emitRoomChange);
          }

        }
      } 
    });//end items query
  }
}//End onTryAction

function onGiveAction(input, input_room, emitDorisText, emitInvChange, emitGivePopup) {
  var response = '';

  //pull item/target from input
  var givenItem = input.substring(input.indexOf(" ")+1,input.indexOf(" to "));
  var targetName = input.substring(input.indexOf(" to ")+4).toLowerCase();
  var targetPrintName = targetName.substring(0,1).toUpperCase()+targetName.substring(1); //Capitalize UserName for query

  //If input is NOT correctly formatted ERROR
  if (input.indexOf(' to ') == -1){
    emitDorisText(errorMessage,input_room.roomId);
  } 

  //if item not in inventory or does not exist ERROR
  else if(input_room.userInventory.indexOf(givenItem)==-1){ 
    response = notHoldingMessage;
    emitDorisText(response,input_room.roomId);
  }

  else {
    //Pull target userroom from db
    userrooms.find({userName:targetPrintName},function(error,rooms2) {
      var userroom2 = rooms2[0];

      //if user does not exist ERROR
      if (userroom2 == undefined) {
        response = userDoesNotExistMessage;
        emitDorisText(response,input_room.roomId);
      }

      else {
        //if both rooms aren't linked: ERROR
        //Hard-coded exception: Money can be given to anyone, because it is so small and easy to pass
        if(((input_room.linkedRooms.indexOf(userroom2.roomId)==-1)||(userroom2.linkedRooms.indexOf(input_room.roomId)==-1))&&givenItem!="money"){
          response = roomsAreNotLinkedMessage;
          emitDorisText(response,input_room.roomId);
        }

        //otherwise, valid case
        else {

          //remove item from currentUserroom userInventory
          var updatedUserInv = input_room.userInventory;
          updatedUserInv.remove(givenItem);

          //add item to targetUserroom userInventory 
          var updatedTargetInv = userroom2.userInventory;
          updatedTargetInv.push(givenItem);

          //Apply changes to db
          var whereQuery1 = {roomId: input_room.roomId};
          var change1 = {userInventory: updatedUserInv};

          var whereQuery2 = {roomId: userroom2.roomId};
          var change2 = {userInventory: updatedTargetInv};

          //send new Inventories to currentUserroom
          userrooms.findOneAndUpdate(whereQuery1, change1, function(err,rawResponse){
            emitInvChange(updatedUserInv, input_room.roomId);
            
            //Send Doris Text to front end
            response = "You gave away the " + givenItem;
            emitDorisText(response, input_room.roomId);
          });

          //send new Inventories to targetUserroom
          userrooms.findOneAndUpdate(whereQuery2, change2, function(err,rawResponse){
             emitInvChange(updatedTargetInv, userroom2.roomId);
             emitGivePopup(input_room.userName, givenItem, userroom2.roomId);
          });       
        } 
      }
    });//end of second userroom query
  }
} //End onGiveAction

var noLightSwitchMessage = "There's no light switch in this room to toggle.";
var SPIRITMatchedMessage = "You hear the sound of a safe opening in a room away from yours.";
var responseSafeH = "Suddenly, the safe creaked open, and you found a hammer in it!";
//TODO Hardcode logic for changing lights in Stage 4 Task 1
function onToggleAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup) {
  var response = '';
  

  //Hard-coded values
  var roomsWithLightBulbs = ['A','B','C','D','E','F'];
  var resetLightsRoom = 'G';
  var changesGarageLightRoom = 'A';
  var garageRoomId= 'B';

 //Invalid input ERROR
  if (input!='toggle light'&&input!='toggle light switch') {
    response = errorMessage;
    emitDorisText(response,input_room.roomId);
  }
  //No Light Switch available to toggle ERROR
  else if (roomsWithLightBulbs.indexOf(input_room.roomId)==-1) {
    response = noLightSwitchMessage;
    emitDorisText(response, input_room.roomId);
  }

  //Otherwise valid input
  else { 
    var whereQuery = {itemId: "light"+input_room.roomId};
    
    items.find(whereQuery,function(error,itemsFind){
      var item = itemsFind[0];

      var changeLightStatus = false; 
      var lightStatus = "on"; //assume light is off, so we are turning it on
      if(item.unActivated==false){ //but if light is on, turn it off.
        changeLightStatus = true;
        lightStatus = "off"
      }         

      var change = {unActivated:changeLightStatus, description: "The light is "+ lightStatus + "."}; //Presuming here that no objects need to be removed from roomsList      
      //Change Light Status
      items.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
      
        response = "You turned " + lightStatus + " the light.";
        emitDorisText(response, input_room.roomId);
              
        //Check changeImageCmd change on current command
        var itemImageIndex = -1;
        var itemImageState = 1;
        for(var i = 0; i < input_room.roomImageList.length; i++){
          if(input_room.roomImageList[i].imageId==item.itemName){
            itemImageIndex = i;
            itemImageState = input_room.roomImageList[i].imageState;
          }            
        }
        // ONLY IF - item is in roomImageList
        if(itemImageIndex!=-1){
          advanceImageStates(item.itemName, itemImageIndex, input_room, emitRoomChange);
        }

        spiritList += item.roomLightLetter;
        console.log("CURR: "+spiritList);
        
        //Hard-coded Scenario, checking for opening the door on completion of the SPIRIT Lightbulb challenge
        if(spiritList=='SPIRIT'){
          var specificRoom = "H";
          var safeH = "personal safe";

          //pickup hammer here
          items.find({itemName:safeH},function(error,itemSafeHList){
            itemH = itemSafeHList[0];

            userrooms.find({roomId:"H"},function(error,roomsH){
              var roomH = roomsH[0];

              if (itemH.unActivated==true){

                activateItem(itemH, roomH, emitRoomChange, emitInvChange, emitGivePopup);

                var imgList = roomH.roomImageList;

                for(var j=0; j<imgList.length; j++){
                  if(imgList[j].imageId==safeH){
                    advanceImageStates(safeH, j, roomH, emitRoomChange);
                  }
                }
                // Emit Doris Message to roomH about item found
                response = responseSafeH;
                emitDorisText(response, roomH.roomId);
              }//end if query
            });
          });//end findSafeH query
         
          response = SPIRITMatchedMessage;
          emitDorisText(response, input_room.roomId);
        } 
      }); //end updateItem
    });//end itemFindQuery     
  } 
} //End onToggleAction



var morePushDetailsNeededMessage = "What are you trying to push?";
var buttonAPressedMessage = "Something happened when you pressed the button, but you can't tell what";
var pushAloneMessage = "You tried to push, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'push <item name> with NAME and NAME1 and NAME2' where name is your friend's first name!";
var whoAreYouPushingWithMessage = "I don't think that person's room is connected to yours...if they even exist!'";
var failedPushMessage = "Nothing happened when you pushed that.";
var pushNotEnoughPeopleMessage = "You will need more friends in order to push this."
var pushItemNotInRoomMessage = "You can't push an item that's not in your room";
var openSafeH = false;

function onPushAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup) { 
  var response = '';

  //Determine what someone is trying to push
  var tmp = input.substring(input.indexOf(' ')+1);
  var objectPushed = tmp.substring(0,tmp.indexOf(' with'));
  var friend = input.substring(input.indexOf("with ")+5);
  if(objectPushed==''){
    objectPushed = tmp;
  }
  var friendsList = friend.split(" and");
  for(var i = 0; i < friendsList.length; i++){
    friendsList[i] = friendsList[i].replace(" ","");
  }

  //If input only equals push ERROR
  if(input=='push'){
    response = morePushDetailsNeededMessage;
    emitDorisText(response, input_room.roomId);
  }  

  // hard-coded scenario push button A
    // change dark light to roomB
  else if(input=='push tempting button'&&input_room.roomId=='A'){
    userrooms.find({roomId:"B"},function(error,roomsB){
      roomB = roomsB[0];

      whereButtonA = {itemId:"buttonA"};
      items.find(whereButtonA,function(error,itemsA){
        buttonA = itemsA[0];
        var changeButtonStatus = false; 
        if(buttonA.unActivated==false){ //but if darklight is on, turn it off.
          changeButtonStatus = true;
        }         

        //Change DarkLight Status
        var change = {unActivated:changeButtonStatus};      
        items.findOneAndUpdate(whereButtonA, change, function(err,rawResponse){
        
          response = "Something happened, but you're not sure what...maybe try again?";
          emitDorisText(response, input_room.roomId);
  
          //Check changeImageCmd change on current command
          var itemImageIndex = -1;
          var itemImageState = 1;
          for(var i = 0; i < roomB.roomImageList.length; i++){
            if(roomB.roomImageList[i].imageId==buttonA.itemName){
              itemImageIndex = i;
              itemImageState = roomB.roomImageList[i].imageState;
            }            
          }
          // ONLY IF - item is in roomImageList
          if(itemImageIndex!=-1){
            advanceImageStates(buttonA.itemName, itemImageIndex, roomB, emitRoomChange);
          }
        });//end update item
      }); //button a query
    }); //userB query
  }

  // hard-coded scenario push button H
    // change dark light to roomH
  else if(input=='push red button'&&input_room.roomId=='H'){
    
    whereButtonH = {itemId:"buttonH"};
      items.find(whereButtonH,function(error,itemsH){
        buttonH = itemsH[0];
        var changeButtonStatus = false; 
        if(buttonH.unActivated==false){ //but if darklight is on, turn it off.
          changeButtonStatus = true;
        }         

        //Change DarkLight Status
        var change = {unActivated:changeButtonStatus};      
        items.findOneAndUpdate(whereButtonH, change, function(err,rawResponse){
        
          response = "A light changed!";
          emitDorisText(response, input_room.roomId);
  
          //Check changeImageCmd change on current command
          var itemImageIndex = -1;
          var itemImageState = 1;
          for(var i = 0; i < input_room.roomImageList.length; i++){
            if(input_room.roomImageList[i].imageId==buttonH.itemName){
              itemImageIndex = i;
              itemImageState = input_room.roomImageList[i].imageState;
            }            
          }
          // ONLY IF - item is in roomImageList
          if(itemImageIndex!=-1){
            advanceImageStates(buttonH.itemName, itemImageIndex, input_room, emitRoomChange);
          }
        });//end update item
      }); //button h query
  }

  // hard-coded scenario push button G
    // reset all other room lights
  else if(input=='push reset button'&&input_room.roomId=='G'){
      var specificRoom = "H";
      var safeH = "personal safe";

      //pickup hammer here
      items.find({itemName:safeH},function(error,itemSafeHList){
        itemH = itemSafeHList[0];

        userrooms.find({roomId:"H"},function(error,roomsH){
          var roomH = roomsH[0];

          if (itemH.unActivated==true&&openSafeH==false){
            openSafeH = true;
            activateItem(itemH, roomH, emitRoomChange, emitInvChange, emitGivePopup);

            var imgList = roomH.roomImageList;

            for(var j=0; j<imgList.length; j++){
              if(imgList[j].imageId==safeH){
                advanceImageStates(safeH, j, roomH, emitRoomChange);
              }
            }
            // Emit Doris Message to roomH about item found
            response = responseSafeH;
            emitDorisText(response, roomH.roomId);
    
            response = SPIRITMatchedMessage;
            emitDorisText(response, input_room.roomId);
          }//end if query
          else {
            response = failedPushMessage;
            emitDorisText(response, input_room.roomId);
          }
        });
      });//end findSafeH query
         

    // var change = {unActivated:true, description: "The light is off"};       
    // //reset roomA
    // userrooms.find({roomId:"A"},function(error,roomsA){
    //   roomA = roomsA[0];

    //   items.findOneAndUpdate({itemId: "lightA"}, change, function(err,rawResponse){
    //     response = "Something happened, but you're not sure what...";
    //     emitDorisText(response, input_room.roomId);

    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomA.roomImageList.length; i++){
    //       if(roomA.roomImageList[i].imageId=="light a"){
    //         advanceImageStates("light a", i, roomA, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery

    // //reset roomB
    // userrooms.find({roomId:"B"},function(error,roomsB){
    //   roomB = roomsB[0];
    //   items.findOneAndUpdate({itemId: "lightB"}, change, function(err,rawResponse){
    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomB.roomImageList.length; i++){
    //       if(roomB.roomImageList[i].imageId=="light b"){
    //         itemImageIndex = i;
    //         advanceImageStates("light b", i, roomB, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery

    // //reset roomC
    // userrooms.find({roomId:"C"},function(error,roomsC){
    //   roomC = roomsC[0];
    //   items.findOneAndUpdate({itemId: "lightC"}, change, function(err,rawResponse){
    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomC.roomImageList.length; i++){
    //       if(roomC.roomImageList[i].imageId=="light c"){
    //         advanceImageStates("light c", i, roomC, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery

    // //reset roomD
    // userrooms.find({roomId:"D"},function(error,roomsD){
    //   roomD = roomsD[0];
    //   items.findOneAndUpdate({itemId: "lightD"}, change, function(err,rawResponse){
    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomD.roomImageList.length; i++){
    //       if(roomD.roomImageList[i].imageId=="light d"){
    //         advanceImageStates("light d", i, roomD, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery

    // //reset roomE
    // userrooms.find({roomId:"E"},function(error,roomsE){
    //   roomE = roomsE[0];
    //   items.findOneAndUpdate({itemId: "lightE"}, change, function(err,rawResponse){
    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomE.roomImageList.length; i++){
    //       if(roomE.roomImageList[i].imageId=="light e"){
    //         advanceImageStates("light e", i, roomE, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery

    // //reset roomF
    // userrooms.find({roomId:"F"},function(error,roomsF){
    //   roomF = roomsF[0];
    //   items.findOneAndUpdate({itemId: "lightF"}, change, function(err,rawResponse){
    //     var itemImageIndex = -1;
    //     for(var i = 0; i < roomF.roomImageList.length; i++){
    //       if(roomF.roomImageList[i].imageId=="light f"){
    //         advanceImageStates("light f", i, roomF, emitRoomChange, true);
    //       }            
    //     }
    //   }); //end itemsQuery
    // }); //end userroomsQuery
  
    // spiritList = '';
  }

  //assume pushing heavy object
  else {
    items.find({itemName:objectPushed},function(error,itemsList){
      var item = itemsList[0];

      //if Item being pushed does not exist ERROR
      if(item==undefined){
        response = pushItemNotInRoomMessage;
        emitDorisText(response, input_room.roomId);
      }
      //If item is not supposed to be pushed ERROR
      else if(item.pushNumber==undefined){
        response = failedPushMessage;
        emitDorisText(response, input_room.roomId);
      }

      //check if trying to push item outside of room
      else if(input_room.itemsInRoom.indexOf(item.itemName)==-1) {
        response = pushItemNotInRoomMessage;
        emitDorisText(response, input_room.roomId);
      }

      //if pushing alone ERROR
      else if(input == 'push '+objectPushed){
        response = pushAloneMessage;
        emitDorisText(response, input_room.roomId);
      }

      //if already pushed
      else if(item.unActivated==false){
        response = failedPushMessage;
        emitDorisText(response, input_room.roomId);
      }

      //Not enough people pushing ERROR
      else if(friendsList.length<(item.pushNumber-1)){
        response = pushNotEnoughPeopleMessage;
        emitDorisText(response, input_room.roomId);
      }

      else {      
        var foundFriendsList = [];
        userrooms.find({},function(error,friendRooms){

          for(var j = 0; j < friendRooms.length; j++){
            userName = friendRooms[j].userName.toLowerCase().replace(" ",""); 
        // console.log("friendsList: "+ friendsList);
        //     console.log("userName: "+ userName);

            if(friendsList.indexOf(userName)!=-1&&input_room.linkedRooms.indexOf(friendRooms[j].roomId)!=-1){
              foundFriendsList.push(friendRooms[j].roomId);
              //possible option  friendsList.remove(userName);
            }
          }
          //Not all friends were real / rooms are not linked ERROR
          if(foundFriendsList.length<friendsList.length) {
            response = whoAreYouPushingWithMessage;
            emitDorisText(response, input_room.roomId);
          }

          //other wise, valid input
          else {
            stageNum = item.currentStage;
            if(stageNum==undefined){
              stageNum = 0;
            }

            if(item.itemStageList[stageNum].keyType=="push"){
              activateItem(item, input_room, emitRoomChange, emitInvChange);
              emitDorisText(item.itemStageList[stageNum].onStageCompleteMessage, input_room.roomId);
            }

            //Check changeImageCmd change on current command
            var itemImageIndex = -1;
            var itemImageState = 1;
            for(var i = 0; i < input_room.roomImageList.length; i++){
              if(input_room.roomImageList[i].imageId==objectPushed){
                itemImageIndex = i;
                itemImageState = input_room.roomImageList[i].imageState;
              }            
            }
   
            input_room.roomImageList.indexOf(item.itemName);

            // ONLY IF - changeItemCmd is pick, item is in roomImageList and imageState==0
            if(item.changeImageCmd=="push"&&itemImageIndex!=-1&&itemImageState==0){
              advanceImageStates(objectPushed, itemImageIndex, input_room, emitRoomChange);
            }
          }
        });//end userrooms query
      }
    });//end items query
  }
}//end onPushAction

var failedPullMessage = "Nothing happened when you pulled that";
var morePullDetailsNeededMessage = "What are you trying to pull?";
var pullAloneMessage = "You tried to pull, but it's just too heavy to do alone. Someone else might be able to help you...if the doors between your and another's room are open, you may have them help you push. Just type 'pull <item name> with NAME and NAME1 and NAME2' where name is your friend's first name!";
var whoAreYouPullingWithMessage = "I don't think that person's room is connected to yours...if they even exist!'"
var pullNotEnoughPeopleMessage = "You will need more friends in order to pull this."


function onPullAction(input, input_room, emitDorisText, emitRoomChange, emitInvChange, emitGivePopup) { 
  var response = '';

  //Determine what someone is trying to push
  var tmp = input.substring(input.indexOf(' ')+1);
  var objectPulled = tmp.substring(0,tmp.indexOf(' with'));
  var friend = input.substring(input.indexOf("with ")+5);
  if(objectPulled==''){
    objectPulled = tmp;
  }
  var friendsList = friend.split(" and");
  for(var i = 0; i < friendsList.length; i++){
    friendsList[i] = friendsList[i].replace(" ","");
  }

  //If input only equals pull ERROR
  if(input=='pull'){
    response = morePullDetailsNeededMessage; 
    emitDorisText(response, input_room.roomId);
  }  

  // hard-coded scenario
  if(input_room.roomId=="C" && (input=='pull harry potter from bookshelf'||input=='pull harry potter')) {
    items.find({itemName:"harry potter"},function(error,itemsList){
      var itemC = itemsList[0];
      if(itemC.unActivated==true){
        stageNum = itemC.currentStage;
        if(stageNum==undefined){
          stageNum = 0;
        }
        emitDorisText(itemC.itemStageList[stageNum].onStageCompleteMessage, input_room.roomId);
        activateItem(itemC, input_room, emitRoomChange, emitInvChange);
      }
    });
  }

  //assume pulling heavy object
  else {
    items.find({itemName:objectPulled},function(error,itemsList){
      var item = itemsList[0];
      //if Item being pulled does not exist ERROR
      if(item==undefined){
        response = cannotFindItemMessage;
        emitDorisText(response, input_room.roomId);
      }
      //If item is not supposed to be pulled ERROR
      else if(item.pullNumber==undefined){
        response = failedPullMessage; 
        emitDorisText(response, input_room.roomId);
      }

      //if pulling alone ERROR
      else if(input == 'pull '+objectPulled){
        response = pullAloneMessage;  
        emitDorisText(response, input_room.roomId);
      }

      //if already pulled
      else if(item.unActivated==false){
        response = failedPullMessage; 
        emitDorisText(response, input_room.roomId);
      }

      //Not enough people pulling ERROR
      else if(friendsList.length<(item.pullNumber-1)){
        response = pullNotEnoughPeopleMessage; 
        emitDorisText(response, input_room.roomId);
      }

      else {      
        var foundFriendsList = [];
        userrooms.find({},function(error,friendRooms){

          for(var j = 0; j < friendRooms.length; j++){
            userName = friendRooms[j].userName.toLowerCase();

            if(friendsList.indexOf(userName)!=-1&&input_room.linkedRooms.indexOf(friendRooms[j].roomId)!=-1){
              foundFriendsList.push(friendRooms[j].roomId);
              //possible option  friendsList.remove(userName);
            }
          }

          //Not all friends were real / rooms are not linked ERROR
          if(foundFriendsList.length<friendsList.length) {
            response = whoAreYouPullingWithMessage;
            emitDorisText(response, input_room.roomId);
          }

          //other wise, valid input
          else {
            stageNum = item.currentStage;
            if(stageNum==undefined){
              stageNum = 0;
            }

            if(item.itemStageList[stageNum].keyType=="pull"){
              activateItem(item, input_room, emitRoomChange, emitInvChange);
              emitDorisText(item.itemStageList[stageNum].onStageCompleteMessage, input_room.roomId);
            }

            //Check changeImageCmd change on current command
            var itemImageIndex = -1;
            var itemImageState = 1;
            for(var i = 0; i < input_room.roomImageList.length; i++){
              if(input_room.roomImageList[i].imageId==objectPulled){
                itemImageIndex = i;
                itemImageState = input_room.roomImageList[i].imageState;
              }            
            }
   
            input_room.roomImageList.indexOf(item.itemName);

            // ONLY IF - changeItemCmd is pick, item is in roomImageList and imageState==0
            if(item.changeImageCmd=="pull"&&itemImageIndex!=-1&&itemImageState==0){
              advanceImageStates(objectPulled, itemImageIndex, input_room, emitRoomChange);
            }
          }
        });//end userrooms query
      }
    });//end items query
  }
}//end onPullAction


//Function created to pick up actions after item command
function onServerPickUpAction(server_input, input_room, emitInvChange, emitGivePopup){
  //Pull item ID from input
  var input = server_input;
  var item_name = input.substring(input.indexOf('up ')+3);

  //Get Item from db
  items.find({itemName:item_name},function(error,items){
    //(assumption is that the valid names are listed in initial doorList)
    var item = items[0];

    //check if item exists DATA ERROR
    if(item==undefined) {
      console.log("DATA ERROR ON SERVER PICKUP: " + server_input);
    }
     else {
      //Add item to user Inventory
      var updatedUserInv = input_room.userInventory;
      updatedUserInv.push(item.itemName);

      //Update Arrays in db
      var whereQuery = {roomId: input_room.roomId};
      var change = {userInventory: updatedUserInv}; //Presuming here that no objects need to be removed from roomsList
      userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

        //Send new inventory to the front end 
        emitInvChange(updatedUserInv, input_room.roomId);  

        if(input_room.roomId=='H'&&item.itemName=="hammer") {
          emitGivePopup("Doris", item.itemName, "H");
        }
      });
    }
    });//end item query
} //endServerPickup


function advanceImageStates(target, target_index, room, emitRoomChange){
  advanceImageStates(target, target_index, room, emitRoomChange, false);
}

//updates needed to setup emitRoomChange
function advanceImageStates(target, target_index, room, emitRoomChange, isRoomG){
  //if target is a door
  if(target.substring(0,4)=="door") {
    //TODO: Maybe?
  }

  //if target is an item
  else {

    items.find({itemName:target},function(error,items){
      var item = items[0];
      var i = target_index;

      if(room.roomImageList[i].imageState==1){
        room.roomImageList[i].imageState = 0;
      } else {
        room.roomImageList[i].imageState = 1;
      }
      
      //hardcode isRoomG
      if(isRoomG==true){
        room.roomImageList[i].imageState = 0;
      }

      var whereQuery = {roomId: room.roomId};
      var change = {roomImageList: room.roomImageList};
      userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
        emitRoomChange(room, room.roomId);
      });
      
    });
  }
} //end advanceImageStates


var otherDoorLockedMessage = "Your door is open, but it appears that there is a door still blocking you from interacting with the other room. You cannot open it from your side.";
// Function sets variables needed to openDoor
// set locked = false, set description to unlocked description, and run postStageCmd cmd
//ServerOnly function, so we assume input is correct
function openDoor(input_door, input_room, emitRoomChange, emitInvChange, dorisDoors){

  /*** STAGE HANDLING ***/
  var stageNum = input_door.currentStage;
  var lockedSet = input_door.locked;
  var doorDescription = input_door.description;
  if(stageNum==undefined){
    stageNum = 0;
  }
  var change;
  //If Final Stage, door is unlocked
  if(stageNum==(input_door.doorStageList.length-1)){    
    
    //Check to see that door on opposite side is open as well
    var input_doorId = input_door.doorId;
    var otherDoor = input_doorId.substring(0,4)+input_doorId.substring(5,6)+input_doorId.substring(4,5);

    //check if opposite door is open
    doors.find({doorId:otherDoor},function(error,doors2){
      var door2 = doors2[0];

      //If other door is unlocked, link all of the rooms together
      if(door2.locked==false) {
        var newRoom = input_door.doorId.substring(input_door.doorId.length-1);
        linkRooms(input_room, newRoom);

        //modify door description if needed
        var currStageNum = input_door.currentStage;
        if(currStageNum==undefined){
          currStageNum = 0;
        }

        doorDescription = input_door.doorStageList[currStageNum].onStageCompleteDesc;
        if(doorDescription==undefined){ doorDescription = "The door is open!"; }

        var whereQuery1 = {doorId: input_doorId};
        var changeDesc1 = {description: doorDescription};
        doors.findOneAndUpdate(whereQuery1, changeDesc1, function(err,rawResponse){});  

        var whereQuery2 = {doorId: otherDoor};
        var changeDesc2 = {description: doorDescription};
        doors.findOneAndUpdate(whereQuery2, changeDesc2, function(err,rawResponse){});  

      } 
      else {
        
        //Modify door description appropriately
        doorDescription = otherDoorLockedMessage;

        if (input_doorId == 'doorAP') {
          doorDescription = input_door.doorStageList[0].onStageCompleteDesc;
        }

        var whereQuery1 = {doorId: input_doorId};
        var changeDesc1 = {description: doorDescription};
        doors.findOneAndUpdate(whereQuery1, changeDesc1, function(err,rawResponse){});  

      }
    }); //end doors query

    //set locked to false
    lockedSet = false;
    stageNum+=1;
    change = {locked: lockedSet, currentStage: stageNum};

  } 
  //door not yet unlocked, advance to next stage
  else {
    lockedSet = true;
    doorDescription = (input_door.doorStageList[stageNum]).onStageCompleteDesc;
    if(doorDescription==undefined){ doorDescription = door.description; }
    stageNum += 1;
    
    change = {locked: lockedSet, currentStage: stageNum, description: doorDescription};

  }

  //update db function
  var whereQuery = {doorId: input_door.doorId};
  if(change=={}){ console.log("DOOR CHANGE ERROR"); }
  doors.findOneAndUpdate(whereQuery, change, function(err,rawResponse){

    //send doris doors - only if door was unlocked
    if(lockedSet==false) {
      doors.find({},function(error,doorsList){
        dorisDoors(doorsList,"dorisview");
      });
    }

    //insert testlog
    var log = new testlogs({
                          itemOrDoor: "door",
                          doorId: input_door.doorId,
                          currentStage: input_door.doorStageList[stageNum-1],
                          currentRoom: input_room.roomId,
                          timeStamp: (new Date()).toString()
                          });
    log.save(function(err,logSaved){});

    //emit DoorOpen to frontEndRoom
    if(lockedSet==false){
      emitRoomChange(input_room, input_room.roomId);
    }

    //handle postStageCmd
    var server_input = input_door.doorStageList[stageNum-1].postStageCmd;
    if(server_input!=undefined&&server_input!=''&&server_input!=null){
      //assume server_input is PickupAction
      onServerPickUpAction(server_input, input_room, emitInvChange);
    }
  });
}//end openDoor 


// Activate item being passed
// Set activated = true, set description to activated description, and run onActivateItem command
function activateItem(item, input_room, emitRoomChange, emitInvChange, emitGivePopup){
  //Hard-coded scenarios
  var specialKeypadK = "keypad";
  var specialTopHatJ = "top hat";
  var specialRedHatP = "red hat";

  //Handle Item Stages
  var stageNum = item.currentStage;
  var activationSet = item.unActivated;
  var itemDescription = item.description;
  if(stageNum==undefined){
    stageNum = 0;
  } 

  //Advance to final stage
  if(stageNum==(item.itemStageList.length-1)){
    activationSet = false;
  } 
  //item not yet activated, advance to next stage
  else {
    activationSet = true;
  }
  

  itemDescription = (item.itemStageList[stageNum]).onStageCompleteDesc;
  if (itemDescription==undefined) {
    itemDescription = item.description;
  }
  stageNum += 1;

  //update db function
  var whereQuery = {itemName: item.itemName};
  var change = {unActivated: activationSet, currentStage: stageNum, description: itemDescription};
  items.findOneAndUpdate(whereQuery, change, function(err,rawResponse) {
    //insert testlog
    var log = new testlogs({
                          itemOrDoor: "item",
                          itemName: item.itemName,
                          currentStage: item.itemStageList[stageNum-1],
                          currentRoom: input_room.roomId,
                          timeStamp: (new Date()).toString()
                          });
    log.save(function(err,logSaved){});

    //handle postStageCmd
    var server_input = item.itemStageList[stageNum-1].postStageCmd;
    if(server_input!=undefined&&server_input!=''&&server_input!=null){
      //assume server_input is PickupAction
      onServerPickUpAction(server_input, input_room, emitInvChange, emitGivePopup);

      //hard-coded handling specialKeypad4k scenario
      if(item.itemName==specialKeypadK){
        var whereQuery = {itemName: specialTopHatJ};
        var changeStage = {
                    keyType: "look", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "I think the hat is actually empty this time.",
                    onStageCompleteMessage: "You found a rod? How did that fit in a hat?!",
                    postStageCmd: "pick up rod"
                 };
        var change = {itemStageList: changeStage};
        items.findOneAndUpdate(whereQuery, change, function(err,rawResponse){});

        var whereQueryP = {itemName: specialRedHatP};
        var changeStageP = {
                    keyType: "look", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "The red hat looks normal now.",
                    onStageCompleteMessage: "You found a reel near the computer!",
                    postStageCmd: "pick up reel" //FINDME change to pick up reel
                };
        var changeP = {itemStageList: changeStageP};
        items.findOneAndUpdate(whereQueryP, changeP, function(err,rawResponse){});
       }
    }
  }); //endItemQuery
} //End on activateItem



//Does prep work to appropriately link all rooms
function linkRooms(input_room, newRoomId){
  //send both rooms to linkRooms Function
  userrooms.find({roomId:newRoomId},function(error,rooms2){
    room2 = rooms2[0];

    //combine input_room linkedRooms and room2.linkedRooms
    var linkedRooms = input_room.linkedRooms;
    var otherList = room2.linkedRooms;
    for(var i = 0; i < otherList.length; i++){
       //Only add new room to list if it isn't there now
       if(linkedRooms.indexOf(otherList[i])==-1){
        linkedRooms.push(otherList[i]);
       }
    }

    //sort linkedRoomsList
    linkedRooms = linkedRooms.sort();

    //call helper function
    linkRoomsHelper(linkedRooms, 0);

  });
} //end linkRooms

//Called recursively to link all rooms affected by door being open
function linkRoomsHelper(linkedRoomsList, index){
  //if index == the length of linkedRoomsList, this is finished
  if(index==linkedRoomsList.length){

  }
  //update current room from linkedRoomsList
  else {
    var whereQuery = {roomId: linkedRoomsList[index]};
    var change = {linkedRooms: linkedRoomsList};
    userrooms.findOneAndUpdate(whereQuery, change, function(err,rawResponse){
      linkRoomsHelper(linkedRoomsList, index+1);
    });
  }
} //end linkRoomsHelper

/*Start Server*/ 
http.listen(3661, function(){
  console.log('listening on *:3661');
//  mongodb://localhost/trapped<use trapped; on mongo>/schema<collection>_name 
  mongoose.connect('mongodb://localhost/trapped_db');
});



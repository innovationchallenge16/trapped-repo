//Reset database
//use trapped_db;

db.items.drop();
db.userrooms.drop();
db.doors.drop();  
db.testlogs.drop();
db.createCollection("items");
db.createCollection("userrooms");
db.createCollection("doors");
db.createCollection("testlogs");

//UserRooms - 8
//TODO ADD All roomSafes
  db.userrooms.insert(
   [{	
   //1a: use password from physical jigsaw puzzle on doorAB

  	roomId: "A",
  	roomDescription: "You seem to be trapped in a large walk-in closet. Who needs this much space? You see a secure safe, a purse, a light with a switch, hanging coats, a spiffy charger, and most importantly a tempting button that is begging to be pushed. The only ways out that you can see are two doors on either side of the room.",
  	roomLeftDoor: "doorAB", 
  	roomRightDoor: "doorAP",
  	roomImagePath: "images/newRooms/closet/closet.png",
    roomImageList:   [{ imageId: "A",
                        imageState: 0 },
                      { imageId: "doorAB", 
                        imageState: 0  },
                      { imageId: "doorAP",
                        imageState: 0 },
                      { imageId: "spiffy charger",
                        imageState: 0 },
                      { imageId: "secure safe",
                        imageState: 0 },
                      { imageId: "light a",
                        imageState: 0 }],
  	roomLightSwitch: false, //by default, light is off and this is true
    heavyItemMoved: true, //This is true because theres no heavy item in the room
  	linkedRooms: ["A"],
  	itemsInRoom: ["tempting button", "spiffy charger", "secure safe", "light a", "hanging coats", "purse", "shoebox"],
  	userName: "Benjamin",
    userPassword: "hatchling123",
  	userInventory: ["note 1a"]
   },
   {
   //1b: use password from winning rock paper scissors on doorBA
  	roomId: "B",
  	roomDescription: "As you look around you suppress a slight shiver. It is quite cold in here. You see a light with a switch, a toolbox, a car and a trash can. The only ways out that you can see are two doors on either side of the room.",
  	roomLeftDoor: "doorBC", 
  	roomRightDoor: "doorBA",
  	roomImagePath: "images/newRooms/garage/garage.png", 
  	roomImageList:   [{ imageId: "B",
                        imageState: 0 },
                      { imageId: "doorBC", 
                        imageState: 0  },
                      { imageId: "doorBA",
                        imageState: 0 },
                      { imageId: "hidden panel",
                        imageState: 0 },
                      { imageId: "light b",
                        imageState: 0 },
                      { imageId: "tempting button",
                        imageState: 0 }], 
  	roomLightLetter: "I",
  	itemsInRoom: ["hidden panel", "light b", "toolbox", "car", "trash can"],
  	linkedRooms: ["B"],
  	userName: "Lauren-charlee",
    userPassword: "cria456",
  	userInventory: ["note 1b", "note 2b"]
   },
   {
  	roomId: "C",
  	roomDescription: "You find yourself trapped in some sort of library. You see a light with a switch, a coffee mug, a sign, and a bookshelf on either side of a couch. Every so often you can hear a faint scratching sound. The only ways out that you can see are the two doors on either side of the room.",
  	roomLeftDoor: "doorCD", 
  	roomRightDoor: "doorCB",
  	roomImagePath: "images/newRooms/library/library.png",
  	roomImageList:   [{ imageId: "C",
                        imageState: 0 },
                      { imageId: "doorCD", 
                        imageState: 0  },
                      { imageId: "doorCB",
                        imageState: 0 },
                      { imageId: "couch",
                        imageState: 0 },
                      { imageId: "mouse",
                        imageState: 0 },
                      { imageId: "light c",
                        imageState: 0 }],
  	roomLightLetter: "I",
  	itemsInRoom: ["couch", "bookshelf", "mouse", "light c", "coffee mug", "sign"],
  	linkedRooms: ["C"],
  	userName: "Alexander m",
    userPassword: "antling789",
  	userInventory: ["note 1c"]
   },
   {
  	roomId: "D",
  	roomDescription: "You find yourself in a small dining room with a light and switch, a painting, a mug, some cheddar, and kitchen chairs. The figure in the painting on the wall looks familiar to you. The only ways out that you can see are two doors on either side of the room. ",
  	roomLeftDoor: "doorDE", 
  	roomRightDoor: "doorDC",
  	roomImagePath: "images/newRooms/dining/dining.png",
  	roomImageList:   [{ imageId: "D",
                        imageState: 0 },
                      { imageId: "doorDE", 
                        imageState: 0  },
                      { imageId: "doorDC",
                        imageState: 0 },
                      { imageId: "cheddar",
                        imageState: 0 },
                      { imageId: "light d",
                        imageState: 0 }],
  	roomLightLetter: "R",
  	itemsInRoom: ["cheddar", "light d", "painting", "kitchen chairs", "mug"], 
  	linkedRooms: ["D"],
  	userName: "Carlye-aqeelah",
    userPassword: "codling101",
  	userInventory: ["note 1d", "note 2d"]
   },
    {
  	roomId: "E",
  	roomDescription: "You find yourself in a clean office. As you look around you notice a light and switch, a wall clock, a wastebin, a workdesk, an outlet, and a number panel. The clock on the wall does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
  	roomLeftDoor: "doorEF", 
  	roomRightDoor: "doorED",
  	roomImagePath: "images/newRooms/office/office.png",
  	roomImageList:   [{ imageId: "E",
                        imageState: 0 },
                      { imageId: "doorEF", 
                        imageState: 0  },
                      { imageId: "doorED",
                        imageState: 0 },
                      { imageId: "number panel",
                        imageState: 0 },
                      { imageId: "light e",
                        imageState: 0 }],
    roomLightLetter: "S",	
  	itemsInRoom: ["number panel", "light e", "wall clock", "wastebin", "workdesk", "outlet"],
  	linkedRooms: ["E"],
  	userName: "Matt",
    userPassword: "pullet112",
  	userInventory: ["note 1e","note 1e-2"]
   },
   {
  	roomId: "F",
  	roomDescription: "You find yourself in a retro looking kitchen. As you look around you see a light and switch, an oven, a pantry, the counter, and a fridge with magnets on it. The only ways out that you can see are the two doors on either side of the room.",
  	roomLeftDoor: "doorFG", 
  	roomRightDoor: "doorFE",
  	roomImagePath: "images/newRooms/kitchen/kitchen.png",
  	roomImageList:   [{ imageId: "F",
                        imageState: 0 },
                      { imageId: "doorFG", 
                        imageState: 0  },
                      { imageId: "doorFE",
                        imageState: 0 },
                      { imageId: "fridge",
                        imageState: 0 },
                      { imageId: "cold safe",
                        imageState: 0 },
                      { imageId: "light f",
                        imageState: 0 }],
    roomLightLetter: "T",
  	itemsInRoom: ["fridge", "cold safe", "light f", "oven", "pantry", "counter"],
  	linkedRooms: ["F"],
  	userName: "Bella-shalyn",
    userPassword: "puggle131",
  	userInventory: ["note 1f"]
   },
   { //TODO add special use case toc CheckRoomChange for G
  	roomId: "G",
  	roomDescription: "You find yourself in a strange room. You think that this room may be used to clean clothes but you are not sure how. Your mother always does it for you anyway. As you look around a plain safe, a washing machine, a dryer, a reset button, an ironing board, and some sort of electronic charger catches your eye. There are three doors in this room, one on either side of the room and a center door which looks like it has been nailed down. I wonder what is behind there?",
  	roomLeftDoor: "doorGH", 
  	roomRightDoor: "doorGF",
  	roomImagePath: "images/newRooms/laundryRoom/laundryRoom.png",
  	roomImageList:   [{ imageId: "G",
                        imageState: 0 },
                      { imageId: "doorGH", 
                        imageState: 0  },
                      { imageId: "doorGF",
                        imageState: 0 },
                      { imageId: "electronic charger",
                        imageState: 0 },
                      { imageId: "plain safe",
                        imageState: 0 },
                      { imageId: "center door",
                        imageState: 0 },
                      { imageId: "vending machine",
                        imageState: 0 }],    
    centerDoorGOpen:true,
  	itemsInRoom: ["electronic charger", "plain safe","center door","vending machine", "reset button", "washing machine", "dryer", "ironing board"],
  	linkedRooms: ["G"],
  	userName: "Ismael",
    userPassword: "elver415",
  	userInventory: ["note 1g","note 4g"]
   },
   { 
  	roomId: "H",
  	roomDescription: "You find yourself in a bedroom of some sorts. You see a full bed, a red button, a red box, a lamp, and for some reason a personal safe on a shelf. Who puts a safe on a shelf? Near the shelf there is a light but no switch to toggle. You also sense that there is a secret panel in the room. The only ways out that you can see are the two doors on either side of the room.",
  	roomLeftDoor: "doorHI", 
  	roomRightDoor: "doorHG",
  	roomImagePath: "images/newRooms/guestRoom/guestRoom.png",
  	roomImageList:   [{ imageId: "H",
                        imageState: 0 },
                      { imageId: "doorHI", 
                        imageState: 0  },
                      { imageId: "doorHG",
                        imageState: 0 },
                      { imageId: "personal safe",
                        imageState: 0 },
                      { imageId: "secret panel",
                        imageState: 0 },
                      { imageId: "red button",
                        imageState: 0 }],
  	itemsInRoom: ["personal safe","secret panel", "old remote", "red button", "full bed", "lamp", "red box"],
  	linkedRooms: ["H"],
  	userName: "Luz-scott",
    userPassword: "gosling161",
  	userInventory: ["note 1h", "note 2h"]
   }
   ]);


////ITEMS LIST//// - 45

db.items.insert([
//ALL NOTES
 {
  itemId: "note1A",
  itemName: "note 1a",
  description: "Look under your chair...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1A.png" 
 },
 {
  itemId: "note1B",
  itemName: "note 1b",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1B.png"
 },
 {
  itemId: "note2B",
  itemName: "note 2b",
  description: "Day 5: I know that there is a hidden panel in here somewhere... Maybe if I try a certain word on hidden panel it will open.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2B.png" 
 },
 {
  itemId: "note1C",
  itemName: "note 1c",
  description: "Day 23: Doris has given me the following clue for one of the door passwords: What year did two companies merge in order to form ConocoPhillips? How am I supposed to figure it out without google?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1C.png" 
 },
 {
  itemId: "note1D",
  itemName: "note 1d",
  description: "Day 73: I know I have seen the man in the painting before. Could it have been when I was looking up information on ConocoPhillips the night before I got here? Maybe it is the password to one of the doors",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1D.png" 
 },
  {
  itemId: "note2D",
  itemName: "note 2d",
  description: "Day 91: The stratching sounds will not stop. They are driving me crazy. I think they are coming from under the couch next door.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2D.png" 
 },
 {
  itemId: "note1E",
  itemName: "note 1e",
  description: "Day 52: Still can not figure out the password to one of the doors. The only thing Doris will say when I ask is Tick Toc...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1E.png"  

 },
 {
  itemId: "note1E-2",
  itemName: "note 1e-2",
  description: "Day 10: I have tried every combination I can think of with birthdays on the safe behind the fridge. What does Doris mean by the the birthday of the one whose birth month comes first?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2E.png" 
 },
 {
  itemId: "note1F",
  itemName: "note 1f",
  description: "Day 67: I still cannot unscramble the letters on the fridge. I know it has to do with ConocoPhillips but what else? Come on THINK! I think the two words that they spell is the password to one of the doors",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1F.png" 
 },
 {
  itemId: "note1G",
  itemName: "note 1g",
  description: "Look under your chair...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1G.png" 
 },
 {
  itemId: "note4G",
  itemName: "note 4g",
  description: "Day 107: We have noticed that there are 6 lights in 6 different rooms. Each are labeled something with a letter and have their own light switch. Additionally there is one light with no letter or a switch. Perhaps if the bulbs were lit in the correct order they would light up. But what do the letters mean? Maybe something to do with ConocoPhillips?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note4G.png" //TODO MAKE THIS NOTE 
 },
 {
  itemId: "note1H",
  itemName: "note 1h",
  description: "Day 101: I have finally tricked Doris into giving me a clue to the password to one of the doors. Doris said something about inspecting the code comments? What does that even mean though?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1H.png" 
 },
 {
  itemId: "note2H",
  itemName: "note 2h",
  description: "Day 5: I know that there is a secret panel in here somewhere... Maybe if I try a certain word on secret panel it will open.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2H.png" 
 }]);

//ROOM A ITEMS -- 39
db.items.insert([
 {
  itemId: "chargerA",
  itemName: "spiffy charger",
  description: "This item is used to charge electronic objects",
    canBeHeld: true,
  unActivated: false,
  imagePath: "images/items/chargerA.png",
  roomImagePath: "images/newRooms/closet/closetSpiffyCharger.png",
  changeImagePath: "images/newRooms/closet/closetBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" // Command that signals item will need to be changed
 }, 
 {
  itemId: "safeA",
  itemName: "secure safe",
  description: "I wonder if there's anything useful in here...",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "plastic remote",
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up blue key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/closet/closetBlank.png",
  changeImagePath: "images/newRooms/closet/closetOpen.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed 
 },
 {
  itemId: "lightA",
  itemName: "light a",
  description: "The light is off",
    canBeHeld: false,
  unActivated: true,
  roomLightLetter: "P", 
  imagePath: "",
  roomImagePath: "images/newRooms/closet/closetBlank.png",
  changeImagePath: "images/newRooms/closet/closetOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 {
  itemId: "keyBC", //found in room A, in safeA
  itemName: "blue key",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/blueKey.png" 
 },

 //ROOM B ITEMS
 {
  itemId: "panelB",
  itemName: "hidden panel",
  description: "This is a hidden panel - if you tell it a password, it may open for you.",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password",
                    key: "integrity",
                    onStageCompleteDesc: "It's just a panel.",
                    onStageCompleteMessage: "You found a plastic remote! I wonder what you could use it on...",
                    postStageCmd: "pick up plastic remote"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/garage/garageBlank.png",
  changeImagePath: "images/newRooms/garage/garageUp.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed 
 },
 {
  itemId: "remoteB",
  itemName: "plastic remote",
  description: "This remote looks dead...",
  canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "spiffy charger",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote."
                }],
  imagePath: "images/items/remoteB.png" 
 },
 { //buttonA represents an image in roomB
  itemId: "buttonA",
  itemName: "tempting button",
  description: "",
  canBeHeld: false,
  unActivated: true,
  imagePath: "",
  roomImagePath: "images/newRooms/garage/garageBlank.png",
  changeImagePath: "images/newRooms/garage/garageDark.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 {
  itemId: "lightB",
  itemName: "light b",
  description: "The light is off",
  canBeHeld: false,
  unActivated: true,
  roomLightLetter: "I", 
  imagePath: "",
  roomImagePath: "images/newRooms/garage/garageBlank.png",
  changeImagePath: "images/newRooms/garage/garageOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 //ROOM C ITEMS
 {//TODO CHECK THIS ITEM
  itemId: "couchC",
  itemName: "couch",
  description: "This is heavy, I wonder if there's anything behind it?",
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "D",
                    onStageCompleteDesc: "Now that you've moved the couch once, you really don't feel like moving it any more.",
                    onStageCompleteMessage: "You found a mouse sitting behind the bookself...he's holding a key!"
                }],
  imagePath: "" ,
  roomImagePath: "images/newRooms/library/libraryBlank.png",
  changeImagePath: "images/newRooms/library/libraryMoved.png", //Path for image once its been changed
  changeImageCmd: "push"
 },
 {
  itemId: "bookshelfC",
  itemName: "bookshelf",
  description: "There's an assortment of books here, Harry Potter, The Longest Ride, Gone Girl...you should pull one that you're interested in. Who knows, maybe something will happen?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "pull",
                    key: "harry potter", //Not used stage
                    onStageCompleteDesc: "It was pretty smart to pull that book. I don't think you can do anything else here.",
                    onStageCompleteMessage: "What a brilliant idea! You found an extension cord.",
                    postStageCmd: "pick up extension cord"
                }],
  imagePath: "" 
 },
 {
  itemId: "bookC-1",
  itemName: "harry potter",
  description: "Maybe something magical will happen if you pull this from bookshelf?",
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "pull",
                    key: "harry potter", //FINDME PERSONALIZED
                    onStageCompleteDesc: "It was pretty smart to pull this book. You don't have time to read this book now.",
                    onStageCompleteMessage: "What a brilliant idea! You found an extension cord.",
                    postStageCmd: "pick up extension cord"
                }],
  imagePath: "" 
 },
 {
  itemId: "bookC-2",
  itemName: "gone girl",
  description: "Maybe something dramatic will happen if you pull this from bookshelf?",
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "the longest ride",
  itemName: "wild",
  description: "Maybe something wild will happen if you pull this from bookshelf?",
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
 {
  itemId: "mouseC", //found behind couch
  itemName: "mouse",
  description: "I wonder if the critter has anything I need?",
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "cheddar",
                    onStageCompleteDesc: "The mouse scattered away after you fed him. You can't see him anymore.",
                    onStageCompleteMessage: "The mouse appreciated your cheese, and left a key behind him!",
                    postStageCmd: "pick up purple key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/library/libraryBlank.png",
  changeImagePath: "images/newRooms/library/libraryItemDropped.png", //Path for image once its been changed
  changeImageCmd: "use" 
 },
 {
  itemId: "extensionCordC", //found behind bookshelf
  itemName: "extension cord",
  description: "this cord could connect a shorter cord to power...",
    canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/extensionC.png" 
 },
 {
  itemId: "keycb", //found from the mouse
  itemName: "purple key",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/purpleKey.png" 
 },
 {
  itemId: "lightC",
  itemName: "light c",
  description: "The light is off",
    canBeHeld: false,
  unActivated: true,
  roomLightLetter: "I", 
  imagePath: "",
  roomImagePath: "images/newRooms/library/libraryBlank.png",
  changeImagePath: "images/newRooms/library/libraryOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 //ROOM D ITEMS
 {
  itemId: "cheeseD",
  itemName: "cheddar",
  description: "cheddar is yummy",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/cheeseO.png",
  roomImagePath: "images/newRooms/dining/diningYummyCheese.png",
  changeImagePath: "images/newRooms/dining/diningBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" 
 },
 {
  itemId: "lightD",
  itemName: "light d",
  description: "The light is off",
    canBeHeld: false,
  unActivated: true,
  roomLightLetter: "R", 
  imagePath: "",
  roomImagePath: "images/newRooms/dining/diningBlank.png",
  changeImagePath: "images/newRooms/dining/diningOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 //ROOM E ITEMS
 {
  itemId: "panelE",
  itemName: "number panel",
  description: "The panel in this room has a keypad, indicating you can enter numbers. No spaces, hashes, or asterisks though. #unfair.",
  canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based vending machine hint
                    key: "030505060606060810101112", //FINDME Personalized! - ages A-H sorted by least to most
                    onStageCompleteDesc: "The panel is open",
                    onStageCompleteMessage: "You found a power plug...not connected to anything. Is there anything that needs electricity?",
                    postStageCmd: "pick up power plug"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/office/officeBlank.png",
  changeImagePath: "images/newRooms/office/officeUp.png", //Path for image once its been changed
  changeImageCmd: "try"  
 },

 {
  itemId: "clockE",
  itemName: "wall clock",
  description: "The time is 3:40",
    canBeHeld: false,
  unActivated: false,
  imagePath: ""
 },

 {
  itemId: "power plugE",
  itemName: "power plug",
  description: "This power plug could be used on something to give it power...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/extensionC.png" 
 },
 {
  itemId: "lightE",
  itemName: "light e",
  description: "The light is off",
    canBeHeld: false,
  unActivated: true,
  roomLightLetter: "S", 
  imagePath: "",
  roomImagePath: "images/newRooms/office/officeBlank.png",
  changeImagePath: "images/newRooms/office/officeOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 //ROOM F ITEMS
 {
  itemId: "heavyItemF",
  itemName: "fridge",
  description: "This is heavy, I wonder if there's anything behind it?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "E",
                    onStageCompleteDesc: "There's a cold safe behind this...it looks like it needs a combination",
                    onStageCompleteMessage: "You found a cold safe!"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kitchen/kitchenBlank.png",
  changeImagePath: "images/newRooms/kitchen/kitchenMoved.png", //Path for image once its been changed
  changeImageCmd: "push"  
 },
 {
  itemId: "safeF",
  itemName: "cold safe",
  description: "I wonder if there's anything useful in here...there's space for 4 numbers to open it",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //User derives this password based on a hint note1E-2
                    key: "0310", //FINDME Personalized! userE birthday NOTE:T UserE must be older than userF
                    onStageCompleteDesc: "The safe is open, but empty.",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up green key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kitchen/kitchenBlank.png",
  changeImagePath: "images/newRooms/kitchen/kitchenOpen.png", //Path for image once its been changed
  changeImageCmd: "try"  
 },
 {
  itemId: "keyFG",
  itemName: "green key",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/greenKey.png" 
 },
 {
  itemId: "lightF",
  itemName: "light f",
  description: "The light is off",
    canBeHeld: false,
  unActivated: true,
  roomLightLetter: "T", 
  imagePath: "",
  roomImagePath: "images/newRooms/kitchen/kitchenBlank.png",
  changeImagePath: "images/newRooms/kitchen/kitchenOn.png", //Path for image once its been changed
  changeImageCmd: "toggle" // Command that signals item will need to be changed 
 },
 //ROOM G ITEMS
 {
  itemId: "safeG",
  itemName: "plain safe",
  description: "I wonder if there's anything useful in here...",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "old remote",
                    onStageCompleteDesc: "The key has been put in your inventory, so the safe is now empty",
                    onStageCompleteMessage: "You found a key! I wonder what you could use it on...",
                    postStageCmd: "pick up gold key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/laundryRoom/laundryRoomBlank.png",
  changeImagePath: "images/newRooms/laundryRoom/laundryRoomOpen.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changeImageCmd 
 },
 {
  itemId: "chargerG",
  itemName: "electronic charger",
  description: "This item is used to charge electronic objects",
    canBeHeld: true,
  unActivated: false,
  imagePath: "images/items/chargerH.png",
  roomImagePath: "images/newRooms/laundryRoom/laundryRoomCharger.png", 
  changeImagePath: "images/newRooms/laundryRoom/laundryRoomBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" 
 },
 {
  itemId: "buttonG", //TODO THIS MIGHT NOT BE AN ISSUE
  itemName: "reset button",
  description: "You see this button has the letters RESET on it",
  canBeHeld: false,
  unActivated: false, //true if change needed to object, default false
  imagePath: ""
 },
 {
  itemId: "keyGF",
  itemName: "gold key",
  description: "This key looks different...it has the words 'exploration and production' engraved on it.",
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/goldEPKey.png" 
 },
 {
  itemId: "center door G",
  itemName: "center door",
  description: "This door looks like its nailed shut",
  canBeHeld: false,
  unActivated: true,
  pullNumber: 8, 
  itemStageList: [{
                    keyType: "key",
                    key: "hammer",
                    onStageCompleteDesc: "This door looks loose, maybe if you try to pull it, you'll see some results...",
                    onStageCompleteMessage: "You removed the nails, and the door looks like it just needs to be pulled"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/laundryRoom/laundryRoomBlank.png",
  changeImagePath: "images/newRooms/laundryRoom/laundryRoomVendingMachine.png", //Path for image once its been changed
  changeImageCmd: "use" 
 },
 {
  itemId: "vendingMachineG",
  itemName: "vending machine",
  description: "This vending machine clearly is dead. I think all of the food in it is probably bad. You see a note on the vending machine, it says 'Theres a panel you should open. Order the players by days of birth, using months. Another note reads 'one of you has a favorite book. If you can find pull the book, it may prove to be useful to you.' ", //enter every players birthday in backwords order
    canBeHeld: false,
  unActivated: true, 
  pushNumber: 16,
  itemStageList: [{
                    keyType: "key",
                    key: "power plug",
                    onStageCompleteDesc: "The plug doesn't quite reach the wall, but you're so close! Got something to extend it?",
                    onStageCompleteMessage: "The plug doesn't quite reach the wall, but you're so close!"
                },
                {
                    keyType: "key",
                    key: "extension cord",
                    onStageCompleteDesc: "The vending machine is plugged into the wall! Now all you need is some money to get a key from it.",
                    onStageCompleteMessage: "That worked! Now the vending machine has power...got money?"
                },
                {
                    keyType: "key",
                    key: "money",
                    onStageCompleteDesc: "This looks insanely heavy...it would probable take the combined strength of everyone you know to move this.",
                    onStageCompleteMessage: "That worked, and you got a key for your trouble.",
                    postStageCmd: "pick up final key"
                },
                {
                    keyType: "push",
                    key: "",
                    onStageCompleteDesc: "What are you doing? Get out of here!",
                    onStageCompleteMessage: "What a feat of teamwork! Doris is allowing you to leave the house rooms and proceed to the last challenge.",
                    postStageCmd: ""
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/laundryRoom/laundryRoomBlank.png",
  changeImagePath: "images/newRooms/laundryRoom/laundryRoomCommonDoor.png", //Path for image once its been changed
  changeImageCmd: "push" 
 },
 {
  itemId: "final key",
  itemName: "final key",
  description: "This key feels more important than any other key you've seen this whole game.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/key.png" 
 },
 {
 //ROOM H ITEMS
 itemId: "panel H",
  itemName: "secret panel",
  description: "This is a secret panel",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //user derives this based on safety obviously being the most important thing to COP
                    key: "safety",
                    onStageCompleteDesc: "You found a remote! I wonder what you could use it on...",
                    onStageCompleteMessage: "The remote has been put in your inventory",
                    postStageCmd: "pick up old remote"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/guestRoom/guestRoomBlank.png",
  changeImagePath: "images/newRooms/guestRoom/guestRoomUp.png", //Path for image once its been changed
  changeImageCmd: "try"  
 },
  {
  itemId: "remoteH",
  itemName: "old remote",
  description: "This remote looks dead...",
    canBeHeld: true,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "electronic charger",
                    onStageCompleteDesc: "This remote is fully charged! I wonder what you can use it on.",
                    onStageCompleteMessage: "The charger fully charged the remote."
                }],
  imagePath: "images/items/remoteB.png" 
 },
 {
  itemId: "safeH", //TODO THIS MIGHT NOT BE AN ISSUE
  itemName: "personal safe",
  description: "The safe looks electronic...but it doesn't seem to give you an indication of how to open it.",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "TEMP",
                    key: "",
                    onStageCompleteDesc: "the safe is empty",
                    onStageCompleteMessage: "The safe is now empty",
                    postStageCmd: "pick up hammer"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/guestRoom/guestRoomBlank.png",
  changeImagePath: "images/newRooms/guestRoom/guestRoomOpen.png", //Path for image once its been changed
  changeImageCmd: "try" 
 },
 {
  itemId: "buttonH", //TODO THIS MIGHT NOT BE AN ISSUE
  itemName: "red button",
  description: "Pushing this button looks really tempting...who knows what it does?",
  canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  imagePath: "",
  roomImagePath: "images/newRooms/guestRoom/guestRoomBlank.png",
  changeImagePath: "images/newRooms/guestRoom/guestRoomDark.png", //Path for image once its been changed
  changeImageCmd: "push" 
 },
 {
  itemId: "hammerH", 
  itemName: "hammer",
  description: "This hammer looks perfect for removing nails from something...do I know anywhere that nails are?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/hammerH.png" 
 }
 ]);

//Doors - 16
db.doors.insert(
[{
 	doorId: "doorAP",
	description: "This door doesn't seem to give you any clue of how you will open it, but it certainly looks important.",
	locked: true,
  // doorImage: 'images/newRooms/closet/closetBlank.png',
  // doorOpenImage: 'images/newRooms/closet/closetOpenLeft.png',
	doorStageList: 
	[{
    keyType: "key", //received from vending machine
    key: "final key",
    onStageCompleteDesc: "To connect all rooms, the password is 'vending machine'",
    onStageCompleteMessage: "The door opened! You see something is inscribed on the door..."
  }],
 },
 {
	doorId: "doorAB",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this goes on the back of a jigsaw puzzle
		key: "kittens123", //THIS CANNOT CHANGE, already on puzzle
    onStageCompleteDesc: "The door is open"

	}]
 },
 {
	doorId: "doorBA",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //this is received after rock paper scissors
		key: "puppies456",
    onStageCompleteDesc: "The door is open"

	}]
 },
 { 
	doorId: "doorBC",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "blue key", //user got this from safeA
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorCD",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1c
		key: "2002",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorDC",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this from googling answer to note1d
		key: "frank phillips",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorCB",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList: 
	[{
		keyType: "key",
		key: "purple key", //user received this from a mouse
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
 	
	doorId: "doorDE", //for stageABCD, this is where they enter the passwords from mentors
	description: "You will definitely need some help for this - at least 6 friends. When you all can huddle up in the same space, find an administrator for the clue to this door.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //First Names, Reverse Alphabetical
		key: "lccbaa", //FINDME
    onStageCompleteMessage: "You heard the door creak open...the password worked!"
	}]
 },
 {
	doorId: "doorEF",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //user gets this password from the time on the wall, based on the hint
		key: "3:40",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorFE",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //User picks this up by unscrambling the letters on the fridge, based on the hint
		key: "natural gas",
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorFG",
	description: "This door has a hole for a key.",
	locked: true,
	doorStageList:  
	[{
		keyType: "key", //received from safeF 
		key: "green key",
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },
 {
	doorId: "doorGH",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the back of a puzzle
		key: "wolfpups112", //CANNOT CHANGE, this is on puzzle already
    onStageCompleteDesc: "The door is open"
	}]
 },
 {
	doorId: "doorHG",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //Password in the code
		key: "joeys101", //FINDME make sure there is a fairly technical person on this for Test2
    onStageCompleteDesc: "The door is open" 
	}]
 },
 {
	doorId: "doorGF",
	description: "This door has a space for words...maybe a password could work?",
	locked: true,
	doorStageList: 
	[{
		keyType: "password", //found on the key in safeG after using remote
		key: "exploration and production",
		onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
	}]
 },{ 
	doorId: "doorED", 	//For EFGH, passwords from mentors
	description: "You will definitely need some help for this - at least 6 friends. When you all can huddle up in the same space, find an administrator for the clue to this door.",
	locked: true,
	doorStageList: 
	[{
		keyType: "password",  //FINDME
		key: "bilmss", //First Name initials alphabetical
    onStageCompleteDesc: "You heard the door creak open...the password worked!"
	}]
 },
 {
  doorId: "doorHI",
  description: "This door doesn't even look like it opens any more...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "RANDOM", //found in riddle in note
    key: "",
    onStageCompleteDesc: "user will never enter this door"
  }]
 }
 ]);

db.userrooms.insert(
 [{
  roomId: "I",
  roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. Looking around you find a toilet, an old vault, some shampoo, and a shower. The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorIJ", 
  roomRightDoor: "doorIH",
  roomImagePath: "images/newRooms/masterBathroom/MasterBathroom.png",
  roomImageList:   [{ imageId: "I",
                        imageState: 0 },
                      { imageId: "doorIJ", 
                        imageState: 0  },
                      { imageId: "doorIH",
                        imageState: 0 },
                      { imageId: "old vault",
                        imageState: 0 }],
  itemsInRoom: ["old vault", "shower", "toilet", "shampoo"],
  linkedRooms: ["I"],
  userName: "Aidan",
  userPassword: "eyas718",
  userInventory: ["note 1i", "note 2i-b", "clue is"]
 },
 {
  roomId: "J",
  roomDescription: "You find yourself in a modern looking living room. There is also a new vault that looks interesting, a futon, some movies, a stereo, and a top hat. Wonder what is in there? The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorJK", 
  roomRightDoor: "doorJI",
  roomImagePath: "images/newRooms/livingRoom/livingRoom.png",
  roomImageList:   [{ imageId: "J",
                        imageState: 0 },
                      { imageId: "doorJK", 
                        imageState: 0  },
                      { imageId: "doorJI",
                        imageState: 0 },
                      { imageId: "new vault",
                        imageState: 0 }],
  itemsInRoom: ["new vault", "top hat", "futon", "movies", "stereo"],
  linkedRooms: ["J"],
  userName: "Teonna-miguel",
  userPassword: "ephyna192",
  userInventory: ["note 1j","clue computer"]
 },
 {
  roomId: "K",
  roomDescription: "You find yourself in an overwhelmingly pink bedroom. As you squint your eyes against the offensive color scheme you see a memory box, a rug, a cabinet, a twin bed, and a keypad. Every now and then you can hear a faint sound. Is that scratching? The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorKL", 
  roomRightDoor: "doorKJ",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoom.png",
  roomImageList:   [{ imageId: "K",
                        imageState: 0 },
                      { imageId: "doorKL", 
                        imageState: 0  },
                      { imageId: "doorKJ",
                        imageState: 0 },
                      { imageId: "cabinet",
                        imageState: 0 },
                      { imageId: "parrot",
                        imageState: 0 }],
  itemsInRoom: ["cabinet", "parrot", "keypad", "twin bed", "memory box", "rug"],
  linkedRooms: ["K"],
  userName: "Alexander i",
  userPassword: "pinkie021",
  userInventory: ["note 1k"]
 },
 {
  roomId: "L",
  roomDescription: "You find yourself in a cute nursery. As you look around you notice a crib, a chest of drawers, crackers and toys on the floor, an old clock and a poster on the wall, and some money on the ceiling. The money looks like it is just out of reach for you. If only you had something to help you reach it. The clock on the wall does not seem to be moving. The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorLM", 
  roomRightDoor: "doorLK",
  roomImagePath: "images/newRooms/nursery/nursery.png",
  roomImageList:   [{ imageId: "L",
                        imageState: 0 },
                      { imageId: "doorLM", 
                        imageState: 0  },
                      { imageId: "doorLK",
                        imageState: 0 },
                      { imageId: "crackers",
                        imageState: 0 },
                      { imageId: "money",
                        imageState: 0 }],
  itemsInRoom: ["crackers","money", "old clock", "poster", "toys", "crib", "chest of drawers"],
  linkedRooms: ["L"],
  userName: "Rachel-jalen",
  userPassword: "peachick222",
  userInventory: ["note 1l", "clue smartest"]
 },
 {
  roomId: "M",
  roomDescription: "You find yourself in trapped in a gym. Looking around the only thing of interest that you see is a combo lockbox. The only way out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorMN", 
  roomRightDoor: "doorML",
  roomImagePath: "images/newRooms/gym/gym.png",
  roomImageList:   [{ imageId: "M",
                        imageState: 0 },
                      { imageId: "doorMN", 
                        imageState: 0  },
                      { imageId: "doorML",
                        imageState: 0 },
                      { imageId: "combo lockbox",
                        imageState: 0 }],
  itemsInRoom: ["combo lockbox","punching bag","water fountain","barbell"],
  linkedRooms: ["M"],
  userName: "Tyler",
  userPassword: "cosset324",
  userInventory: ["note 1m", "note 2m", "clue the"]
 },
 {
  roomId: "N",
  roomDescription: "You find yourself trapped in a theater room of some sort. Looking around you see a combination lock, comfy chairs, a media center, speakers and a black hat. Hmm It is not so bad in here. Maybe if you could put on a movie it would not be horrible to stay here for a bit. The only way out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorNO", 
  roomRightDoor: "doorNM",
  roomImagePath: "images/newRooms/theater/theater.png",
  roomImageList:   [{ imageId: "N",
                        imageState: 0 },
                      { imageId: "doorNO", 
                        imageState: 0  },
                      { imageId: "doorNM",
                        imageState: 0 },
                      { imageId: "combination lock",
                        imageState: 0 }],
  itemsInRoom: ["combination lock","black hat","comfy chairs", "media center", "speakers"],
  linkedRooms: ["N"],
  userName: "Vladia-tama",
  userPassword: "neonate252",
  userInventory: ["note 1n", "clue most"]
 },
 {
  roomId: "O",
  roomDescription: "You find yourself trapped in a… bathroom?! Eww gross. As you hold your nose from the smell you see a bathtub, a locked box, a sink, an efficient toilet, and some cheese. Nasty! Who keeps cheese in the bathroom? The only ways out that you can see are the two doors on either side of the room.",
  roomLeftDoor: "doorOP", 
  roomRightDoor: "doorON",
  roomImagePath: "images/newRooms/bathroom/bathroom.png",
  roomImageList:   [{ imageId: "O",
                        imageState: 0 },
                      { imageId: "doorOP", 
                        imageState: 0  },
                      { imageId: "doorON",
                        imageState: 0 },
                      { imageId: "cheese",
                        imageState: 0 },
                      { imageId: "locked box",
                        imageState: 0 }],
  itemsInRoom: ["cheese", "locked box", "sink", "efficient toilet", "bath tub"],
  linkedRooms: ["O"],
  userName: "Slayter-rhys",
  userPassword: "cygnet627",
  userInventory: ["note 1o", "note 2o", "clue who"]
 },
 {
  roomId: "P",
  roomDescription: "You look around the simple bedroom and don't find much of interest besides a desk, a queen bed, a portrait, a floormat and the a Red hat icon on the computer. As you look at the figure in the potrait on the wall you think that it looks familiar. The only ways out that you can see are two doors on either side of the room. ",
  roomLeftDoor: "doorPA", 
  roomRightDoor: "doorPO",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroom.png",
  roomImageList:   [{ imageId: "P",
                        imageState: 0 },
                      { imageId: "doorPA", 
                        imageState: 0  },
                      { imageId: "doorPO",
                        imageState: 0 },
                      { imageId: "desk",
                        imageState: 0 },
                      { imageId: "rat",
                        imageState: 0 }],
  itemsInRoom: ["desk", "rat", "red hat", "queen bed", "portrait", "floormat"],
  linkedRooms: ["P"],
  userName: "Juan-kyle",
  userPassword: "poult282",
  userInventory: ["note 1p", "clue awesome"]
 }
 ]);


////ITEMS LIST//// - 40
db.items.insert(
 [
//ALL NOTES
{
  itemId: "note1I",
  itemName: "note 1i",
  description: "Day 35: Doris has given me the following clue: What oil sector is ConocoPhillips classified as that is also known as E&P?  How am I supposed to figure it out without google?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1I.png" 
 },
 {
  itemId: "note2I",
  itemName: "note 2i",
  description: "A S E H L",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2I.png" 
 },
 {
  itemId: "note2I-B",
  itemName: "note 2i-b",
  description: "Day 329: I don't understand. Doris said that the combination was a birth month and day. If it was not mine, whose is it?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2IB.png" 
 },
 {
  //TODO CHANGEME
  itemId: "note1J",
  itemName: "note 1j",
  description: "Look under your chair",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1K.png" //nchanged because jk mix up
 },
 {
  itemId: "note2J",
  itemName: "note 2j",
  description: "New technologies have allowed us to more effectively get oil and gass out of what type of rock? Doris said the password is the answer to that question. What could it be? Im not a geologist...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2J.png" 
 },
  {
  itemId: "note1K",
  itemName: "note 1k",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors'" ,
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1J.png" //nchanged because jk mix up
 },
 {
  itemId: "note4k", //USER DOES NOT START HOLDING THIS CLUE
  itemName: "note4k",
  description: "Look in the Hat" ,
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note4K.png" 
 },
 {
  itemId: "note1L",
  itemName: "note 1l",
  description: "Day 52: Still can not figure out the password to the door on the right. The only thing Doris will say when I ask is Tick Toc...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1L.png" 
 },
  {
  itemId: "note1M",
  itemName: "note 1m",
  description: "Day 45: I have found that Doris loves a good game of Rock Paper Scissors. Word on the grapevine is that she will bet anything on it. Maybe even my Freedom? I will try tomorrow by saying 'I Challenge you to a game of Rock Paper Scissors' ",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1M.png" 
 },
 {
  itemId: "note 2M",
  itemName: "note 2m",
  description: "Day 329: I don't understand. Doris said that the combination was a birth month and day. If it was not mine, whose is it?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2M.png" 
 },
 { //TODO Double check this pw on door
  itemId: "note 2MP",
  itemName: "note 2mpic",
  description: "Note with two Pictograms on it",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2MP.png" 
 },
 {
  itemId: "note1N",
  itemName: "note 1n",
  description: "Day 34: The only clue I can get from Doris is that the password to one of the doors is the name of a movie that has her computer brother, Joshua, in it and that he likes to play games? I wonder if her brother is as crazy as she is. If only I had access to google I could figure it out...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1N.png" 
 },
 {
  itemId: "note 2NP",
  itemName: "note 2npic",
  description: "<Has note about password being answer to the pictogram puzzles>",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2NP.png" 
 },
  {
  itemId: "note1O",
  itemName: "note 1o",
  description: "Day 101:  I have finally tricked Doris into giving me a clue to the password for one of the doors. Doris said something about inspecting the code comments? What does that even mean though?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1O.png" 
 },
 {
  itemId: "note2O",
  itemName: "note 2o",
  description: "Day 2: I have found a Bobby pin. Maybe I can use it to pick a lock. Like the one I saw in the room next door.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note2O.png" 
 },
 {
  itemId: "note1P",
  itemName: "note 1p",
  description: "Day 97: After months of questioning I have made a break through. The password to one of the doors has something to do with Doris's Mother. She said that there is a picture of her mother in the room. How does a computer have a mother? If only I had access to google I might be able to figure it out...",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/note1P.png" 
 },
 {
  itemId: "clueI",
  itemName: "clue is",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionC.png" 
 },
 {
  itemId: "clueJ",
  itemName: "clue computer",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionH.png" 
 },
 {
  itemId: "clueL",
  itemName: "clue smartest",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionG.png" 
 },
 {
  itemId: "clueM",
  itemName: "clue the",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionD.png" 
 },
 {
  itemId: "clueN",
  itemName: "clue most",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionE.png" 
 },
 {
  itemId: "clueO",
  itemName: "clue who",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionB.png" 
 },
 {
  itemId: "clueP",
  itemName: "clue awesome",
  description: "",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/notes/sevenWordQuestionF.png" 
 },

//ROOM I ITEMS
{
  itemId: "old vaultI",
  itemName: "old vault",
  description: "What's inside?...there's space for 4 numbers to open it",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //password is userJ's birthday, derived from note
                    key: "0516", //FINDME Personalized! userJ
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2i"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/masterBathroom/masterBathroomBlank.png",
  changeImagePath: "images/newRooms/masterBathroom/masterBathroomOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed  
 },
 //ROOM J ITEMS
 {
  itemId: "new vault",
  itemName: "new vault",
  description: "What's inside? ...there's space for 4 numbers to open it",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "password", //password is userI's birthday, drived from note
                    key: "1027", //FINDME Personalized! userI
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2j"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/livingRoom/livingRoomBlank.png",
  changeImagePath: "images/newRooms/livingRoom/livingRoomOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed 
 },
 {
  itemId: "hatJ",
  itemName: "top hat",
  description: "I don't see anything inside!",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "TEMP", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "I think the hat is actually empty this time.",
                    onStageCompleteMessage: "You found a fishing rod? How did that fit in a hat?!",
                    postStageCmd: "pick up rod"
                }],
  imagePath: "images/items/topHat.png" 
 },
 {
  itemId: "rodJ",
  itemName: "rod",
  description: "This looks like it's missing an important piece.",
    canBeHeld: true,
  unActivated: false, 
  imagePath: "images/items/rod.png" 
 },
{
  itemId: "fishingrodJ",
  itemName: "fishing rod",
  description: "What can you use this on? There's no water around...maybe it can reach something far away?",
    canBeHeld: true,
  unActivated: false, 
  imagePath: "images/items/fishingrod.png" 
 },
 //DOOR K ITEMS
  
 {
  itemId: "heavyItemK",
  itemName: "cabinet",
  description: "This is heavy, I wonder if there's anything behind it?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "L",
                    onStageCompleteDesc: "Well this is unusual...you found a parrot behind the cabinet!",
                    onStageCompleteMessage: "A wild parrot appeared! He is shouting 'POLLY WANTS A CRACKER', and it's very annoying. ",
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoomBlank.png",
  changeImagePath: "images/newRooms/kidsRoom/kidsRoomMoved.png", //Path for image once its been changed
  changeImageCmd: "push" // Command that signals item will need to be changed  
 },
 {
  itemId: "parrotK",
  itemName: "parrot",
  description: "The parrot has a key..but he doesn't look like he wants to give it up. ",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "crackers",
                    onStageCompleteDesc: "You aren't sure how, but the parrot disappeared when you stopped looking...how bizarre!",
                    onStageCompleteMessage: "The parrot gave you a red key",
                    postStageCmd: "pick up red key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/kidsRoom/kidsRoomBlank.png",
  changeImagePath: "images/newRooms/kidsRoom/kidsRoomTaken.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed 
 },
{ 
  itemId: "keypadK",
  itemName: "keypad",
  description: "It appears that you can type a password directly to Doris herself through this alphanumeric keypad...I wonder what it could be for? ",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password derived from 7 worded question
                    key: "4712563",
                    onStageCompleteDesc: "The keypad doesn't look very interesting now that you've already activated it.",
                    onStageCompleteMessage: "Doris is very impressed that you figured out her puzzle..she has given you a special clue to help you get out.",
                    postStageCmd: "pick up note4k"
                }],
  imagePath: "" 
 },
{
  itemId: "keyKJ",
  itemName: "red key",
  description: "Where can I possibly use this?",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/redKey.png" 
 },
//ROOM L ITEMS
 {
  itemId: "moneyL", //TODO Give Override
  itemName: "money",
  description: "That money looks pretty nifty...too bad you can't reach it, no matter how hard you reach.",
    canBeHeld: false, //manually set this when money can actually be reached
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "key",
                    key: "fishing rod",
                    onStageCompleteDesc: "Now you have money! ....in a house. Where you can't buy anything. How useful. It does look like this money is incredibly thin, it would probably fit under a door...",
                    onStageCompleteMessage: "You retrieved the money!",
                    postStageCmd: "pick up money"
                }],
  imagePath: "images/items/moneyL.png",
  roomImagePath: "images/newRooms/nursery/nurseryMoney.png",
  changeImagePath: "images/newRooms/nursery/nurseryBlank.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed  
 },
 {
  itemId: "crackersL",
  itemName: "crackers",
  description: "I probably wouldn't eat crackers that have been left out...but I bet someone else would.",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/crackers2L.png",
  roomImagePath: "images/newRooms/nursery/nurseryCrackers.png",
  changeImagePath: "images/newRooms/nursery/nurseryBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" // Command that signals item will need to be changed 
 },
 {
  itemId: "clockL",
  itemName: "old clock",
  description: "The time is 3:30",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "" 
 },
//ROOM M ITEMS
{
  itemId: "combinationlockM",
  itemName: "combo lockbox",
  description: "I wonder if there's anything useful in here...there's space for 4 numbers to open it",//"contains note about password [2NP]; Opens with birthday of N",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password is userN's birthday, derived from note
                    key: "0217", //FINDME Personalized! userN Birthday
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2mpic"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/gym/gymBlank.png",
  changeImagePath: "images/newRooms/gym/gymOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed  
 },
 //ROOM N ITEMS
 {
  itemId: "combinationlockN",
  itemName: "combination lock",
  description: "I wonder if there's anything useful in here...there's space for 4 numbers to open it",//"Contains note with Pictograms [2MP]; Opens with birthday of M",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  itemStageList: [{
                    keyType: "password", //password is userM's birthday, derived from note
                    key: "0629", //FINDME Personalized! userM birthday
                    onStageCompleteDesc: "Good work, there's no longer anything in this combo lock box.",
                    onStageCompleteMessage: "That worked! And inside you found a note",
                    postStageCmd: "pick up note 2npic"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/theater/theaterBlank.png",
  changeImagePath: "images/newRooms/theater/theaterOpen.png", //Path for image once its been changed
  changeImageCmd: "try" // Command that signals item will need to be changed 
 },
 {
  itemId: "hatN",
  itemName: "black hat",
  description: "There is a worn handle inside the hat...what a weird place to keep that",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "look", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "Nothing else inside this giant black hat.",
                    onStageCompleteMessage: "There is a worn handle inside the hat...what a weird place to keep that",
                    postStageCmd: "pick up handle"
                }],
  imagePath: "" 
 },
 {
  itemId: "handleN",
  itemName: "handle",
  description: "This handle is clearly old, but it looks like it connects to something.",
  canBeHeld: true,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key", //change to look when sevenworded question is answered
                    key: "rod",
                    onStageCompleteDesc: "This is part of the pole now",
                    onStageCompleteMessage: "The handle combined with the rod to create a pole",
                    postStageCmd: "pick up pole"
                }],
  imagePath: "images/items/handle.png" 
 },
 {
  itemId: "poleN",
  itemName: "pole",
  description: "This handle is clearly old, but it looks like it connects to something.",
  canBeHeld: true,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key", //change to look when sevenworded question is answered
                    key: "reel",
                    onStageCompleteDesc: "This is part of the fishing rod now",
                    onStageCompleteMessage: "The pole combined with the reel to create a working fishing rod! I wonder what you can use it for?",
                    postStageCmd: "pick up fishing rod"
                }],
  imagePath: "images/items/pole.png" 
 },
 //ROOM O ITEMS
 {
  itemId: "cheeseO",
  itemName: "cheese",
  description: "cheese",
    canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/cheeseO.png",
  roomImagePath: "images/newRooms/bathroom/bathroomCheese.png",
  changeImagePath: "images/newRooms/bathroom/bathroomBlank.png", //Path for image once its been changed
  changeImageCmd: "pick" // Command that signals item will need to be changed  
 },
 {
  itemId: "lockedboxO",
  itemName: "locked box",
  description: "I wonder how you'll open this?",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "bobby pin",
                    onStageCompleteDesc: "You found a key! I wonder what you could use it on...",
                    onStageCompleteMessage: "The key has been put in your inventory",
                    postStageCmd: "pick up dusty key"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/bathroom/bathroomBlank.png",
  changeImagePath: "images/newRooms/bathroom/bathroomOpen.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed 
 },
 {
  itemId: "keyON",
  itemName: "dusty key",
  description: "Where can I possibly use this?",
  canBeHeld: true,
  unActivated: false, //true if change needed to object, default false
  imagePath: "images/items/greyKey.png" 
 },
 //ROOM P ITEMS
 {
  itemId: "heavyItemP",
  itemName: "desk",
  description: "This is heavy, I wonder if there's anything behind it?",
    canBeHeld: false,
  unActivated: true, //true if change needed to object, default false
  pushNumber: 2,
  itemStageList: [{
                    keyType: "push",
                    key: "O",
                    onStageCompleteDesc: "There's a rat staring at you with beady eyes, holding a bobby pin.",
                    onStageCompleteMessage: "You pushed the desk, only to reveal a rat, holding a bobby pin.",
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroomBlank.png",
  changeImagePath: "images/newRooms/adultBedroom/adultBedroomMoved.png", //Path for image once its been changed
  changeImageCmd: "push" // Command that signals item will need to be changed 
 },
 {
  itemId: "mouseP",
  itemName: "rat",
  description: "This rat seems be carrying a bobby pin",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "key",
                    key: "cheese",
                    onStageCompleteDesc: "The rat already disappeared...you can't find him anymore.",
                    onStageCompleteMessage: "The rat dropped a bobby pin, then scattered away.",
                    postStageCmd: "pick up bobby pin"
                }],
  imagePath: "",
  roomImagePath: "images/newRooms/adultBedroom/adultBedroomBlank.png",
  changeImagePath: "images/newRooms/adultBedroom/adultBedroomMouseGone.png", //Path for image once its been changed
  changeImageCmd: "use" // Command that signals item will need to be changed  
 },
 {
  itemId: "bobbypinP",
  itemName: "bobby pin",
  description: "I bet this thing will be useful..",
    canBeHeld: true,//ue if change needed to 
  unActivated: false, //trobject, default false
  imagePath: "images/items/pinP.png" 
 },
 {
  itemId: "hatP",
  itemName: "red hat",
  description: "Something seems off about this, but you don't know what.",
    canBeHeld: false,
  unActivated: true, 
  itemStageList: [{
                    keyType: "TEMP", //change to look when sevenworded question is answered
                    key: "",
                    onStageCompleteDesc: "The red hat looks normal now.",
                    onStageCompleteMessage: "You found a reel near the computer!",
                    postStageCmd: "pick up reel"
                }],
  imagePath: "" 
 },
 {
  itemId: "reelP",
  itemName: "reel",
  description: "something seems off about this, but you don't know what.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "images/items/reel.png" 
 }
 ]);

//Doors - 16
db.doors.insert(
 [
 {
  doorId: "doorIJ",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //user gets this answer by reading their first note
    key: "upstream",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorJI",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password",//THIS CANNOT CHANGE, already on puzzle
    key: "tinyspider883", //usergets this from the back of their puzzle
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorJK",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //get this from a note
    key: "shale",
    onStageCompleteDesc: "The door is open"
  }]
 },{
  doorId: "doorKL",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //Received from Rock Paper Scissors
    key: "cubs789",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorLK",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received from clock hint
    key: "3:30",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorKJ",
  description: "This door has a hole for a key.",
  locked: true,
  doorStageList: 
  [{
    keyType: "key", //received from parrot
    key: "red key",
    onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
  }]
 },
 {
  doorId: "doorLM", //mentor
  description: "You will definitely need some help for this - at least 6 friends. When you all can huddle up in the same space, find an administrator for the clue to this door.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //reverse alphabetical first initials
    key: "trmjaa", //FINDME
    onStageCompleteDesc: "You heard the door creak open...the password worked!"
  }]
 },
 {
  doorId: "doorMN",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received from rock paper scissors
    key: "calf131",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorNM",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //found in riddle in note
    key: "wargames",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorNO",
  description: "This door definitely takes a password. You should use the symbol '&' if you are going to list two things as the password.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //Found in pictograms in each room combolockbox
    key: "trail mix & side show", 
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorOP",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //received in HTML
    key: "joeys101", //TODO MAKE SURE THIS MATCHES doorHG
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorPO",
  description: "This door has a space for words...maybe a password could work?",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //from clue, picture in room
    key: "ada lovelace",
    onStageCompleteDesc: "The door is open"
  }]
 },
 {
  doorId: "doorON",
  description: "This door has a hole for a key.",
  locked: true,
  doorStageList: 
  [{
    keyType: "key", //received from lockbox that is opened by bobby pin
    key: "dusty key",
    onStageCompleteDesc: "BAM DOOR IS OPEN WOO"
  }]
 },
 {
  doorId: "doorML", //mentor
  description: "You will definitely need some help for this - at least 6 friends. When you all can huddle up in the same space, find an administrator for the clue to this door.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //alphabetical first name initials 
    key: "jkrsttv", //FINDME
    onStageCompleteDesc: "You heard the door creak open...the password worked!"
  }]
 },
 {
  doorId: "doorPA",
  description: "This door doesn't even look like it is going to open any time soon...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "password", //found in riddle in note
    key: "vending machine",
    onStageCompleteDesc: "You united the full team - now how to you get out? "
  }]
 },
 {
  doorId: "doorIH",
  description: "This door doesn't even look like it opens any more...maybe we should look for a way out somewhere else.",
  locked: true,
  doorStageList: 
  [{
    keyType: "RANDOM", //found in riddle in note
    key: "",
    onStageCompleteDesc: "user will never enter this door"
  }]
 }
 ]);

//Steps: 
//0. Create list of items from looking at image in room
//1. Create Item below
//2. Add funny description
//3. Add item to items in room list
//4. Add item to description of room
//5. Test Item


//TOADD
//CONFIRM ROOM is not in any of these of door
// A. Folded clothes, dresser, 
// B. tool Shelves
// C. (spare books?)
// D. overhead lamp, red shoebox
// E. books on shelf, pc desk, outlet, wastebin
// F. Oven, pantry, counter
// G. Washer, Dryer, Ironing board, 
// H. full bed, rug, lightH, wall shelfs

// I. toilet, shower curtain, bath tub, cleanser, sink
// J. Futon, movies, stereo,  floorboard(?), 
// K. twin bed, memory box, color
// L. crib, wall poster, child blocks, chest of drawers, bouncy ball
// M. punching back, water cooler, heavy weight, lockers, yoga mats
// N. surround sound, theater seats
// O. Efficient toilet, sink, tub 
// H. queen bed, portrait, floor mat


//// EXTRA ITEMS LIST//// 
db.items.insert([
  //ROOM A Extra
{
  itemId: "itemA1",
  itemName: "hanging coats",
  description: "These clothes look outdated, and like they might need to be cleaned. Alas, there is no path to Narnia behind them.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemA2",
  itemName: "purse",
  description: "That purse looks so last season, it's probably not useful.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemA3",
  itemName: "shoebox",
  description: "The shoebox holds a pair of old and unused ",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
  //ROOM B Extra
{
  itemId: "itemB1",
  itemName: "toolbox",
  description: "There's a myriad of rusty tools in this toolbox, but they all look too old and rusted to be much use for you.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemB2",
  itemName: "car",
  description: "The car looks fairly vintage, fairly neglected and fairly uninteresting.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemB3",
  itemName: "trash can",
  description: "You can't tell what is causing it, but the trash smells pretty foul. You better leave it alone.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
  //ROOM C Extra
{
  itemId: "itemC1",
  itemName: "coffee mug",
  description: "Ew. Who leaves a used coffee mug out in a library?",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemC2",
  itemName: "sign",
  description: "It reads 'Library', indicating that you are right now in a library. ",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM D Extra
{
  itemId: "itemD1",
  itemName: "painting",
  description: "This painting looks like it is of someone incredibly important. The letters 'FP' are inscribed in the corner of the painting.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemD2",
  itemName: "kitchen chairs",
  description: "The kitchen chairs look incredibly old...they would probably break if you tried to use them.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemD3",
  itemName: "mug",
  description: "What a strange place to store a mug. Out of reach, out of mind, I suppose.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM E Extra
{
  itemId: "itemE1",
  itemName: "outlet",
  description: "This outlet may be dangerous, don't try to use it.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemE2",
  itemName: "wastebin",
  description: "There's mostly some crumbled paper in the wastebin, nothing important looking though.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemE3",
  itemName: "workdesk",
  description: "You see a cheap looking desk for working. None of the drawers look like they open.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM F Extra
{
  itemId: "itemF1",
  itemName: "oven",
  description: "The oven looks pristine...clearly Doris has not been using this to cook. However, a computer probably doesn't need to cook, so that makes sense.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemF2",
  itemName: "pantry",
  description: "The pantry is empty - no food for you!",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemF3",
  itemName: "counter",
  description: "The counter is devoid of any crumbs or objects. However, you do see 'DORIS' scratched into the counter.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM G Extra
{
  itemId: "itemG1",
  itemName: "washing machine",
  description: "The washing machine is surprisingly modern, but you don't have the first idea how to use it.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemG2",
  itemName: "dryer",
  description: "You probably shouldn't waste time trying to do laundry when you should be trying to escape.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemG3",
  itemName: "ironing board",
  description: "You don't see anything useful about this ironing board.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM H Extra
{
  itemId: "itemH1",
  itemName: "full bed",
  description: "The bed is full-sized.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemH2",
  itemName: "lamp",
  description: "You do love lamp. But you don't see any way to turn this one on.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemH3",
  itemName: "red box",
  description: "WHAT'S IN THE BOX? ...nothing at all actually. What a let down.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},

//ROOM I Extra
{
  itemId: "itemI1",
  itemName: "shower",
  description: "The shower looks dry.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemI2",
  itemName: "toilet",
  description: "Well it's a....toilet. Not sure why you're trying to look at this.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemI3",
  itemName: "shampoo",
  description: "The label says: 'Easy, Breezy, Beautiful: Doris'.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM J Extra
{
  itemId: "itemJ1",
  itemName: "futon",
  description: "While it is tempting, but you probably shouldn't waste time napping while you're trapped in this house.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemJ2",
  itemName: "movies",
  description: "You see some pretty interesting movies here: Hocus Pocus, The Illusionist, and Now You See Me.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemJ3",
  itemName: "stereo",
  description: "For some reason, someone has etched the word 'MAGIC' on this stereo.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM K Extra
{
  itemId: "itemK1",
  itemName: "twin bed",
  description: "It's clearly a bed meant for a child. You probably don't need it for anything.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemK2",
  itemName: "memory box",
  description: "There's a bunch of photos of people you don't know in this box.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemK3",
  itemName: "rug",
  description: "The rug is has white and a horrible pink color. It looks like its been glued to the floor for some reason...",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM L Extra
{
  itemId: "itemL1",
  itemName: "poster",
  description: "The poster has all of the letters in the alphabet...or does it?",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemL2",
  itemName: "toys",
  description: "While very cute, the toys on the floor do not interest you.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemL3",
  itemName: "crib",
  description: "You see something in the crib! ..............................................oh actually, you don't. It's empty.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemL4",
  itemName: "chest of drawers", //FINDME
  description: "It looks cheaply made.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM M Extra
{
  itemId: "itemM1",
  itemName: "punching bag",
  description: "It looks like someone bought this at some point for a new year's resolution, and then never used it once.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemM2",
  itemName: "water fountain",
  description: "The water fountain is so dry it actually looks thirsty.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemM3",
  itemName: "barbell",
  description: "You know just from looking at the barbell that there is no way you could pick it up.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM N Extra
{
  itemId: "itemN1",
  itemName: "comfy chairs",
  description: "The chairs look unused and comfy. I wonder if they're useful in anyway?",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemN2",
  itemName: "media center",
  description: "Something is scratched into the media center....it says 'UNDER THE TABLE'",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemN3",
  itemName: "speakers",
  description: "The speakers are surprisingly modern. But they aren't hooked up to anything.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM O Extra
{
  itemId: "itemO1",
  itemName: "sink",
  description: "You see a sink with a word etched on it 'KEANU'",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemO2",
  itemName: "efficient toilet",
  description: "Good for Doris - she has an efficient toilet! It's not useful to you.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemO3",
  itemName: "bath tub",
  description: "A bath would probably be nice right now, but you probably shouldn't waste time bathing when you should be escaping.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
//ROOM P Extra
{
  itemId: "itemP1",
  itemName: "queen bed",
  description: "What a comfy-looking, queen sized-bed. ",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemP2",
  itemName: "portrait",
  description: "The woman looks very lovely in this portrait. The frame around has a plaque that reads 'Mother'.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
},
{
  itemId: "itemP3",
  itemName: "floormat",
  description: "The floormat is quite pretty.",
  canBeHeld: false,
  unActivated: false,
  imagePath: "" 
}
]);


db.items.count();
db.userrooms.count();
db.doors.count();